﻿function loadUserList(host, access_token, serverChange, callback, retry) {   
    var url = host + "/api/main/employee-full-list";
    var dataKey = "data_users";
    var changeKey = "change_users";
    var localChange = readCacheChange(changeKey);
    if (localChange < serverChange || localChange == 0) {
        PNCustomGet(url, access_token, function (data) {
            if (data == "400") {
                retry();
            }
            else {
                var list = JSON.parse(data);
                localStorage.setItem(dataKey, data);
                writeCacheChange(changeKey, serverChange);                    

                callback(list);
            }
        }, function () { callback(new Array()); });
    }
    else {
        var text = localStorage.getItem(dataKey);
        if (text)
            callback(JSON.parse(text));
        else
            callback(new Array());
    }
}
