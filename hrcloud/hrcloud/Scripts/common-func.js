﻿function showLoading(id) {
    $("#" + id).css("display", "block");
    $("#" + id).append('<p style="text-align:center"><img src="/Common/Images/loading.gif" /></p>');
}
function stopLoading(id) {
    $("#" + id).empty();
    $("#" + id).css("display", "none");
}
