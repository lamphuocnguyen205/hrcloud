﻿var arrows;
if (KTUtil.isRTL()) {
    arrows = {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>'
    }
} else {
    arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }
}
function makeDatepicker(id, change) {
    $('#' + id).datepicker({
        rtl: KTUtil.isRTL(),
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        templates: arrows,
        format: 'dd/mm/yyyy'
    }).on('change', function () {
        if (change)
            change();
    });;
}

