﻿function loadAreas(host, access_token, serverChange, callback) {
    var url = host + "/api/main/area-list";
    var dataKey = "data_areas";
    var changeKey = "change_areas";
    var localChange = readCacheChange(changeKey);
    if (localChange < serverChange || localChange == 0) {
        PNCustomGet(url, access_token, function (data) {
            if (data == "400") {
                alert("Lỗi lấy dữ liệu");
            }
            else {

                var list = JSON.parse(data);
                localStorage.setItem(dataKey, data);
                writeCacheChange(changeKey, serverChange);

                callback(list);
            }
        }, function () { callback(new Array()); });
    }
    else {
        var text = localStorage.getItem(dataKey);
        if (text)
            callback(JSON.parse(text));
        else
            callback(new Array());
    }
}
