﻿
function registerSimpleUpload(fileInputId, postPhotoURL, callback) {
    var tid = "#" + fileInputId;
    $(tid).change(function () {
        var img = document.createElement("img");
        var reader = new FileReader();
        reader.onload = function (e) {
            img.src = e.target.result;
        }
        reader.readAsDataURL($(tid).get(0).files[0]);
        img.onload = function () {
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);

            var MAX_WIDTH = 600;
            var MAX_HEIGHT = 600;
            var width = img.width;
            var height = img.height;

            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            } else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);

            var dataurl = canvas.toDataURL('image/png', 1.0);
            var data = new FormData();
            data.append("photo", dataurl.substr(22, dataurl.length - 22));
            var ajaxRequest = $.ajax({
                type: "POST",
                url: postPhotoURL,
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    setTimeout(function () {
                        callback(result);
                    }, 1000);
                },
                error: function (xhr, errorType, exception) {
                    alert(JSON.stringify(xhr));
                    alert(JSON.stringify(errorType));
                    alert(JSON.stringify(exception));
                }
            });

            ajaxRequest.done(function (xhr, textStatus) {
                $(tid).val(null);
            });
        }
    });
}

var uploadedCount = 0;

function registerMultiUpload(fileInputId, postURL, callback) {
    var tid = "#" + fileInputId;
    $(tid).change(function () {
        $("#loadingBox").css("display", "block");
        var files = $(tid).get(0).files;
        var count = 0;
        uploadedCount = 0;
        var imgs = [];
        var names = {};
        var dataArray = [];
        for (var i = 0; i < files.length; i++) {
            names[files[i].name] = i;
            var img = document.createElement("img");

            var reader = new FileReader();

            reader.onload = function (e) {
                var fileName = e.target.fileName;
                imgs[names[fileName]].src = e.target.result;
            }

            reader.fileName = files[i].name;
            reader.readAsDataURL(files[i]);
            img.name = files[i].name;
            img.onload = function () {
                count++;
                var current = imgs[names[this.name]]
                var canvas = document.createElement('canvas');

                var MAX_WIDTH = 900;
                var MAX_HEIGHT = 900;
                var width = current.width;
                var height = current.height;

                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(current, 0, 0, width, height);

                var dataurl = canvas.toDataURL('image/png', 1.0);
                dataArray.push(dataurl);
                if (count >= files.length) {
                    var p0 = "";
                    var p1 = "";
                    for (var j = 0; j < count; j++) {
                        if (j % 2 == 0) {
                            p0 = dataArray[j].substr(22, dataArray[j].length - 22);
                            p1 = "";
                        }
                        else {
                            p1 = dataArray[j].substr(22, dataArray[j].length - 22);
                        }
                        if (j % 2 != 0 || j == count - 1) {
                            var data = new FormData();
                            data.append("photo0", p0);
                            data.append("photo1", p1);
                            var ajaxRequest = $.ajax({
                                type: "POST",
                                url: postURL,
                                contentType: false,
                                processData: false,
                                data: data,
                                success: function (result) {
                                    callback(result);
                                    uploadedCount = uploadedCount + 2;
                                    if (uploadedCount >= count)
                                        $("#loadingBox").css("display", "none");
                                }
                            });

                        }
                    }
                }
            }
            imgs.push(img);
        }
    });
}
var globalHost = "";
function MultiPhotoBox(elementId, host, dataList, postMultiURL) {
    this.elementId = elementId;
    this.data = dataList;
    this.host = host;
    globalHost = host;
    this.changed = false;
    this.postMultiURL = postMultiURL;
    this.origin = "";
    this.renderUI = function () {
        var cbCount = 0;
        var ci = 0;
        var ttCount = 0;
        var template = '<button class="btn btn-success btnHidePhoto" style="display:none">Thu gọn</button>'+
                        '<table id="tbPhoto" style="margin-top:10px; margin-bottom:10px; display:none">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th style="width:200px;text-align:center;border: 1px solid #454545;">Hình</th>'+
                                    '<th style="width:40px;text-align:center;border: 1px solid #454545;"><span class="oi oi-arrow-thick-bottom"></span></th>'+
                                    '<th style="width:40px;text-align:center;border: 1px solid #454545;"><span class="oi oi-arrow-thick-top"></span></th>'+
                                    '<th style="width:40px;text-align:center;border: 1px solid #454545;">Del</th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody id="tbBody">'+
                            '</tbody>'+
                        '</table>'+
                        '<button class="btn btn-success btnHidePhoto" style="display:none; margin-bottom:10px;">Thu gọn</button>'+
                        '<button id="btnShowTBPhoto" class="btn btn-info" style="width:100%; margin-bottom:10px; display:block">0 ảnh</button>'+
                        '<div id="loadingBox" style="padding-bottom:10px; display:none; text-align:center;">'+
                            '<img style="height:50px;" src="/Images/loading.gif" />'+
                            '<span>Đang tải ảnh lên...</span>'+
                        '</div>'+
                        '<div><button id="btnOpenMulti" class="btn btn-warning"><i class="icon-1x flaticon-photo-camera"></i> Chọn ảnh</button></div>'+
                        '<input style="visibility:hidden;" type="file" id="multiFileInput" accept="image/x-png,image/gif,image/jpeg" multiple />';

        var temp = '<tr>' +
            '<td style="padding:2px;border: 1px solid #454545;"><img tag="{tag}" style="width:250px;" src="{src}" /><input type="text" value="{desc}" placeholder="Mô tả ảnh" class="form-control" /></td>' +
            '<td style="border: 1px solid #454545;" class="pdown">{down}</td>' +
            '<td style="border: 1px solid #454545;">{up}</td>' +
            '<td style="border: 1px solid #454545;"><button type="button" class="btn btn-danger btn-del-photo"><i class="bi bi-trash"></i></button></td>' +
            '</tr>';
        var upHtml = '<button type="button" class="btn btn-primary btn-up-photo"><i class="bi bi-caret-up-fill"></i></button>';
        var downHtml = '<button type="button" class="btn btn-primary btn-down-photo"><i class="bi bi-caret-down-fill"></i></button>';

        $("#" + this.elementId).append(template);
        for (var i = 0; i < this.data.length; i++) {
            if (this.data[i].URL != "") {
                ttCount++;
                var html = '<input type="hidden" class="hphoto" value="' + this.data[i].URL + '" />';
                $("#" + this.elementId).append(html);

                var upH = i > 0 ? upHtml : "";
                var downH = i < this.data.length - 1 ? downHtml : "";
                var t = this.data[i].Desc;
                var src = this.host != "" ? this.host + this.data[i].URL : this.data[i].URL;
                var html = temp.replace("{src}", src)
                    .replace("{tag}", this.data[i].URL)
                    .replace("{desc}", t)
                    .replace("{down}", downH)
                    .replace("{up}", upH);
                $("#tbBody").append(html);
            }
        }

        this.origin = this.getMultiPostData();

        triggerPhotoButton();

        $("#btnShowTBPhoto").text(ttCount + " ảnh");
        //$("#btnShowTBPhoto").css("display", "block");

        $("#btnShowTBPhoto").click(function () {
            $("#tbPhoto").css("display", "block");
            $("#btnShowTBPhoto").css("display", "none");
            $(".btnHidePhoto").css("display", "block");
        });

        $(".btnHidePhoto").click(function () {
            $("#tbPhoto").css("display", "none");
            $("#btnShowTBPhoto").css("display", "block");
            $(".btnHidePhoto").css("display", "none");
            //window.scrollTo(0, 0);
        });

        $("#btnOpenMulti").click(function () { $("#multiFileInput").click(); });

        registerMultiUpload("multiFileInput", this.postMultiURL, function (results) {
            var split = results.split('*');
            for (var i = 0; i < split[i].length; i++) {
                if (split[i] != "") {
                    addPhoto(globalHost + split[i], split[i]);
                }
            }
            var list = $("#tbBody").find("tr");
            $("#btnShowTBPhoto").text(list.length + " ảnh");
        });

        function postPhoto(data, callback) {
            var ajaxRequest = $.ajax({
                type: "POST",
                url: "/api/post-multi-photo",
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    callback(result);
                }
            });
        }

        function triggerPhotoButton() {
            $(".btn-up-photo").unbind("click");
            $(".btn-up-photo").click(function () {
                var tr = $($(this).parent()).parent();
                var topTr = $(tr.prev());
                if (topTr) {
                    var abowsrc = $(topTr).find("img").attr("src");
                    var currentsrc = $(tr).find("img").attr("src");
                    $(topTr).find("img").attr("src", currentsrc);
                    $(tr).find("img").attr("src", abowsrc);
                }

            });

            $(".btn-down-photo").unbind("click");
            $(".btn-down-photo").click(function () {
                var tr = $($(this).parent()).parent();
                var nextTr = $(tr.next());
                if (nextTr) {
                    var belowsrc = $(nextTr).find("img").attr("src");
                    var currentsrc = $(tr).find("img").attr("src");
                    $(nextTr).find("img").attr("src", currentsrc);
                    $(tr).find("img").attr("src", belowsrc);
                }

            });

            $(".btn-del-photo").unbind("click");
            $(".btn-del-photo").click(function () {

                var tr = $($(this).parent()).parent();
                $(tr).remove();
                var first = $("#tbBody tr:first-child");
                var src = $(first).find("img").attr("src");
                var btnUp = $(first).find(".btn-up-photo").attr("class");
                if (btnUp != undefined) {
                    $(first).find(".btn-up-photo").remove();
                }

                var last = $("#tbBody tr:last-child");
                var src = $(last).find("img").attr("src");
                var btnDown = $(last).find(".btn-down-photo").attr("class");
                if (btnDown != undefined) {
                    $(last).find(".btn-down-photo").remove();
                }

                var list = $("#tbBody").find("tr");
                $("#btnShowTBPhoto").text(list.length + " ảnh");
            });
        }

        function addPhoto(url, path) {
            if (url != "") {
                var last = $("#tbBody tr:last-child");
                $(last).find(".pdown").html(downHtml);
                var updata = $("#tbBody").find("tr").length == 0 ? "" : upHtml;
                var html = temp.replace("{src}", url).replace("{tag}", path).replace("{desc}", "").replace("{down}", "").replace("{up}", updata);
                $("#tbBody").append(html);
                triggerPhotoButton();
            }
        }
    }

    this.isFull = function () {
        var list = $("#tbBody").find("tr");
        return list.length >= 10;
    }

    this.getPhotoCount = function () {
        var list = $("#tbBody").find("tr");
        return list.length;
    }
    this.clearPhoto = function () {
        $("#tbBody").empty();
    }

    this.getMultiPostData = function () {
        var list = $("#tbBody").find("tr");
        var res = new Array();
        for (var i = 0; i < list.length; i++) {
            var tag = $(list[i]).find("img").attr("src");
            var v = $(list[i]).find("input").val();
            var item = { URL: tag, Desc: v };
            res.push(item);
        }
        return JSON.stringify(res);
    }
}
