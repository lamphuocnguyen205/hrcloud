﻿function PNPost(url, data, callback, error) {
    $.ajax(url, {
        'data': JSON.stringify(data),
        'type': 'POST',
        'processData': false,
        'contentType': 'application/json',
        'success': function (data) {
            callback(data);
        },
        'error': function (err) {
            error(err);
        }
    });
}

function PNCustomPost(url, token, data, callback, error) {
    $.ajax(url, {
        'headers': {
            'access_token': token,
        },
        'data': JSON.stringify(data),
        'type': 'POST',
        'processData': false,
        'contentType': 'application/json',
        'success': function (data) {
            callback(data);
        },
        'error': function () {
            error();
        }
    });
}


function PNGet(url, callback, error) {
    $.ajax(url, {
        'type': 'GET',
        'processData': false,
        'contentType': 'application/json',
        'success': function (data) {
            callback(data);
        },
        'error': function () {
            error();
        }
    });
}

function PNCustomGet(url, token, callback, error) {
    $.ajax(url, {
        'headers': {
            'access_token': token,
        },
        'type': 'GET',
        'processData': false,
        'contentType': 'application/json',
        'success': function (data) {
            callback(data);
        },
        'error': function () {
            error();
        }
    });
}