﻿var dataLoadingTemplate = '<div class="data-paging-loading" style="text-align:center;">' +
    '<img style="width:50px;" src="/Images/page-loading.gif" />' +
    '<p style="margin-top:5px; color:dodgerblue; text-align:center;">Đang tải dữ liệu...</p>' +
    '</div>';

var dataPagingSearchTemplate = '<div class="box-search-video" style="margin-top:10px;">' +
    '<div style="margin-right:80px;">' +
    '<input class="form-control paging-search-keyword" placeholder="{hd}" style="font-style:italic;" />' +
    '</div>' +
    '<button class="btn btn-warning paging-search-btn" style="float:right; margin-top:-44px;"><i class="bi bi-search"></i></button>' +
    '</div>';
var dataPagingBredrum = '<p style="color:red; margin-top:5px;"><span style="color:blue;">Từ khoá: </span><span>{q}</span></p>'

var dataPagingBodyBox = '<div class="paging-body">' +
    '</div>';

var dataPagingPage = '<ul class="pagination ajax-paging-page" style="margin-top:20px;">' +
    '<li class="page-item previous"><a href="#" class="page-link"><i class="previous"></i></a></li>' +
    '<li class="page-item"><a href="#" class="page-link">x</a></li>' +
    '<li class="page-item"><a href="#" class="page-link">x</a></li>' +
    '<li class="page-item"><a href="#" class="page-link">x</a></li>' +
    '<li class="page-item"><a href="#" class="page-link">x</a></li>' +
    '<li class="page-item"><a href="#" class="page-link">x</a></li>' +
    '<li class="page-item next"><a href="#"  class="page-link"><i class="next"></i></a></li>' +
    '</ul>';

function PNAjaxPaging(config) {
    this.config = config;
    this.cpage = 1;
    this.query = "";
    var me;
    var doSearch = function (url) {
        $("#" + me.config.boxId).empty();
        $("#" + me.config.boxId).append(dataLoadingTemplate);
        PNGet(url, function (data) {
            var list = JSON.parse(data);

            $("#" + me.config.boxId).find(".data-paging-loading").remove();
            var shtml = dataPagingSearchTemplate.replace("{hd}", me.config.searchHolder);
            $("#" + me.config.boxId).append(shtml);
            $("#" + me.config.boxId).find(".paging-search-btn").click(function () {
                var keyword = $("#" + me.config.boxId).find(".paging-search-keyword").val();
                me.doQuery(1, keyword);
            });

            $("#" + me.config.boxId).find(".paging-search-keyword").val(me.query);
            if (me.query) {
                var html = dataPagingBredrum.replace("{q}", me.query);
                $("#" + me.config.boxId).append(html);
            }

            me.config.renderUI(list);

            $("#" + me.config.boxId).append(dataPagingPage);

            var paging = $("#" + me.config.boxId).find(".ajax-paging-page");
            var lps = $(paging).find("li");
            var first = lps[0];
            var one = lps[1];

            var min = 0;
            var max = 0;
            if (me.cpage <= 3) {
                min = 1;
                max = 5;
            }
            else {
                min = me.cpage - 2;
                max = me.cpage + 2;
            }

            var ci = min;
            for (var i = 1; i < lps.length - 1; i++) {
                if (ci == me.cpage) {
                    $(lps[i]).addClass("active");
                }
                $(lps[i]).find("a").text(ci);
                ci++;
            }
            if (me.cpage == 1) {
                $(lps[0]).addClass("disabled");
            }

            $("#" + me.config.boxId).find(".paging-search-keyword").keyup(function (event) {
                if (event.keyCode === 13) {
                    var keyword = $("#" + me.config.boxId).find(".paging-search-keyword").val();
                    me.doQuery(1, keyword);
                }
            });

            $(paging).find("li").click(function () {
                var keyword = $("#" + me.config.boxId).find(".paging-search-keyword").val();
                var cls = $(this).attr("class");
                var ptext = $(this).find("a").text();
                if (ptext) {
                    var vp = parseInt(ptext);
                    me.doQuery(vp, keyword);
                }
                else {
                    var index = cls.indexOf('previous');
                    if (index > 0 && me.cpage > 1) {
                        var vp = me.cpage - 1;
                        me.doQuery(vp, keyword);
                    }
                    else {
                        var index2 = cls.indexOf('next');
                        if (index2 > 0) {
                            var vp = me.cpage + 1;
                            me.doQuery(vp, keyword);
                        }
                    }
                }

                return false;
            });

            if (me.config.renderUICompleted) {
                me.config.renderUICompleted();
            }

        }, function () { alert("Lỗi"); });
    }
    this.init = function () {
        this.query = "";
        var url = this.config.baseURL + "/" + this.cpage;
        this.cpage = 1;
        me = this;
        doSearch(url);
    }

    this.reload = function () {
        this.doQuery(this.page, this.query);
    }

    this.doQuery = function (page, query) {
        this.cpage = page;
        this.query = query;
        var url = this.config.baseURL + "/" + this.cpage + "/" + query;
        me = this;
        doSearch(url);
    }
}