﻿function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function digitsOnly(obj) {
    obj.value = obj.value.replace(/\D/g, "");
}
function replaceAll(str, find, replace) {
    return str.split(find).join(replace);
}

function removeInvalidNumberCharacters(input) {
    var str = '';
    for (var i = 0; i < input.length; i++) {
        var code = input.charCodeAt(i);
        if (code == 44 || code == 46 || (code >= 48 && code <= 57)) {
            str = str + input[i];
        }
    }
    return str;
}

$(document).ready(function () {
    $('.pn-number').keyup(function (event) {       
        var text = $(this).val();
        text = removeInvalidNumberCharacters(text);
        text = text.split(",").join("");
        text = text.split(".").join("");
        var num = numberWithCommas(text);
        $(this).val(num);
    });
});