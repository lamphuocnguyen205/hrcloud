﻿
{
    "data": [
      {
          "name": "Tạp Hóa Yến Phi - Nguyễn Văn Quá",
          "checkins": 1,
          "phone": "08 5436 9441",
          "id": "109803426237429",
          "link": "https://www.facebook.com/109803416237430",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.8394,
              "longitude": 106.629997,
              "street": "90/1, Nguyễn Văn Quá, Q12"
          }
      },
      {
          "name": "Tạp Hóa Ngọc Trâm - Đông Hưng Thuận 2",
          "checkins": 6,
          "id": "420953854933021",
          "link": "https://www.facebook.com/420953848266355",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.835096,
              "longitude": 106.627296,
              "street": "1/170, Đông Hưng Thuận 2"
          }
      },
      {
          "name": "Tạp hóa Bắp Lùn",
          "checkins": 20,
          "phone": "0908184764",
          "id": "558206061268531",
          "link": "https://www.facebook.com/558206037935200",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.837036280878,
              "longitude": 106.62752866745,
              "street": "A167 Bis Khu Phố 2, Đông Hưng Thuận",
              "zip": "700000"
          }
      },
      {
          "name": "SHOP Tạp Hóa Bột Nguyên Chất",
          "checkins": 8,
          "phone": "0788502032",
          "id": "2335254376691918",
          "link": "https://www.facebook.com/2335254193358603",
          "location": {
              "city": "Buôn Dak (1)",
              "country": "Vietnam",
              "latitude": 10.835096768694,
              "longitude": 106.62654085263,
              "street": "thôn tập lập, eakpam",
              "zip": "70000"
          }
      },
      {
          "name": "Tiệm tạp hoá CÔ TIÊN XANH",
          "checkins": 241,
          "website": "https://shopee.vn/fairyworldshop",
          "id": "1951094571637267",
          "link": "https://www.facebook.com/1951094448303946",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.827707377505,
              "longitude": 106.640551245,
              "street": "Tân Bình"
          }
      },
      {
          "name": "Tiệm tạp hóa Hoa Kỳ",
          "checkins": 327,
          "phone": "0908521863",
          "id": "257244634944793",
          "link": "https://www.facebook.com/257244631611460",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.82689,
              "located_in": "1600233556916008",
              "longitude": 106.67898,
              "street": "1050/27 Quang Trung, Phường 8, Quận Gò Vấp"
          }
      },
      {
          "name": "Tạp hóa nhỏ, bán bằng sự tử tế",
          "checkins": 29,
          "website": "www.taphoanho.com.vn",
          "phone": "0913 587820",
          "id": "104947021202436",
          "link": "https://www.facebook.com/104946824535789",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.8099,
              "longitude": 106.58915,
              "street": "Bình Thạnh",
              "zip": "084"
          }
      },
      {
          "name": "Tạp hoá Cô Năm",
          "checkins": 121,
          "phone": "+84909768996",
          "id": "164491731579262",
          "link": "https://www.facebook.com/105082497520186",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.765,
              "longitude": 106.65,
              "street": "Lê Thị Bạch Cát"
          }
      },
      {
          "name": "Tạp hóa trái cây - Fruit Land",
          "checkins": 54,
          "phone": "+84704637080",
          "id": "112661163685117",
          "link": "https://www.facebook.com/112660217018545",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.76449,
              "longitude": 106.64218
          }
      },
      {
          "name": "Tạp Hoá Chi Chi",
          "checkins": 2,
          "phone": "0964069345",
          "id": "1711620812291114",
          "link": "https://www.facebook.com/1711620805624448",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.8373287,
              "longitude": 106.6276001
          }
      },
      {
          "name": "Tạp hóa Nhi Phong",
          "checkins": 19,
          "phone": "+84932256576",
          "id": "194630978667374",
          "link": "https://www.facebook.com/103535481110258",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.812229324067,
              "longitude": 106.70613520296,
              "street": "109H2 Chu Văn An. Đường số 5. Phường 26. Quận Bình Thạnh"
          }
      },
      {
          "name": "TẠP HÓA NHÀ GẤU",
          "checkins": 5,
          "phone": "+84339585832",
          "id": "101361034927176",
          "link": "https://www.facebook.com/100579055005374",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.83552,
              "longitude": 106.6288,
              "street": "325/2 Nguyễn Văn Quá, P.Đông Hưng Thuân Q.12",
              "zip": "70000"
          }
      },
      {
          "name": "TẠP HOÁ MÁ BRIAN",
          "checkins": 1,
          "phone": "0908447170",
          "id": "100337691631233",
          "link": "https://www.facebook.com/100337531631249",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.81058,
              "longitude": 106.63349
          }
      },
      {
          "name": "Tạp Hóa Mẹ Na",
          "checkins": 18,
          "phone": "0963253625",
          "id": "1708916922464132",
          "link": "https://www.facebook.com/859217174100782",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.8342,
              "longitude": 106.64493,
              "street": "647 Tân Sơn, P12, Gò Vấp"
          }
      },
      {
          "name": "Tiệm Tạp Hoá Của Nhà NẮNG THẢO MỘC",
          "checkins": 4,
          "website": "https://nangthaomoc.vn",
          "phone": "+84888123242",
          "id": "115584230046465",
          "link": "https://www.facebook.com/102330594705162",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.82118,
              "longitude": 106.6395,
              "street": "141 Tân Sơn, Phường 15, quận tân bình, TP. Hồ Chí Minh"
          }
      },
      {
          "name": "Tạp Hóa Ji Jang",
          "checkins": 394,
          "website": "https://www.instagram.com/jijang.sg/",
          "phone": "0927997998",
          "id": "378879006204259",
          "link": "https://www.facebook.com/378878832870943",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.8060485,
              "longitude": 106.601765,
              "street": "28 đường số 5 , KDC Bình Hưng , xã Bình Hưng , huyện Bình Chánh",
              "zip": "700000"
          }
      },
      {
          "name": "Tạp Hóa Nhà Pie",
          "checkins": 197,
          "phone": "0907097662",
          "id": "113510870291194",
          "link": "https://www.facebook.com/113510666957881",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.7439,
              "longitude": 106.6195,
              "street": "4J Hoàng Hưng"
          }
      },
      {
          "name": "Tạp Hóa Chú Cụi",
          "checkins": 284,
          "phone": "+84933946638",
          "id": "108573050677381",
          "link": "https://www.facebook.com/107800360754650",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.77035,
              "longitude": 106.68655,
              "street": "173/2 Nguyễn Thị Minh Khai, P.Phạm Ngũ Lão, Q.1",
              "zip": "7000000"
          }
      },
      {
          "name": "An's House - Tiệm tạp hóa Nhà An",
          "checkins": 587,
          "phone": "+84901149811",
          "id": "308427009855293",
          "link": "https://www.facebook.com/308426879855306",
          "location": {
              "city": "Gò Vấp",
              "country": "Vietnam",
              "latitude": 10.823202530771,
              "longitude": 106.68962951349,
              "street": "62/10 Huỳnh Khương An"
          }
      },
      {
          "name": "Tạp hóa Nessie",
          "checkins": 0,
          "id": "577139059318653",
          "link": "https://www.facebook.com/295664497466112",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.84259,
              "longitude": 106.63079,
              "street": "HAI BÀ TRƯNG"
          }
      },
      {
          "name": "Tạp Hoá ÔNG TƯ RÂU.",
          "checkins": 5,
          "phone": "+84779689354",
          "id": "104288827764685",
          "link": "https://www.facebook.com/104277247765843",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.84378404989,
              "longitude": 106.66621532165,
              "street": "Đường Thống Nhất F16 Quận Gò Vấp"
          }
      },
      {
          "name": "Tạp hóa Ba Thuận",
          "checkins": 51,
          "phone": "+84837173487",
          "id": "364592760608404",
          "link": "https://www.facebook.com/364592753941738",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.865603,
              "longitude": 106.649774,
              "street": "104-106, Lê Văn Khương, Q12"
          }
      },
      {
          "name": "Tạp Hóa Nhà Khuyên",
          "checkins": 111,
          "phone": "0979357001",
          "id": "112126283505290",
          "link": "https://www.facebook.com/112126073505311",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.81182,
              "longitude": 106.62416,
              "street": "123 Nguyễn Hữu Tiến, Phường Tây Thạnh, Quận Tân Phú"
          }
      },
      {
          "name": "Tập Đoàn Vệ Sỹ Hoàng Bảo Long",
          "checkins": 471,
          "website": "http://www.hoangbaolong.com",
          "phone": "+841900234578",
          "id": "1463371757118999",
          "link": "https://www.facebook.com/351712591618260",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.84855,
              "longitude": 106.67865,
              "street": "578 nguyễn oanh",
              "zip": "700000"
          }
      },
      {
          "name": "Tiệm Tạp Hoá Thuận Mập",
          "checkins": 957,
          "id": "624146297997938",
          "link": "https://www.facebook.com/624146294664605",
          "location": {
              "city": "Ho Chi Minh City",
              "country": "Vietnam",
              "latitude": 10.761485304634,
              "longitude": 106.69776472965
          }
      }
    ],
    "paging": {
        "cursors": {
            "after": "MjQZD"
        },
      "next": "https://graph.facebook.com/v7.0/search?access_token=EAAHuWjRgn3kBADg1eZCe2a7XHZBhE1ZBEnoE9zYl2cKNo2CEZCGaFH6AGH2wNt5SaaPpnEBaq678XA3wqC1G5FZA3zfgHcSWIDRqswAGTRAF0XruBhqPymp7nS8U6gi2ZAezZCVFMaleEew97lWpJUe6W3b7AKt4rDZCJtxEGMuJQUwvZC8FZBn3rAXNMjVWmIjaG4QDfG1ZBZBNvFW7LUGFEfemgG4DS2tbD9xmuCEHaZA6kbAZDZD&pretty=0&fields=name%2Ccheckins%2Cwebsite%2Cphone%2Cid%2Clink%2Clocation&q=tap+hoa&type=place&center=10.8393801%2C106.6304705&limit=25&after=MjQZD"
    }
}