﻿function loadShifts(callback) {
    var url = host + "/api/main/get-all-shifts";
    var dataKey = "data_shifts";
    var changeKey = "change_shifts";
    var localChange = readCacheChange(changeKey);
    var serverChange = latestChanges.ShiftChanged;
    if (localChange < serverChange || localChange == 0) {
        PNCustomGet(url, access_token, function (data) {
            if (data == "400") {
                showRetry();
            }
            else {

                var list = JSON.parse(data);
                localStorage.setItem(dataKey, data);
                writeCacheChange(changeKey, serverChange);

                callback(list);
            }
        }, function () { callback(new Array()); });
    }
    else {
        var text = localStorage.getItem(dataKey);
        if (text)
            callback(JSON.parse(text));
        else
            callback(new Array());
    }
}
