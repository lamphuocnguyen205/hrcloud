﻿$(document).ready(function () {
    var page = $("#PNPageVal").val();
    var middle = $("#PNMiddleVal").val();
    PNPaging(page, middle);
});

function PNPaging(page, middle) {
    var fullURL = "";
    var currentPage = parseInt(page);
    var mpage = parseInt(middle);
    fullURL = window.location.toString().toLowerCase();
    var index = fullURL.indexOf("page=");
    if (index < 0) {
        var findand = fullURL.indexOf("?");
        var c = findand < 0 ? "?" : "&";
        $("#fpage").attr("href", fullURL + c + "page=1");
        $("#1page").attr("href", fullURL + c + "page=1");
        $("#2page").attr("href", fullURL + c + "page=2");
        $("#3page").attr("href", fullURL + c + "page=3");
        $("#4page").attr("href", fullURL + c + "page=4");
        $("#5page").attr("href", fullURL + c + "page=5");
        $("#next-page").attr("href", fullURL + "page=2");
    }
    else {
        var findand = fullURL.indexOf("&", index);
        if (findand < 0) {
            var begin = fullURL.substr(0, index);
            $("#fpage").attr("href", begin + "page=1");
            $("#1page").attr("href", begin + "page=" + (mpage - 2));
            $("#2page").attr("href", begin + "page=" + (mpage - 1));
            $("#3page").attr("href", begin + "page=" + mpage);
            $("#4page").attr("href", begin + "page=" + (mpage + 1));
            $("#5page").attr("href", begin + "page=" + (mpage + 2));
            $("#next-page").attr("href", begin + "page=" + (currentPage + 1));
        }
        else {
            var begin = fullURL.substr(0, index);
            var end = fullURL.substr(findand, fullURL.length - findand);
            $("#fpage").attr("href", begin + "page=1" + end);
            $("#1page").attr("href", begin + "page=" + (mpage - 2) + end);
            $("#2page").attr("href", begin + "page=" + (mpage - 1) + end);
            $("#3page").attr("href", begin + "page=" + mpage + end);
            $("#4page").attr("href", begin + "page=" + (mpage + 1) + end);
            $("#5page").attr("href", begin + "page=" + (mpage + 2) + end);
            $("#next-page").attr("href", begin + "page=" + (currentPage + 1) + end);
        }
    }
}