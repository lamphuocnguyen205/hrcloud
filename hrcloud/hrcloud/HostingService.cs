﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrcloud
{
    public static class HostingService
    {
        public static void CreateHostingUser(EF.QUser user)
        {
            var owner = CacheCenter.DomainMapping[user.Domain];
            AMUser amuser = new AMUser();
            amuser.Username = user.QUserId;
            amuser.Role = user.Role;
            amuser.OwnerId = user.OwnerId;
            amuser.Domain = user.Domain;
            amuser.FirstName = user.FirstName;
            amuser.LastName = user.LastName;
            var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(amuser);
            NetworkService.callPostAPI(owner.Server.URL + "/api/main/create-user", jdata, (result) => {
                string t = result;
                int y = 0;
            }, () => {
            
            });
        }
    }
}