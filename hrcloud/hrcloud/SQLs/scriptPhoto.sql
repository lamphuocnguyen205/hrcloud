USE [HrWidget]
GO
/****** Object:  Table [dbo].[EPhoto]    Script Date: 23/8/2021 10:48:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EPhoto](
	[PhotoId] [bigint] IDENTITY(1,1) NOT NULL,
	[Path] [nvarchar](100) NOT NULL,
	[Location] [geography] NULL,
	[Note] [nvarchar](100) NULL,
	[DateVal] [bigint] NOT NULL,
	[IsTakePhoto] [bit] NOT NULL,
 CONSTRAINT [PK_EPhoto] PRIMARY KEY CLUSTERED 
(
	[PhotoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Event]    Script Date: 23/8/2021 10:48:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[EventId] [uniqueidentifier] NOT NULL,
	[OTime] [bigint] NOT NULL,
	[CreateTime] [smalldatetime] NOT NULL,
	[IsFuel] [bit] NOT NULL,
	[Value] [int] NOT NULL,
	[Km] [int] NULL,
	[AvLast] [int] NOT NULL,
	[AvTotal] [int] NOT NULL,
	[Location] [geography] NULL,
	[Photo] [nvarchar](1000) NULL,
	[Day] [int] NOT NULL,
	[Note] [nvarchar](100) NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EventDetail]    Script Date: 23/8/2021 10:48:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventDetail](
	[ERecordId] [uniqueidentifier] NOT NULL,
	[EventId] [uniqueidentifier] NOT NULL,
	[CreateDate] [smalldatetime] NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[Photo] [nvarchar](1000) NULL,
	[Value] [int] NOT NULL,
	[IsPay] [bit] NOT NULL,
	[SearchText] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_EventDetail] PRIMARY KEY CLUSTERED 
(
	[ERecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EPhoto] ADD  CONSTRAINT [DF_EPhoto_DateVal]  DEFAULT ((0)) FOR [DateVal]
GO
ALTER TABLE [dbo].[EPhoto] ADD  CONSTRAINT [DF_EPhoto_IsTakePhoto]  DEFAULT ((1)) FOR [IsTakePhoto]
GO
ALTER TABLE [dbo].[Event] ADD  CONSTRAINT [DF_Event_IsFuel]  DEFAULT ((1)) FOR [IsFuel]
GO
ALTER TABLE [dbo].[Event] ADD  CONSTRAINT [DF_Event_Value]  DEFAULT ((0)) FOR [Value]
GO
ALTER TABLE [dbo].[Event] ADD  CONSTRAINT [DF_Event_Km]  DEFAULT ((0)) FOR [Km]
GO
ALTER TABLE [dbo].[Event] ADD  CONSTRAINT [DF_Event_AvLast]  DEFAULT ((0)) FOR [AvLast]
GO
ALTER TABLE [dbo].[Event] ADD  CONSTRAINT [DF_Event_AvTotal]  DEFAULT ((0)) FOR [AvTotal]
GO
ALTER TABLE [dbo].[EventDetail] ADD  CONSTRAINT [DF_EventDetail_Value]  DEFAULT ((0)) FOR [Value]
GO
ALTER TABLE [dbo].[EventDetail] ADD  CONSTRAINT [DF_EventDetail_IsPay]  DEFAULT ((1)) FOR [IsPay]
GO
