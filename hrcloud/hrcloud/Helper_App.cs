﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;

namespace hrcloud
{
    public static class Helper_App
    {
        public static string ToSystemPath(string path)
        {
            return HostingEnvironment.MapPath(path);
        }

        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        public static string GetFileNameFromTemphotoFolder(string path)
        {
            try
            {
                return path.Substring(11, path.Length - 11);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string EncryptString(string key, string plainInput)
        {
            byte[] iv = new byte[16];
            byte[] array;
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainInput);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }

        public static string DecryptString(string key, string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

        public static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }

        public static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }

        public static string PNEncode(string input)
        {
            var arr = Encoding.UTF8.GetBytes(input);
            string temp = "";
            foreach (var item in arr)
            {
                var tn = item.ToString();
                int v;
                int.TryParse(tn, out v);
                if (v < 10)
                    temp = temp + "00" + v.ToString();
                else if (v < 100)
                    temp = temp + "0" + v.ToString();
                else
                    temp = temp + v.ToString();
            }
            var res = Hvi(temp);
            return res;
        }

        public static string PNDecode(string input)
        {
            var hvi = Hvi(input);
            List<byte> l = new List<byte>();
            for (int i = 0; i < hvi.Length; i = i + 3)
            {
                var str = hvi.Substring(i, 3);
                var b = BitConverter.GetBytes(PNUtility.Helpers.UtilityHelper.ParseInt(str))[0];
                l.Add(b);
            }
            var a = l.ToArray();
            return Encoding.UTF8.GetString(a);
        }

        public static string Hvi(string input)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < input.Length; i = i + 2)
            {
                if (i + 1 < input.Length)
                {
                    sb.Append(input[i + 1]);
                    sb.Append(input[i]);
                }
                else
                {
                    if (i == input.Length - 1)
                        sb.Append(input[i]);
                }
            }
            return sb.ToString();
        }

        public static void MoveFile(string temp, string taget)
        {
            try
            {
                System.IO.File.Move(ToSystemPath(temp), ToSystemPath(taget));
            }
            catch (Exception) { }
        }

        public static void CopyFile(string temp, string taget)
        {
            try
            {
                System.IO.File.Copy(ToSystemPath(temp), ToSystemPath(taget));
            }
            catch (Exception) { }
        }

        public static void DeleteFile(string filePath)
        {
            MyTasks.FileTask.AddTask((a, k) => {
                try
                {
                    var spath = ToSystemPath(k);
                    if (System.IO.File.Exists(spath))
                    {
                        System.IO.File.Delete(spath);
                    }
                }
                catch (Exception eeee) {
                    int u = 0;
                
                }
            }, filePath, new object[] { });
        }

        public static void DeleteFolder(string folderPath)
        {
            MyTasks.FileTask.AddTask((a, k) => {
                try
                {
                    var spath = ToSystemPath(k);
                    if (System.IO.Directory.Exists(spath))
                    {
                        System.IO.Directory.Delete(spath, true);
                    }
                }
                catch (Exception eeee)
                {
                    int u = 0;

                }
            }, folderPath, new object[] { });
        }

        public static void WriteTextFile(string folderPath, string fileName, string data)
        {
            var sysPath = ToSystemPath(folderPath);
            if (!System.IO.Directory.Exists(sysPath))
            {
                System.IO.Directory.CreateDirectory(sysPath);
            }
            System.IO.File.WriteAllText(sysPath + "/" + fileName, data);
        }

        public static System.Drawing.Image ResizeImage(System.Drawing.Image imgToResize, Size size)
        {
            //Get the image current width  
            int sourceWidth = imgToResize.Width;
            //Get the image current height  
            int sourceHeight = imgToResize.Height;
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;
            //Calulate  width with new desired size  
            nPercentW = ((float)size.Width / (float)sourceWidth);
            //Calculate height with new desired size  
            nPercentH = ((float)size.Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;
            //New Width  
            int destWidth = (int)(sourceWidth * nPercent);
            //New Height  
            int destHeight = (int)(sourceHeight * nPercent);
            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.Default;
            // Draw image with new width and height  
            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();
            return (System.Drawing.Image)b;
        }

        public static Image CropImage(Image source, int width, int height)
        {
            var th = width * source.Height / source.Width;
            if (th >= height)
            {
                var resize = ResizeImage(source, new Size(width, th));
                int o = (th - height) / 2;
                Bitmap target = new Bitmap(width, height);
                Rectangle cropRect = new Rectangle(0, o, width, height);
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.DrawImage(resize, new Rectangle(0, 0, target.Width, target.Height),
                                     cropRect,
                                     GraphicsUnit.Pixel);
                }
                return target;
            }
            else
            {
                var tw = height * source.Width / source.Height;
                var resize = ResizeImage(source, new Size(tw, height));
                var o = (tw - width) / 2;
                Bitmap target = new Bitmap(width, height);
                Rectangle cropRect = new Rectangle(o, 0, width, height);
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.DrawImage(resize, new Rectangle(0, 0, target.Width, target.Height),
                                     cropRect,
                                     GraphicsUnit.Pixel);
                }
                return target;
            }
        }
    }
}