﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hrcloud
{
    public class DataHelper
    {
        public static int? IntNParse(string val)
        {
            if (string.IsNullOrEmpty(val))
                return null;
            else
            {
                int v = 0;
                int.TryParse(val, out v);
                return v;
            }
        }

        public static int IntParse(string val, int defaultVal)
        {
            if (string.IsNullOrEmpty(val))
                return defaultVal;
            else
            {
                int v = 0;
                int.TryParse(val, out v);
                return v;
            }
        }

        public static float? FloatNParse(string val)
        {
            if (string.IsNullOrEmpty(val))
                return null;
            else
            {
                //val = val.Replace('.', ',');
                val = val.Replace(',', '.');
                float v = 0;
                float.TryParse(val, out v);
                return v;
            }
        }

        public static int? FloatN100Parse(string val)
        {
            var value = FloatNParse(val);
            return value.HasValue ? (int)(Math.Round(value.Value, 2) * 100) : (int?)null;
        }

        public static double? DoubleNParse(string val)
        {
            if (string.IsNullOrEmpty(val))
                return null;
            else
            {
                //val = val.Replace('.', ',');
                val = val.Replace(',', '.');
                double v = 0;
                double.TryParse(val, out v);
                return v;
            }
        }

        public static double DoubleParse(string val, double defaultVal)
        {
            if (string.IsNullOrEmpty(val))
                return defaultVal;
            else
            {
                double v = 0;
                double.TryParse(val, out v);
                return v;
            }
        }

        public static byte? ByteNParse(string val)
        {
            if (string.IsNullOrEmpty(val))
                return null;
            else
            {
                byte v = 0;
                byte.TryParse(val, out v);
                return v;
            }
        }

        public static byte ByteParse(string val, byte defaultVal)
        {
            if (string.IsNullOrEmpty(val))
                return defaultVal;
            else
            {
                byte v = 0;
                byte.TryParse(val, out v);
                return v;
            }
        }

        public static bool? BoolNParse(string val)
        {
            if (string.IsNullOrEmpty(val))
                return (bool?)null;
            else
            {
                return val.ToLower() == "true";
            }
        }

        public static bool BoolParse(string val)
        {
            if (string.IsNullOrEmpty(val))
                return false;
            else
            {
                return val.ToLower() == "true";
            }
        }

        public static decimal DecimalParse(string val, decimal defaultVal)
        {
            if (string.IsNullOrEmpty(val))
                return defaultVal;
            else
            {
                decimal v = 0;
                decimal.TryParse(val, out v);
                return v;
            }
        }

        public static DbGeography CreatePoint(double lat, double lon, int srid = 4326)
        {
            String lonx = lon.ToString(CultureInfo.InvariantCulture);
            String latx = lat.ToString(CultureInfo.InvariantCulture);
            return DbGeography.PointFromText(string.Format("POINT({0} {1})", lonx, latx), DbGeography.DefaultCoordinateSystemId);
        }
    }
}
