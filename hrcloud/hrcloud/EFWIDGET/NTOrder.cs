//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hrcloud.EFWIDGET
{
    using System;
    using System.Collections.Generic;
    
    public partial class NTOrder
    {
        public System.Guid NTOrderID { get; set; }
        public System.Guid CustomerId { get; set; }
        public int Plastic { get; set; }
        public int WeightInput { get; set; }
        public int WeightOutput { get; set; }
        public byte Status { get; set; }
        public decimal Price { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> CompletedDate { get; set; }
        public string CreateBy { get; set; }
        public string Photo { get; set; }
        public string Note { get; set; }
        public System.DateTime DateIn { get; set; }
        public string SearchData { get; set; }
        public int CountIn { get; set; }
        public int CountOut { get; set; }
        public int CBack { get; set; }
        public int LoseCount { get; set; }
        public string LoseReason { get; set; }
    }
}
