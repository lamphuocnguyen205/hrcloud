﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace hrcloud
{
    public static class Helper
    {
        public static float ParseFloat(string input)
        {
            var text = input.Replace('.', ',');
            return float.Parse(input);
        }
    }
}