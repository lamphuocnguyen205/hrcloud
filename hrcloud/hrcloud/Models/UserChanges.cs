﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrcloud.Models
{
    public class UserChanges
    {
        public long InfoChanged { get; set; }
        public long WorkingChanged { get; set; }
        public long FeedChanged { get; set; }
    }
}