﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrcloud.Models
{
    public class ExtraFunctionModel
    {
        public string Name { get; set; }
        public List<WMenu> TopMenu { get; set; }
        public List<WMenu> ActionMenu { get; set; }
        public List<WMenu> SettingMenu { get; set; }
    }
}