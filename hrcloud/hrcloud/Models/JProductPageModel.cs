﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hrcloud.Models
{
    public class JProductPageModel
    {
        public List<EF.Folder> Folders { get; set; }
        public List<EF.Tag> Tags { get; set; }
    }
}
