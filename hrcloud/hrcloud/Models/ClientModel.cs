﻿using hrcloud.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrcloud
{
    public class ClientModel
    {
        public string Username { get; set; }
        public string Domain { get; set; }
        public string HostURL { get; set; }
        public string Token { get; set; }
        public long Timestamp { get; set; }
        public EF.QUser QUser { get; set; }
        public EF.Owner Owner { get; set; }

        public ChangeDataModel Changes { get; set; }

        public UserChanges UChanges { get; set; }

        public string ToChangesText()
        {
            return PNUtility.Helpers.JSONHelper.Jsoner.Serialize(Changes);
        }
    }
}