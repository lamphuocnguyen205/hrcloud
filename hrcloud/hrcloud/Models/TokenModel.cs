﻿using hrcloud.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrcloud
{
    public class TokenModel
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public long Create { get; set; }
        public Guid OwnerId { get; set; }
        public string Domain { get; set; }

    }
}