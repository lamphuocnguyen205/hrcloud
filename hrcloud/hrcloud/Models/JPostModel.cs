﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hrcloud.Models
{
    public class JPostModel
    {
        public string Detail { get; set; }
        public string Note { get; set; }
        public string Photo { get; set; }
        public string Videos { get; set; }
        public string Files { get; set; }
        public string Links { get; set; }
    }
}
