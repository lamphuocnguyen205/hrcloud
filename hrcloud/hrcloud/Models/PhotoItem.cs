﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace hrcloud.Models
{
    public class PhotoItem
    {
        public string URL { get; set; }
        public string Desc { get; set; }
        public bool IsTempPath()
        {
            if (URL.Substring(0, 2) == "/T")
            {
                return true;
            }
            else
                return false;
        }

        public string GetFileName()
        {
            if (URL.Length > 40)
                return URL.Substring(URL.Length - 40, 36);
            else
                return "";
        }

        public void MoveToDataFolder(string domain)
        {
            var fileName = Helper_App.GetFileNameFromTemphotoFolder(URL);
            var newPath = "/UData/" + domain + "/images/" + fileName;
            Helper_App.MoveFile(URL, newPath);
        }

        public void CreateThumnail(string domain, string id, string orginPath = "")
        {
            if (string.IsNullOrEmpty(orginPath))
            {
                var fileName = Helper_App.GetFileNameFromTemphotoFolder(URL);
                var newPath = "/I/" + domain + "/" + fileName;
                Image img = Image.FromFile(Helper_App.ToSystemPath(newPath));
                var newIMG = UpdateMultiPhotoModel.ResizeImage(img, new Size(100, 100));
                var path = "/Images/Thumnails/" + id + ".jpg";
                newIMG.Save(Helper_App.ToSystemPath(path));
            }
            else
            {
                Image img = Image.FromFile(Helper_App.ToSystemPath(orginPath));
                var newIMG = UpdateMultiPhotoModel.ResizeImage(img, new Size(100, 100));
                var path = "/Images/Thumnails/" + id + ".jpg";
                newIMG.Save(Helper_App.ToSystemPath(path));
            }
        }

        public void Remove()
        {
            Helper_App.DeleteFile(URL);
        }
    }
}
