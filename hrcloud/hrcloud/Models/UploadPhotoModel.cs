﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hrcloud.Models
{
    public class UploadPhotoModel
    {
        public string Pid { get; set; }
        public string Desc { get; set; }
        public long Time { get; set; }
        public string U { get; set; }
        public int W { get; set; }
        public int H { get; set; }

        public string GetPictureURL(string oid, string aid)
        {
            return "/UData/" + oid + "/albums/pictures/" + aid + "/" + Pid + ".jpg";
        }

        public string GetThumnailURL(string oid, string aid)
        {
            return "/UData/" + oid + "/albums/thumnails/" + aid + "/" + Pid + ".jpg";
        }
    }
}
