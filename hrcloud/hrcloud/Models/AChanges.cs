﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace hrcloud
{
    public class AChanges
    {
        public string Domain { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public long Time { get; set; }
        
        public List<AChanges> SubChanges { get; set; }
            
    }
}