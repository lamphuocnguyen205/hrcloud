﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;

namespace hrcloud.Models
{
    public class UpdateMultiPhotoModel
    {
        public List<PhotoItem> OldList { get; set; }
        public List<PhotoItem> NewList { get; set; }
        private string originText;
        private string newText;
        private string domain;
        private List<string> newImages;
        private List<string> removeImages;
        private string oid;
        public string FirstPhoto = "";

        public UpdateMultiPhotoModel(string oid, string oStr, string nStr)
        {
            this.newText = nStr;
            this.oid = oid;
            if (!string.IsNullOrEmpty(oStr))
            {
                OldList = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<PhotoItem>>(oStr);
            }
            else
                OldList = new List<PhotoItem>();

            if (!string.IsNullOrEmpty(nStr))
            {
                NewList = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<PhotoItem>>(nStr);
                if(NewList.Count > 0)
                {
                    var e = NewList.ElementAt(0);
                    if (e.IsTempPath())
                    {
                        var fileName = Helper_App.GetFileNameFromTemphotoFolder(e.URL);
                        FirstPhoto = fileName;
                    }
                    else
                        FirstPhoto = e.URL.Substring(e.URL.Length - 40, 40);
                }
            }
            else
                NewList = new List<PhotoItem>();

            var path = Helper_App.ToSystemPath("/UData/" + this.oid + "/images");
            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);
        }

        public static System.Drawing.Image ResizeImage(System.Drawing.Image imgToResize, Size size)
        {
            //Get the image current width  
            int sourceWidth = imgToResize.Width;
            //Get the image current height  
            int sourceHeight = imgToResize.Height;
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;
            //Calulate  width with new desired size  
            nPercentW = ((float)size.Width / (float)sourceWidth);
            //Calculate height with new desired size  
            nPercentH = ((float)size.Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;
            //New Width  
            int destWidth = (int)(sourceWidth * nPercent);
            //New Height  
            int destHeight = (int)(sourceHeight * nPercent);
            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.Default;
            // Draw image with new width and height  
            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();
            return (System.Drawing.Image)b;
        }

        public void ProcessImagesWithAvatar(string oid)
        {
            MyTasks.FileTask.AddTask((a, k) => {
                int j = 0;
                foreach (var item in OldList)
                {
                    var find = NewList.Where(i => i.URL == item.URL).FirstOrDefault();
                    if (find == null)
                    {
                        item.Remove();
                    }
                }
                foreach (var item in NewList)
                {
                    j++;
                    if (item.IsTempPath())
                    {
                        item.MoveToDataFolder(k);
                    }
                }
                //buildCoverPhoto(id);
            }, oid, new string[] { oid });

        }

        public void ProcessImagesNoAvatar()
        {
            MyTasks.FileTask.AddTask((a, k) => {
                int j = 0;
                foreach (var item in OldList)
                {
                    var find = NewList.Where(i => i.URL == item.URL).FirstOrDefault();
                    if (find == null)
                    {
                        item.Remove();
                    }
                }
                foreach (var item in NewList)
                {
                    j++;
                    if (item.IsTempPath())
                    {
                        item.MoveToDataFolder(k);
                    }
                }
            }, this.oid, new string[] { });

        }

        //private void buildCoverPhoto(string id)
        //{
        //    Bitmap target = new Bitmap(600, 400);
        //    var count = NewList.Count;
        //    if (count == 0)
        //    {

        //    }
        //    else if (count == 1)
        //    {
        //        var IM1 = NewList.ElementAt(0);
        //        var realPath1 = IM1.URL;
        //        if (IM1.IsTempPath())
        //            realPath1 = IM1.URL.Replace("TempPhoto", "I/" + textTime);

        //        var pic1 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath1));
        //        var resPic1 = CropImage(pic1, 600, 400);

        //        using (Graphics g = Graphics.FromImage(target))
        //        {
        //            g.Clear(Color.White);

        //            g.DrawImage(resPic1, new Rectangle(0, 0, 600, 400),
        //                             new Rectangle(0, 0, resPic1.Width, resPic1.Height),
        //                             GraphicsUnit.Pixel);
        //        }
        //        var encoder = ImageCodecInfo.GetImageEncoders()
        //                    .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
        //        var encParams = new EncoderParameters(1);
        //        encParams.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
        //        target.Save(Helper_App.ToSystemPath("/images/thumnails/" + id + ".jpg"), encoder, encParams);

        //    }
        //    else if (count == 2)
        //    {
        //        var IM1 = NewList.ElementAt(0);
        //        var realPath1 = IM1.URL;
        //        if (IM1.IsTempPath())
        //            realPath1 = IM1.URL.Replace("TempPhoto", "I/" + textTime);

        //        var IM2 = NewList.ElementAt(1);
        //        var realPath2 = IM2.URL;
        //        if (IM2.IsTempPath())
        //            realPath2 = IM2.URL.Replace("TempPhoto", "I/" + textTime);

        //        var pic1 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath1));
        //        var pic2 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath2));
        //        var resPic1 = CropImage(pic1, 299, 400);
        //        var resPic2 = CropImage(pic2, 299, 400);

        //        using (Graphics g = Graphics.FromImage(target))
        //        {
        //            g.Clear(Color.White);

        //            g.DrawImage(resPic1, new Rectangle(0, 0, 299, 400),
        //                             new Rectangle(0, 0, resPic1.Width, resPic1.Height),
        //                             GraphicsUnit.Pixel);
        //            g.DrawImage(resPic2, new Rectangle(301, 0, 299, 400),
        //                             new Rectangle(0, 0, resPic2.Width, resPic2.Height),
        //                             GraphicsUnit.Pixel);
        //        }
        //        var encoder = ImageCodecInfo.GetImageEncoders()
        //                    .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
        //        var encParams = new EncoderParameters(1);
        //        encParams.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
        //        target.Save(Helper_App.ToSystemPath("/images/thumnails/" + id + ".jpg"), encoder, encParams);
        //    }
        //    else if (count == 3)
        //    {
        //        var IM1 = NewList.ElementAt(0);
        //        var realPath1 = IM1.URL;
        //        if (IM1.IsTempPath())
        //            realPath1 = IM1.URL.Replace("TempPhoto", "I/" + textTime);

        //        var IM2 = NewList.ElementAt(1);
        //        var realPath2 = IM2.URL;
        //        if (IM2.IsTempPath())
        //            realPath2 = IM2.URL.Replace("TempPhoto", "I/" + textTime);

        //        var IM3 = NewList.ElementAt(2);
        //        var realPath3 = IM3.URL;
        //        if (IM3.IsTempPath())
        //            realPath3 = IM3.URL.Replace("TempPhoto", "I/" + textTime);

        //        var pic1 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath1));
        //        var pic2 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath2));
        //        var pic3 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath3));
        //        var resPic1 = CropImage(pic1, 299, 400);
        //        var resPic2 = CropImage(pic2, 299, 199);
        //        var resPic3 = CropImage(pic3, 299, 199);

        //        using (Graphics g = Graphics.FromImage(target))
        //        {
        //            g.Clear(Color.White);

        //            g.DrawImage(resPic1, new Rectangle(0, 0, 299, 400),
        //                             new Rectangle(0, 0, resPic1.Width, resPic1.Height),
        //                             GraphicsUnit.Pixel);
        //            g.DrawImage(resPic2, new Rectangle(301, 0, 299, 199),
        //                             new Rectangle(0, 0, resPic2.Width, resPic2.Height),
        //                             GraphicsUnit.Pixel);
        //            g.DrawImage(resPic3, new Rectangle(301, 201, 299, 199),
        //                             new Rectangle(0, 0, resPic3.Width, resPic3.Height),
        //                             GraphicsUnit.Pixel);
        //        }
        //        var encoder = ImageCodecInfo.GetImageEncoders()
        //                    .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
        //        var encParams = new EncoderParameters(1);
        //        encParams.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
        //        target.Save(Helper_App.ToSystemPath("/images/thumnails/" + id + ".jpg"), encoder, encParams);
        //    }
        //    else
        //    {
        //        var IM1 = NewList.ElementAt(0);
        //        var realPath1 = IM1.URL;
        //        if (IM1.IsTempPath())
        //            realPath1 = IM1.URL.Replace("TempPhoto", "I/" + textTime);

        //        var IM2 = NewList.ElementAt(1);
        //        var realPath2 = IM2.URL;
        //        if (IM2.IsTempPath())
        //            realPath2 = IM2.URL.Replace("TempPhoto", "I/" + textTime);

        //        var IM3 = NewList.ElementAt(2);
        //        var realPath3 = IM3.URL;
        //        if (IM3.IsTempPath())
        //            realPath3 = IM3.URL.Replace("TempPhoto", "I/" + textTime);

        //        var IM4 = NewList.ElementAt(3);
        //        var realPath4 = IM4.URL;
        //        if (IM4.IsTempPath())
        //            realPath4 = IM4.URL.Replace("TempPhoto", "I/" + textTime);

        //        var pic1 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath1));
        //        var pic2 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath2));
        //        var pic3 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath3));
        //        var pic4 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath4));
        //        var resPic1 = CropImage(pic1, 299, 199);
        //        var resPic2 = CropImage(pic2, 299, 199);
        //        var resPic3 = CropImage(pic3, 299, 199);
        //        var resPic4 = CropImage(pic4, 299, 199);
        //        target.MakeTransparent();
        //        using (Graphics g = Graphics.FromImage(target))
        //        {
        //            g.Clear(Color.White);

        //            g.DrawImage(resPic1, new Rectangle(0, 0, 299, 199),
        //                             new Rectangle(0, 0, resPic1.Width, resPic1.Height),
        //                             GraphicsUnit.Pixel);

        //            g.DrawImage(resPic4, new Rectangle(0, 201, 299, 199),
        //                             new Rectangle(0, 0, resPic4.Width, resPic4.Height),
        //                             GraphicsUnit.Pixel);

        //            g.DrawImage(resPic2, new Rectangle(301, 0, 299, 199),
        //                             new Rectangle(0, 0, resPic2.Width, resPic2.Height),
        //                             GraphicsUnit.Pixel);
        //            g.DrawImage(resPic3, new Rectangle(301, 201, 299, 199),
        //                             new Rectangle(0, 0, resPic3.Width, resPic3.Height),
        //                             GraphicsUnit.Pixel);

        //            if (count > 4)
        //            {
        //                g.FillEllipse(Brushes.White, new Rectangle(420, 270, 60, 60));
        //                var o = count - 4;
        //                g.DrawString("+" + o.ToString(), new Font("Arial", 30), Brushes.Red, 417, 280, StringFormat.GenericDefault);
        //            }
        //        }
        //        var encoder = ImageCodecInfo.GetImageEncoders()
        //                    .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
        //        var encParams = new EncoderParameters(1);
        //        encParams.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
        //        target.Save(Helper_App.ToSystemPath("/images/thumnails/" + id + ".jpg"), encoder, encParams);
        //    }
        //}

        //public static Image CropImage(Image source, int width, int height)
        //{
        //    var th = width * source.Height / source.Width;
        //    if (th >= height)
        //    {
        //        var resize = ResizeImage(source, new Size(width, th));
        //        int o = (th - height) / 2;
        //        Bitmap target = new Bitmap(width, height);
        //        Rectangle cropRect = new Rectangle(0, o, width, height);
        //        using (Graphics g = Graphics.FromImage(target))
        //        {
        //            g.DrawImage(resize, new Rectangle(0, 0, target.Width, target.Height),
        //                             cropRect,
        //                             GraphicsUnit.Pixel);
        //        }
        //        return target;
        //    }
        //    else
        //    {
        //        var tw = height * source.Width / source.Height;
        //        var resize = ResizeImage(source, new Size(tw, height));
        //        var o = (tw - width) / 2;
        //        Bitmap target = new Bitmap(width, height);
        //        Rectangle cropRect = new Rectangle(o, 0, width, height);
        //        using (Graphics g = Graphics.FromImage(target))
        //        {
        //            g.DrawImage(resize, new Rectangle(0, 0, target.Width, target.Height),
        //                             cropRect,
        //                             GraphicsUnit.Pixel);
        //        }
        //        return target;
        //    }
        //}

        public string GetResultText()
        {
            return newText.Replace("TempPhoto", "UData/" + this.oid + "/images");
        }
    }
}
