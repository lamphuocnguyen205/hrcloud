﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrcloud.Models.SonNguyen
{
    public class ChangeLog
    {
        public string PI { get; set; }
        public string U { get; set; }
        public int O { get; set; }
        public int N { get; set; }
        public long T { get; set; }
        public string Note { get; set; }
    }
}