﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrcloud.Models.SonNguyen
{
    public class HWUsers
    {
        public List<string> OutsideUsers;
        public List<string> ManageUsers;

        public HWUsers()
        {
            OutsideUsers = new List<string>();
            ManageUsers = new List<string>();
        }
    }
}