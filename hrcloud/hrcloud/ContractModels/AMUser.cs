﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrcloud
{
    public class AMUser
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
        public Guid OwnerId { get; set; }
        public string Domain { get; set; }
    }
}