﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace hrcloud.Services
{
    public static class LogService
    {
        public static void Log(string key, string message)
        {
            var path = HostingEnvironment.MapPath("/Logs/" + key + ".txt");            
            var text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - " + message;
            if (System.IO.File.Exists(path))
            {
                var list = System.IO.File.ReadAllLines(path).ToList();
                list.Add(text);
                var data = list.ToArray();
                System.IO.File.WriteAllLines(path, data);
            }
            else
            {
                var list = new List<string>();
                list.Add(text);
                var data = list.ToArray();
                System.IO.File.WriteAllLines(path, data);
            }
        }
    }
}