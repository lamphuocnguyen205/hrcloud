﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Hosting;
using hrcloud.Models;

namespace hrcloud
{
    public class CacheCenter
    {
        private static Dictionary<string, EF.Owner> domainDic;
        public static Dictionary<string, EF.Owner> DomainMapping
        {
            get
            {
                if(domainDic == null)
                {
                    using(var context = new EF.HrCenterEntities())
                    {
                        domainDic = context.Owner.Include(a => a.Server).Where(a=>a.Domain != "").ToDictionary((x) => { return x.Domain; });
                    }
                }
                return domainDic;
            }
            set
            {
                domainDic = value;
            }
        }

        private static Dictionary<string, EF.QUser> users;

        public static EF.QUser GetUserCached(string username)
        {
            if (users == null)
                users = new Dictionary<string, EF.QUser>();
            if (users.ContainsKey(username))
                return users[username];
            else
            {
                using(var context = new EF.HrCenterEntities())
                {
                    var quser = context.QUser.Where(a => a.QUserId == username).FirstOrDefault();
                    if (quser != null)
                    {
                        users.Add(username, quser);
                        return quser;
                    }
                    else
                        return null;
                }
            }
        }

        private static Dictionary<string, ChangeDataModel> dataChanges;

        public static ChangeDataModel GetChangeDataModel(string domain)
        {
            if(dataChanges == null)            
                dataChanges = new Dictionary<string, ChangeDataModel>();

            if (!dataChanges.ContainsKey(domain))
            {
                try
                {
                    var filePath = HostingEnvironment.MapPath("/App_Data/Changes/" + domain + ".txt");
                    var text = System.IO.File.ReadAllText(filePath);
                    var model = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<ChangeDataModel>(text);
                    dataChanges.Add(domain, model);
                }
                catch (Exception exxx) {
                
                }
            }
            
            var x = dataChanges.ContainsKey(domain) ? dataChanges[domain] : new ChangeDataModel();
            if (x == null)
                return new ChangeDataModel();
            else
                return x;
        }

        public static Dictionary<string, UserChanges> _userchangedMap;

        public static UserChanges GetUserChanges(string username)
        {
            if (_userchangedMap == null)
                _userchangedMap = new Dictionary<string, UserChanges>();
            if (_userchangedMap.ContainsKey(username))
                return _userchangedMap[username];
            else
            {
                var path = "/App_Data/UChanges/" + username + ".txt";
                var spath = HostingEnvironment.MapPath(path);
                try
                {
                    var text = System.IO.File.ReadAllText(spath);
                    var model = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<UserChanges>(text);
                    _userchangedMap.Add(username, model);
                    return model;
                }
                catch (Exception exxx)
                {
                    return new UserChanges();
                }
            }
        }

        public static void SaveUserChanges(string username, UserChanges model)
        {
            var path = "/App_Data/UChanges/" + username + ".txt";
            var spath = HostingEnvironment.MapPath(path);
            System.IO.File.WriteAllText(spath, PNUtility.Helpers.JSONHelper.Jsoner.Serialize(model));
        }

        private static Dictionary<string, List<Models.ExtraFunctionModel>> extraFunction;

        public static List<Models.ExtraFunctionModel> GetExtraFunction(string oid)
        {
            
            if (extraFunction == null)
                extraFunction = new Dictionary<string, List<ExtraFunctionModel>>();

            if (!extraFunction.ContainsKey(oid))
            {
                using (var context = new EF.HrCenterEntities())
                {
                    var guid = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                    var ex = context.Widget.Where(a => a.OwnerId == guid).ToList();
                    List<Models.ExtraFunctionModel> list = new List<ExtraFunctionModel>();
                    foreach (var item in ex)
                    {
                        ExtraFunctionModel model = new ExtraFunctionModel();
                        model.Name = item.Name;
                        model.TopMenu = string.IsNullOrEmpty(item.TopMenuData) ? new List<WMenu>() : PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<WMenu>>(item.TopMenuData);
                        model.ActionMenu = string.IsNullOrEmpty(item.ActionData) ? new List<WMenu>() : PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<WMenu>>(item.ActionData);
                        model.SettingMenu = string.IsNullOrEmpty(item.SettingData) ? new List<WMenu>() : PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<WMenu>>(item.SettingData);
                        list.Add(model);
                    }
                    if (list != null)
                    {
                        try
                        {
                            extraFunction.Add(oid, list);
                            return list;
                        }
                        catch
                        {
                            return list;
                        }
                    }
                    else
                        return null;
                }
            }
            else
            {
                if (extraFunction.ContainsKey(oid))
                    return extraFunction[oid];
                else
                    return null;
            }
        }

        private static Dictionary<string, Models.ExtraFunctionModel> extraUserFunction;

        private static int currentServer;

        public static int CurrentServerId
        {
            get
            {
                if (currentServer > 0)
                    return currentServer;
                else
                {
                    using(var context = new EF.HrCenterEntities())
                    {
                        var server = context.Server.FirstOrDefault();
                        if (server != null)
                            currentServer = server.ServerId;
                        else
                            currentServer = 0;

                        return currentServer;
                    }
                }
            }
        }

        public static Dictionary<Guid, List<EF.Folder>> folders;

        public static List<EF.Folder> GetFoldersByOwner(Guid oid)
        {
            if (folders == null)
                folders = new Dictionary<Guid, List<EF.Folder>>();

            if (folders.ContainsKey(oid))
                return folders[oid];
            else
            {
                using(var context = new EF.HrCenterEntities())
                {
                    var list = context.Folder.Where(a => a.OwnerId == oid).ToList();
                    folders.Add(oid, list);
                    return list;
                }
            }
        }

        public static void ResetFoldersByOwner(Guid oid)
        {
            if (folders != null && folders.ContainsKey(oid))
                folders.Remove(oid);
        }

        //public static Models.ExtraFunctionModel GetExtraFunctionForUser(string username)
        //{
        //    if(extraUserFunction == null)
        //    {
        //        extraUserFunction = new Dictionary<string, ExtraFunctionModel>();
        //    }
        //    if (extraUserFunction.ContainsKey(username))
        //        return extraUserFunction[username];
        //    else
        //    {

        //    }
        //}
    }
}