﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrcloud.Services.Widgets
{
    public class SonNguyenCacheService
    {
        private static List<EFWIDGET.SN_Nail> nails;

        public static List<EFWIDGET.SN_Nail> Nails
        {
            get
            {
                if (nails == null)
                {
                    using (var context = new EFWIDGET.HrWidgetEntities())
                    {
                        nails = context.SN_Nail.ToList();
                    }
                }
                return nails;
            }
        }

        public static void ResetNails()
        {
            nails = null;
        }

        private static List<EFWIDGET.SN_Grit> grit;

        public static List<EFWIDGET.SN_Grit> Grit
        {
            get
            {
                if (grit == null)
                {
                    using (var context = new EFWIDGET.HrWidgetEntities())
                    {
                        grit = context.SN_Grit.ToList();
                    }
                }
                return grit;
            }
        }

        private static List<EFWIDGET.SN_PUser> users;

        public static List<EFWIDGET.SN_PUser> Users
        {
            get
            {
                if (users == null)
                {
                    using (var context = new EFWIDGET.HrWidgetEntities())
                    {
                        users = context.SN_PUser.Where(a => a.IsActive && a.UserId != "thuvla").ToList();
                    }
                }
                return users;
            }
        }

        public static void ResetHomeWorkingUser()
        {
            users = null;
        }

        private static List<string> managerHUsers;

        public static List<string> ManagerHUsers
        {
            get
            {
                if (managerHUsers == null)
                {
                    managerHUsers = new List<string>();
                    using (var context = new EF.HrCenterEntities())
                    {
                        var wid = PNUtility.Helpers.UtilityHelper.ParseGuid("012B8304-659F-4B07-9688-299EF1AA68DA");
                        var widget = context.Widget.Where(a => a.WidgetId == wid).FirstOrDefault();
                        if (widget != null)
                        {
                            Models.SonNguyen.HWUsers hwusers = new Models.SonNguyen.HWUsers();
                            if (!string.IsNullOrEmpty(widget.ConfigValue))
                            {
                                hwusers = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<Models.SonNguyen.HWUsers>(widget.ConfigValue);
                                managerHUsers = hwusers.ManageUsers;
                            }
                        }
                    }
                }
                return managerHUsers;
            }
        }

        public static void ResetManagerUser()
        {
            managerHUsers = null;
        }

        private static List<EFWIDGET.Order> orders;

        public static void ResetOrder()
        {
            orders = null;
        }
        
        public static List<EFWIDGET.Order> Orders
        {
            get
            {
                if (orders == null)
                {
                    using (var context = new EFWIDGET.HrWidgetEntities())
                    {
                        orders = context.Order.OrderByDescending(a=>a.CreateDate).Take(10).ToList();
                    }
                }
                return orders;
            }
        }

    }
}