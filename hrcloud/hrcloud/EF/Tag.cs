//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hrcloud.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tag
    {
        public System.Guid TagId { get; set; }
        public string TagCode { get; set; }
        public string Value { get; set; }
        public System.Guid OwnerId { get; set; }
        public string RootCode { get; set; }
    }
}
