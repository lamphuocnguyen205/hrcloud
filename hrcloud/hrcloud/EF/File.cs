//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hrcloud.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class File
    {
        public System.Guid FileId { get; set; }
        public System.Guid OwnerId { get; set; }
        public int Type { get; set; }
        public string Link { get; set; }
        public int STT { get; set; }
        public string Name { get; set; }
        public string SearchData { get; set; }
        public bool IsUpload { get; set; }
        public int GoCount { get; set; }
        public string Extension { get; set; }
        public string FullName { get; set; }
    }
}
