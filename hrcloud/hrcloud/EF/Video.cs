//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hrcloud.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Video
    {
        public System.Guid VideoId { get; set; }
        public string YoutubeId { get; set; }
        public string Title { get; set; }
        public System.Guid OwnerId { get; set; }
        public string VDesc { get; set; }
        public string SearchData { get; set; }
        public int STT { get; set; }
        public System.DateTime CreateTime { get; set; }
    }
}
