//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hrcloud.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Link
    {
        public System.Guid LinkId { get; set; }
        public System.Guid OwnerId { get; set; }
        public string URL { get; set; }
        public string Text { get; set; }
        public string Photo { get; set; }
        public int STT { get; set; }
        public bool IsPhoto { get; set; }
        public bool IsBlank { get; set; }
        public string SearchData { get; set; }
    }
}
