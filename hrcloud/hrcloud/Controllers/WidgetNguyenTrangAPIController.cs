﻿using hrcloud.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Hosting;
using System.Web.Http;

namespace hrcloud.Controllers
{
    public class WidgetNguyenTrangAPIController : ApiController
    {
        [Route("api/widget/nguyen-trang/add-customer")]
        [Authorize(Roles = "C_ADM")]
        public string AddCustomer(dynamic data)
        {
            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var cus = new EFWIDGET.NTCustomer();
                    cus.Status = 0;
                    cus.Alias = "";
                    cus.Address = (string)dyn.addr;
                    cus.CMoney = 0;
                    cus.ContactName = (string)dyn.contact;
                    cus.CreateDate = DateTime.Now;
                    cus.CustomerId = Guid.NewGuid();
                    cus.Email = (string)dyn.email;
                    cus.Money = 0;
                    cus.Name = (string)dyn.name;
                    cus.Note = (string)dyn.note;
                    cus.OCount = 0;
                    cus.Phone = (string)dyn.phone;
                    cus.WCount = 0;
                    var avatar = (string)dyn.avatar;
                    if (!string.IsNullOrEmpty(avatar))
                    {
                        var fileName = "";
                        try
                        {
                            fileName = avatar.Substring(11, avatar.Length - 11);
                        }
                        catch (Exception)
                        {
                            
                        }
                        var newPath = "/WidgetData/nt/images/" + fileName;
                        cus.Logo = newPath;
                        try
                        {
                            System.IO.File.Move(Helper_App.ToSystemPath(avatar), Helper_App.ToSystemPath(newPath));
                        }
                        catch (Exception) { }
                    }
                    context.NTCustomer.Add(cus);
                    context.SaveChanges();
                }
            }, "", new object[] { data });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/update-customer")]
        [Authorize(Roles = "C_ADM")]
        public string UpdateCustomer(dynamic data)
        {
            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                var id = (string)dyn.id;
                var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var cus = context.NTCustomer.Where(a => a.CustomerId == gid).FirstOrDefault();
                    cus.Address = (string)dyn.addr;
                    cus.ContactName = (string)dyn.contact;
                    cus.Email = (string)dyn.email;
                    cus.Name = (string)dyn.name;
                    cus.Note = (string)dyn.note;
                    cus.Phone = (string)dyn.phone;
                    var avatar = (string)dyn.avatar;
                    if (!string.IsNullOrEmpty(avatar))
                    {
                        var fileName = "";
                        try
                        {
                            fileName = avatar.Substring(11, avatar.Length - 11);
                        }
                        catch (Exception)
                        {

                        }
                        var newPath = "/WidgetData/nt/images/" + fileName;
                        cus.Logo = newPath;
                        try
                        {
                            System.IO.File.Move(Helper_App.ToSystemPath(avatar), Helper_App.ToSystemPath(newPath));
                        }
                        catch (Exception) { }
                    }
                    context.SaveChanges();
                }
            }, "", new object[] { data });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/add-plastic")]
        [Authorize(Roles = "C_ADM")]
        public string AddPlatic(dynamic data)
        {
            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var cus = new EFWIDGET.NTPlastic();  
                    cus.Name = (string)dyn.name;
                    cus.WCount = 0;
                    cus.Description = (string)dyn.note;
                    var avatar = (string)dyn.avatar;
                    var price = (string)dyn.price;
                    price = price.Replace(",", "");
                    price = price.Replace(".", "");
                    cus.TPrice = PNUtility.Helpers.UtilityHelper.ParseLong(price);
                    if (!string.IsNullOrEmpty(avatar))
                    {
                        var fileName = "";
                        try
                        {
                            fileName = avatar.Substring(11, avatar.Length - 11);
                        }
                        catch (Exception)
                        {

                        }
                        var newPath = "/WidgetData/nt/images/" + fileName;
                        cus.Photo = newPath;
                        try
                        {
                            System.IO.File.Move(Helper_App.ToSystemPath(avatar), Helper_App.ToSystemPath(newPath));
                        }
                        catch (Exception) { }
                    }
                    cus.Description = (string)dyn.desc;
                    context.NTPlastic.Add(cus);
                    context.SaveChanges();
                }
            }, "", new object[] { data });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/update-plastic")]
        [Authorize(Roles = "C_ADM")]
        public string UpdatePlatic(dynamic data)
        {
            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var pid = PNUtility.Helpers.UtilityHelper.ParseInt((string)dyn.id);
                    var cus = context.NTPlastic.Where(a => a.PlasticId == pid).FirstOrDefault();
                    cus.Name = (string)dyn.name;
                    cus.Description = (string)dyn.note;
                    var avatar = (string)dyn.avatar;
                    var price = (string)dyn.price;
                    price = price.Replace(",", "");
                    price = price.Replace(".", "");
                    cus.TPrice = PNUtility.Helpers.UtilityHelper.ParseLong(price);
                    if (!string.IsNullOrEmpty(avatar))
                    {
                        var fileName = "";
                        try
                        {
                            fileName = avatar.Substring(11, avatar.Length - 11);
                        }
                        catch (Exception)
                        {

                        }
                        var newPath = "/WidgetData/nt/images/" + fileName;
                        cus.Photo = newPath;
                        try
                        {
                            System.IO.File.Move(Helper_App.ToSystemPath(avatar), Helper_App.ToSystemPath(newPath));
                        }
                        catch (Exception) { }
                    }
                    cus.Description = (string)dyn.desc;
                    context.SaveChanges();
                }
            }, "", new object[] { data });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/get-cp-data")]
        [HttpGet]
        [Authorize(Roles = "C_ADM")]
        public HttpResponseMessage GetCustomerAndPlastic()
        {
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var customers = context.NTCustomer.Where(a => a.Status >= 0).ToList();
                var plastics = context.NTPlastic.ToList();
                var orders = context.NTOrder.OrderByDescending(a=>a.DateIn).ToList();
                var model = new { customers = customers, plastics = plastics, orders = orders };
                HttpResponseMessage respone = new HttpResponseMessage();
                respone.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(model));
                return respone;
            }
        }
        [Route("api/widget/nguyen-trang/get-order-filter")]
        [HttpGet]
        [Authorize(Roles = "C_ADM")]
        public HttpResponseMessage GetOrderByFilter(string fromday, string today, string type, string keyword)
        {
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var set = from h in context.NTOrder
                          select h;

                if (!string.IsNullOrEmpty(fromday) && !string.IsNullOrEmpty(today))
                {

                    DateTime dtf = DateTime.ParseExact(fromday, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    DateTime dtt = DateTime.ParseExact(today, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    dtt = dtt.AddHours(24);

                    set = from s in set where s.CreateDate >= dtf && s.CreateDate < dtt select s;
                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    var query = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(keyword);
                    set = from s in set where s.SearchData.Contains(query) select s;
                }

                if (!string.IsNullOrEmpty(type))
                {
                    var status = PNUtility.Helpers.UtilityHelper.ParseInt(type);
                    if (status >= 0)
                        set = from s in set where s.Status == status select s;
                }

                var model = set.OrderByDescending(a => a.DateIn).ToList();
                HttpResponseMessage respone = new HttpResponseMessage();
                respone.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(model));
                return respone;
            }
        }


        [Route("api/widget/nguyen-trang/post-import-plastic")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string PostImportWorking(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);
            
            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                var domain = (string)args[1];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    using(DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        string s = (string)dyn.date;
                        DateTime dt = DateTime.ParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        var oid = (string)dyn.oid;
                        if (string.IsNullOrEmpty(oid))
                        {
                            var cid = (string)dyn.customer;
                            var vcid = PNUtility.Helpers.UtilityHelper.ParseGuid(cid);
                            var customer = context.NTCustomer.Where(a => a.CustomerId == vcid).FirstOrDefault();

                            var pid = (int)dyn.plastic;
                            var plastic = context.NTPlastic.Where(a => a.PlasticId == pid).FirstOrDefault();

                            if (customer != null && plastic != null)
                            {
                                var ord = new EFWIDGET.NTOrder();
                                ord.CreateBy = key;
                                ord.DateIn = DateTime.Now;
                                ord.CreateDate = dt;
                                ord.CustomerId = vcid;
                                ord.Plastic = pid;
                                ord.Note = (string)dyn.note;
                                var w = (string)dyn.weight;
                                w = w.Replace(",", "").Replace(".", "");
                                ord.WeightInput = PNUtility.Helpers.UtilityHelper.ParseInt(w);
                                customer.WCount += ord.WeightInput;
                                customer.OCount++;
                                plastic.WCount += ord.WeightInput;
                                var p = (string)dyn.price;
                                p = p.Replace(",", "").Replace(".", "");
                                ord.Price = PNUtility.Helpers.UtilityHelper.ParseLong(p);
                                ord.NTOrderID = Guid.NewGuid();
                                ord.Status = 0;
                                ord.CountIn = 1;
                                var photo = (string)dyn.photo;
                                if (!string.IsNullOrEmpty(photo))
                                {
                                    UpdateMultiPhotoModel updateModel = new UpdateMultiPhotoModel(domain, "", photo);
                                    ord.Photo = updateModel.GetResultText();
                                    updateModel.ProcessImagesNoAvatar();
                                }
                                var import = new EFWIDGET.NTImport();
                                import.NoId = (string)dyn.bill;
                                import.CarId = (string)dyn.car;
                                import.CreateDate = ord.CreateDate;
                                import.DateIn = ord.DateIn;
                                import.ImportId = Guid.NewGuid();
                                import.IsRoot = true;
                                import.Note = ord.Note;
                                import.Photo = ord.Photo;
                                import.Weight = ord.WeightInput;
                                import.OrderId = ord.NTOrderID;
                                ord.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(customer.Name + " " + customer.Phone + " " + plastic.Name + " " + ord.Note + " " + import.CarId + " " + import.NoId);
                                import.SearchData = ord.SearchData;
                                context.NTOrder.Add(ord);
                                context.NTImport.Add(import);
                                var caltype = (string)dyn.caltype;
                                if(caltype == "1")
                                {
                                    import.IUC = true;
                                    var money = import.Weight * ord.Price;
                                    customer.Money += money;
                                    customer.CMoney += money;
                                    EFWIDGET.NTHistory his = new EFWIDGET.NTHistory();
                                    his.HistoryId = Guid.NewGuid();
                                    his.DateIn = import.DateIn;
                                    his.RefW = import.Weight;
                                    his.Money = money;
                                    his.IsInc = true;
                                    import.Price = ord.Price;
                                    his.CreateDate = import.CreateDate;
                                    his.RefP = (int)ord.Price;
                                    his.Type = 1;
                                    his.RefIEID = import.ImportId.ToString();
                                    his.Photo = import.Photo;
                                    his.CustomerId = customer.CustomerId;
                                    context.NTHistory.Add(his);
                                }
                            }
                        }
                        else
                        {
                            var orderid = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                            var order = context.NTOrder.Where(a => a.NTOrderID == orderid).FirstOrDefault();
                            if (order != null && order.Status == 0)
                            {
                                var cusomer = context.NTCustomer.Where(a => a.CustomerId == order.CustomerId).FirstOrDefault();
                                var plastic = context.NTPlastic.Where(a => a.PlasticId == order.Plastic).FirstOrDefault();
                                if (cusomer != null && plastic != null)
                                {
                                    var import = new EFWIDGET.NTImport();
                                    import.NoId = (string)dyn.bill;
                                    import.CarId = (string)dyn.car;
                                    import.CreateDate = dt;
                                    import.DateIn = DateTime.Now;
                                    import.ImportId = Guid.NewGuid();
                                    import.IsRoot = false;
                                    import.Note = (string)dyn.note;
                                    var photo = (string)dyn.photo;
                                    if (!string.IsNullOrEmpty(photo))
                                    {
                                        UpdateMultiPhotoModel updateModel = new UpdateMultiPhotoModel(domain, "", photo);
                                        import.Photo = updateModel.GetResultText();
                                        updateModel.ProcessImagesNoAvatar();
                                    }

                                    var w = (string)dyn.weight;
                                    w = w.Replace(",", "").Replace(".", "");
                                    import.Weight = PNUtility.Helpers.UtilityHelper.ParseInt(w);
                                    order.WeightInput += import.Weight;
                                    order.CountIn++;

                                    cusomer.WCount += import.Weight;
                                    plastic.WCount += import.Weight;

                                    import.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(cusomer.Name + " " + cusomer.Phone + " " + plastic.Name + " " + import.Note + " " + import.CarId + " " + import.NoId);
                                    import.OrderId = orderid;
                                    context.NTImport.Add(import);
                                    var caltype = (string)dyn.caltype;
                                    if (caltype == "1")
                                    {
                                        import.IUC = true;
                                        var money = import.Weight * order.Price;
                                        cusomer.Money += money;
                                        cusomer.CMoney += money;
                                        EFWIDGET.NTHistory his = new EFWIDGET.NTHistory();
                                        his.HistoryId = Guid.NewGuid();
                                        his.DateIn = import.DateIn;
                                        his.RefW = import.Weight;
                                        his.Money = money;
                                        his.IsInc = true;
                                        import.Price = order.Price;
                                        his.CreateDate = import.CreateDate;
                                        his.RefP = (int)order.Price;
                                        his.Type = 1;
                                        his.RefIEID = import.ImportId.ToString();
                                        his.Photo = import.Photo;
                                        his.CustomerId = cusomer.CustomerId;
                                        context.NTHistory.Add(his);
                                    }
                                }
                            }
                        }
                        context.SaveChanges();
                        transaction.Commit();
                    }                    
                }
            }, me, new object[] { data, user.Domain });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/post-export-plastic")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string PostExportWorking(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);

            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                var domain = (string)args[1];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        string s = (string)dyn.date;
                        DateTime dt = DateTime.ParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        var oid = (string)dyn.oid;
                        var orderid = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                        var order = context.NTOrder.Where(a => a.NTOrderID == orderid).FirstOrDefault();
                        if (order != null)
                        {
                            var cusomer = context.NTCustomer.Where(a => a.CustomerId == order.CustomerId).FirstOrDefault();
                            var plastic = context.NTPlastic.Where(a => a.PlasticId == order.Plastic).FirstOrDefault();
                            if (cusomer != null && plastic != null && order.Status == 0)
                            {
                                var export = new EFWIDGET.NTTransport();

                                var approve = (string)dyn.approve;

                                export.NTOrderId = order.NTOrderID;
                                export.NoId = (string)dyn.bill;
                                export.CarId = (string)dyn.car;
                                export.STime = dt;
                                export.DateIn = DateTime.Now;
                                export.TransportId = Guid.NewGuid();
                                export.Note = (string)dyn.note;
                                var photo = (string)dyn.photo;
                                if (!string.IsNullOrEmpty(photo))
                                {
                                    UpdateMultiPhotoModel updateModel = new UpdateMultiPhotoModel(domain, "", photo);
                                    export.Photo = updateModel.GetResultText();
                                    updateModel.ProcessImagesNoAvatar();
                                }

                                var w = (string)dyn.weight;
                                w = w.Replace(",", "").Replace(".", "");
                                export.SW = PNUtility.Helpers.UtilityHelper.ParseInt(w);

                                var p = (string)dyn.price;
                                p = p.Replace(",", "").Replace(".", "");
                                export.Price = PNUtility.Helpers.UtilityHelper.ParseLong(p);

                                export.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(cusomer.Name + " " + cusomer.Phone + " " + plastic.Name + " " + export.Note + " " + export.CarId + " " + export.NoId);
                                order.CountOut++;

                                export.Status = 1;
                                export.ATime = export.STime;
                                export.AW = export.SW;
                                export.Money = export.AW * export.Price;
                                order.WeightOutput += export.AW;
                                order.Price = export.Price;
                                cusomer.WCountS += export.AW;

                                var caltype = (string)dyn.caltype;
                                if (caltype == "1")
                                {
                                    export.IUC = true;
                                    cusomer.Money += export.Money;
                                    cusomer.CMoney += export.Money;
                                    EFWIDGET.NTHistory his = new EFWIDGET.NTHistory();
                                    his.HistoryId = Guid.NewGuid();
                                    his.DateIn = export.DateIn;
                                    his.RefW = export.AW;
                                    his.Money = export.Money;
                                    his.IsInc = true;
                                    his.CreateDate = export.STime;
                                    his.RefP = (int)order.Price;
                                    his.Type = 2;
                                    his.RefIEID = export.TransportId.ToString();
                                    his.Photo = export.Photo;
                                    his.CustomerId = cusomer.CustomerId;
                                    context.NTHistory.Add(his);
                                }

                                context.NTTransport.Add(export);
                            }
                        }
                        context.SaveChanges();
                        transaction.Commit();
                    }
                    
                }
            }, me, new object[] { data, user.Domain });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/cancel-export")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string CancelExport(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);
            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        var id = (string)dyn.id;
                        var vid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                        var trans = context.NTTransport.Where(a => a.TransportId == vid).FirstOrDefault();
                        if(trans != null)
                        {
                            var order = context.NTOrder.Where(a => a.NTOrderID == trans.NTOrderId).FirstOrDefault();
                            var customer = context.NTCustomer.Where(a => a.CustomerId == order.CustomerId).FirstOrDefault();
                            if(order != null && customer != null)
                            {
                                order.CountOut--;
                                order.WeightOutput = order.WeightOutput - trans.AW;
                                customer.WCountS -= trans.AW;
                                if (trans.IUC)
                                {
                                    var refid = trans.TransportId.ToString().ToLower();
                                    var his = context.NTHistory.Where(a => a.RefIEID == refid).FirstOrDefault();
                                    if(his != null)
                                    {
                                        context.NTHistory.Remove(his);
                                    }
                                    customer.CMoney -= trans.Money;
                                    customer.Money -= trans.Money;
                                }
                            }
                            context.NTTransport.Remove(trans);
                        }
                        context.SaveChanges();
                        transaction.Commit();
                    }                        
                }
            }, me, new object[] { data, user.Domain });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/cancel-import")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string CancelImport(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);
            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        var id = (string)dyn.id;
                        var vid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                        var import = context.NTImport.Where(a => a.ImportId == vid).FirstOrDefault();
                        if (import != null)
                        {
                            var order = context.NTOrder.Where(a => a.NTOrderID == import.OrderId).FirstOrDefault();
                            var customer = context.NTCustomer.Where(a => a.CustomerId == order.CustomerId).FirstOrDefault();
                            var plastic = context.NTPlastic.Where(a => a.PlasticId == order.Plastic).FirstOrDefault();
                            if (order != null && customer != null && plastic != null)
                            {
                                plastic.WCount -= import.Weight;
                                customer.WCount -= import.Weight;
                                order.WeightInput -= import.Weight;
                                order.CountIn--;

                                if (import.IUC)
                                {
                                    var refid = import.ImportId.ToString().ToLower();
                                    var his = context.NTHistory.Where(a => a.RefIEID == refid).FirstOrDefault();
                                    if (his != null)
                                    {
                                        context.NTHistory.Remove(his);
                                    }
                                    var money = import.Weight * order.Price;
                                    customer.CMoney -= money;
                                    customer.Money -= money;
                                }
                            }
                            context.NTImport.Remove(import);
                        }
                        context.SaveChanges();
                        transaction.Commit();
                    }
                }
            }, me, new object[] { data, user.Domain });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/post-goback-plastic")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string PostGoBack(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);

            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                var domain = (string)args[1];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    string s = (string)dyn.date;
                    DateTime dt = DateTime.ParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var oid = (string)dyn.oid;
                    var orderid = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                    var order = context.NTOrder.Where(a => a.NTOrderID == orderid).FirstOrDefault();
                    if (order != null)
                    {
                        var cusomer = context.NTCustomer.Where(a => a.CustomerId == order.CustomerId).FirstOrDefault();
                        var plastic = context.NTPlastic.Where(a => a.PlasticId == order.Plastic).FirstOrDefault();
                        if (cusomer != null && plastic != null && order.Status == 0)
                        {
                            var back = new EFWIDGET.NTBack();

                            var approve = (string)dyn.approve;

                            back.NTOrderID = order.NTOrderID;
                            back.NoId = (string)dyn.bill;
                            back.CarId = (string)dyn.car;
                            back.CreateDate = dt;
                            back.DateIn = DateTime.Now;
                            back.BackId = Guid.NewGuid();
                            back.Note = (string)dyn.note;
                            var photo = (string)dyn.photo;
                            if (!string.IsNullOrEmpty(photo))
                            {
                                UpdateMultiPhotoModel updateModel = new UpdateMultiPhotoModel(domain, "", photo);
                                back.Photo = updateModel.GetResultText();
                                updateModel.ProcessImagesNoAvatar();
                            }

                            var w = (string)dyn.weight;
                            w = w.Replace(",", "").Replace(".", "");
                            back.BW = PNUtility.Helpers.UtilityHelper.ParseInt(w);

                            order.CBack += back.BW;
                            //order.Status
                            cusomer.CBack += back.BW;

                            var caltype = (string)dyn.caltype;
                            if (caltype == "1")
                            {
                                var money = order.Price * back.BW;
                                cusomer.Money -= money;
                                cusomer.CMoney -= money;
                                back.IUC = true;
                                EFWIDGET.NTHistory his = new EFWIDGET.NTHistory();
                                his.HistoryId = Guid.NewGuid();
                                his.DateIn = back.DateIn;
                                his.RefW = back.BW;
                                his.Money = money;
                                his.IsInc = false;
                                his.CreateDate = back.CreateDate;
                                his.RefP = (int)order.Price;
                                his.Type = 3;
                                his.RefIEID = back.BackId.ToString();
                                his.Photo = photo;
                                his.CustomerId = cusomer.CustomerId;
                                context.NTHistory.Add(his);
                            }

                            context.NTBack.Add(back);
                        }
                    }
                    context.SaveChanges();
                }
            }, me, new object[] { data, user.Domain });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/post-do-payment")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string PostDoPayment(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);

            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                var domain = (string)args[1];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    string s = (string)dyn.date;
                    DateTime dt = DateTime.ParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var cid = (string)dyn.cid;
                    var customerid = PNUtility.Helpers.UtilityHelper.ParseGuid(cid);
                    var customer = context.NTCustomer.Where(a => a.CustomerId == customerid).FirstOrDefault();
                    if (customer != null)
                    {
                        var pm = new EFWIDGET.NTPayment();
                        pm.CreateBy = key;
                        pm.CustomerId = customerid;
                        pm.PaymentId = Guid.NewGuid();
                        pm.DateIn = DateTime.Now;
                        pm.CreateDate = dt;
                        pm.Note = (string)dyn.note;
                        var val = (string)dyn.value;
                        val = val.Replace(",", "").Replace(".", "");
                        pm.Val = PNUtility.Helpers.UtilityHelper.ParseLong(val);
                        var photo = (string)dyn.photo;
                        if (!string.IsNullOrEmpty(photo))
                        {
                            UpdateMultiPhotoModel updateModel = new UpdateMultiPhotoModel(domain, "", photo);
                            pm.Photo = updateModel.GetResultText();
                            updateModel.ProcessImagesNoAvatar();
                        }
                        context.NTPayment.Add(pm);
                        customer.CMoney -= pm.Val;
                    }
                    context.SaveChanges();
                }
            }, me, new object[] { data, user.Domain });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/post-approve-export-plastic")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string PostApproveExportWorking(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);

            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                var domain = (string)args[1];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    string s = (string)dyn.date;
                    DateTime dt = DateTime.ParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var sid = (string)dyn.sid;
                    var exportid = PNUtility.Helpers.UtilityHelper.ParseGuid(sid);
                    var export = context.NTTransport.Where(a => a.TransportId == exportid).FirstOrDefault();
                    if(export != null)
                    {
                        var order = context.NTOrder.Where(a => a.NTOrderID == export.NTOrderId).FirstOrDefault();
                        var customer = context.NTCustomer.Where(a=>a.CustomerId == order.CustomerId).FirstOrDefault();
                        if(order != null && customer != null)
                        {
                            export.Status = 1;
                            export.ATime = dt;
                            var w = (string)dyn.weight;
                            w = w.Replace(",", "").Replace(".", "");
                            export.AW = PNUtility.Helpers.UtilityHelper.ParseInt(w);

                            var p = (string)dyn.price;
                            p = p.Replace(",", "").Replace(".", "");
                            export.Price = PNUtility.Helpers.UtilityHelper.ParseLong(p);

                            export.Money = export.AW + export.Price;
                            order.WeightOutput += export.AW;
                            customer.WCountS += export.AW;
                            customer.Money += export.Money;
                            customer.CMoney += export.Money;

                            export.Note = (string)dyn.note;
                        }
                    }
                    
                    context.SaveChanges();
                }
            }, me, new object[] { data, user.Domain });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/customer-info-login")]
        [HttpPost]
        public HttpResponseMessage LoginForCustomer(dynamic data)
        {

            var cid = (string)data.cid;
            var seckey = (string)data.sec;
            var customerid = PNUtility.Helpers.UtilityHelper.ParseGuid(cid);
            HttpResponseMessage response = new HttpResponseMessage();
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var cus = context.NTCustomer.Where(a => a.CustomerId == customerid && a.Note == seckey).FirstOrDefault();
                if (cus != null)
                {
                    var cookie = new CookieHeaderValue(cid, seckey);
                    cookie.Expires = DateTimeOffset.Now.AddDays(7);
                    cookie.Domain = Request.RequestUri.Host;
                    cookie.Path = "/";
                    response.Headers.AddCookies(new CookieHeaderValue[] { cookie });

                    response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(cus));
                    return response;
                }
                else
                {
                    response.Content = new StringContent("");
                    return response;
                }
            }
        }

        [Route("api/widget/nguyen-trang/post-directly-account")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string PostDirectlyAccount(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);

            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                var domain = (string)args[1];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        string s = (string)dyn.date;
                        DateTime dt = DateTime.ParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        var id = (string)dyn.customer;
                        var cid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                        var cusomer = context.NTCustomer.Where(a => a.CustomerId == cid).FirstOrDefault();

                        if (cusomer != null)
                        {
                            var type = (int)dyn.type;

                            var m = (string)dyn.value;
                            m = m.Replace(",", "").Replace(".", "");
                            var money = PNUtility.Helpers.UtilityHelper.ParseLong(m);

                            if (type == 1)
                            {
                                cusomer.Money += money;
                                cusomer.CMoney += money;
                            }
                            else
                            {
                                cusomer.Money -= money;
                                cusomer.CMoney -= money;
                            }

                           

                            EFWIDGET.NTHistory his = new EFWIDGET.NTHistory();
                            his.HistoryId = Guid.NewGuid();
                            his.DateIn = DateTime.Now;
                            his.RefW = 0;
                            his.Money = money;
                            his.IsInc = type == 1;
                            his.CreateDate = dt;
                            his.RefP = 0;
                            his.Type = 4;
                            his.RefIEID = "";
                            his.CustomerId = cid;
                            his.Detail = (string)dyn.note;
                            var photo = (string)dyn.photo;
                            if (!string.IsNullOrEmpty(photo))
                            {
                                UpdateMultiPhotoModel updateModel = new UpdateMultiPhotoModel(domain, "", photo);
                                his.Photo = updateModel.GetResultText();
                                updateModel.ProcessImagesNoAvatar();
                            }

                            context.NTHistory.Add(his);
                        }
                        context.SaveChanges();
                        transaction.Commit();
                    }

                }
            }, me, new object[] { data, user.Domain });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/post-delete-directly-account")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string PostDeleteDirectlyAccount(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);

            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        var id = (string)dyn.id;
                        var historyId = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                        var history = context.NTHistory.Where(a => a.HistoryId == historyId && a.Type == 4).FirstOrDefault();
                        
                        if(history != null)
                        {
                            var customer = context.NTCustomer.Where(a => a.CustomerId == history.CustomerId).FirstOrDefault();
                            if (customer != null)
                            {
                                if (history.IsInc)
                                {
                                    customer.Money -= history.Money;
                                    customer.CMoney -= history.Money;
                                }
                                else
                                {
                                    customer.Money += history.Money;
                                    customer.CMoney += history.Money;
                                }
                                context.NTHistory.Remove(history);
                                context.SaveChanges();
                            }
                        }                        
                        context.SaveChanges();
                        transaction.Commit();
                    }

                }
            }, me, new object[] { data, user.Domain });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/post-update-price")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string UpdatePrice(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);

            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var id = (string)dyn.id;
                    var vid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    var order = context.NTOrder.Where(a => a.NTOrderID == vid).FirstOrDefault();
                    if(order != null)
                    {
                        var m = (string)dyn.price;
                        m = m.Replace(",", "").Replace(".", "");
                        var money = PNUtility.Helpers.UtilityHelper.ParseLong(m);
                        order.Price = money;
                        context.SaveChanges();
                    }                    
                }
            }, me, new object[] { data });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/get-order-info-for-actions/{id}")]
        [HttpGet]
        [Authorize(Roles = "C_ADM")]
        public HttpResponseMessage GetOrderInfoAction(string id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var vid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var order = context.NTOrder.Where(a => a.NTOrderID == vid).FirstOrDefault();
                if (order != null)
                {
                    response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(order));
                }
                else
                    response.Content = new StringContent("");

                return response;
            }
        }

        [Route("api/widget/nguyen-trang/post-complete-order")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string CompleteOrder(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);

            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var id = (string)dyn.id;
                    var vid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    var order = context.NTOrder.Where(a => a.NTOrderID == vid).FirstOrDefault();
                    if (order != null)
                    {
                        order.Status = 1;
                        order.LoseReason = (string)dyn.res;
                        order.LoseCount = order.WeightInput - order.CBack - order.WeightOutput;
                        context.SaveChanges();
                    }
                }
            }, me, new object[] { data });
            return "ok";
        }

        [Route("api/widget/nguyen-trang/post-cancel-order")]
        [HttpPost]
        [Authorize(Roles = "C_ADM")]
        public string CancelOrder(dynamic data)
        {
            var me = User.Identity.Name;
            var user = CacheCenter.GetUserCached(me);

            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var id = (string)dyn.id;
                    var vid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    var order = context.NTOrder.Where(a => a.NTOrderID == vid).FirstOrDefault();
                    if (order != null && order.WeightInput == 0 && order.WeightOutput == 0)
                    {
                        context.NTOrder.Remove(order);
                        context.SaveChanges();
                    }
                }
            }, me, new object[] { data });
            return "ok";
        }

        public static DbGeography CreatePoint(double lat, double lon, int srid = 4326)
        {
            string wkt = String.Format("POINT({0} {1})", lon, lat);

            return DbGeography.PointFromText(wkt, srid);
        }
    }
}
