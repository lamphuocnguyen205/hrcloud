﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace hrcloud.Controllers
{
    [Authorize(Roles = "M_ADM")]
    public class CenterManagementController : Controller
    {
        // GET: CenterManagement
        [Route("center/mamagement/overview")]
        public ActionResult Overview()
        {

            return View();
        }

        [Route("center/management/customer-list")]
        public ActionResult CustomerList(string page, string q)
        {
            using(var context = new EF.HrCenterEntities())
            {
                var list = context.Owner.Include(a=>a.Widget).OrderByDescending(a => a.CreateDate).ToList();
                return View(list);
            }            
        }

        [Route("center/mamagement/create-owner")]
        public ActionResult CreateOwner()
        {
            return View();
        }

        [Route("center/management/detail-owner/{id}")]
        public ActionResult DetailOwner(string id)
        {
            using(var context = new EF.HrCenterEntities())
            {
                var oid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var owner = context.Owner.Include(a => a.QUser).Where(b => b.OwnerId == oid).FirstOrDefault();
                return View(owner);
            }
        }
    }
}