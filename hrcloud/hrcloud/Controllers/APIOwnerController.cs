﻿using hrcloud.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace hrcloud.Controllers
{
    [Authorize(Roles = "C_ADM")]
    public class APIOwnerController : ApiController
    {
        [Route("api/owner/cms/create-menu")]
        [HttpPost]
        public string CreateMenu(dynamic data)
        {
            var id = Guid.NewGuid();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var fid = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    var menu = new EF.Folder();
                    menu.FolderId = fid;
                    menu.DirectURL = (string)dyn.url;
                    menu.HasChild = false;
                    menu.IsProduct = ((string)dyn.pro) == "true";
                    menu.IsRoot = ((string)dyn.root) == "true";
                    menu.Name = (string)dyn.name;
                    menu.OwnerId = oid;
                    var parentId = (string)dyn.parent;
                    if (!string.IsNullOrEmpty(parentId))
                    {
                        menu.ParentId = PNUtility.Helpers.UtilityHelper.ParseGuid(parentId);
                    }
                    menu.STT = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                    menu.TLink = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3URL(menu.Name);
                    context.Folder.Add(menu);
                    context.SaveChanges();
                }
            }, un, new object[] { data, user.OwnerId, id });
            return id.ToString();
        }

        [Route("api/owner/cms/update-menu")]
        [HttpPost]
        public string UpdateMainMenu(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var menuIdText = (string)dyn.id;
                var menuId = PNUtility.Helpers.UtilityHelper.ParseGuid(menuIdText);
                using (var context = new EF.HrCenterEntities())
                {
                    var menu = context.Folder.Where(m => m.FolderId == menuId && m.OwnerId == oid).FirstOrDefault();
                    if (menu != null)
                    {
                        menu.DirectURL = (string)dyn.url;
                        menu.IsProduct = ((string)dyn.pro) == "true";
                        menu.Name = (string)dyn.name;
                        menu.TLink = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3URL(menu.Name);
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [Route("api/owner/cms/update-sub-menu")]
        [HttpPost]
        public string UpdateSubMenu(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var menuIdText = (string)dyn.id;
                var menuId = PNUtility.Helpers.UtilityHelper.ParseGuid(menuIdText);
                using (var context = new EF.HrCenterEntities())
                {
                    var menu = context.Folder.Where(m => m.FolderId == menuId && m.OwnerId == oid).FirstOrDefault();
                    if (menu != null)
                    {
                        menu.DirectURL = (string)dyn.url;
                        menu.Name = (string)dyn.name;
                        menu.TLink = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3URL(menu.Name);
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [Route("api/owner/cms/create-sub-menu")]
        [HttpPost]
        public string CreateSubMenu(dynamic data)
        {
            var id = Guid.NewGuid();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var fid = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    var pidText = (string)dyn.parent;
                    var pid = PNUtility.Helpers.UtilityHelper.ParseGuid(pidText);
                    var parent = context.Folder.Where(m => m.FolderId == pid).FirstOrDefault();
                    if (parent != null)
                    {
                        var menu = new EF.Folder();
                        menu.FolderId = fid;
                        menu.DirectURL = (string)dyn.url;
                        menu.HasChild = false;
                        menu.IsProduct = parent.IsProduct;
                        menu.IsRoot = false;
                        menu.Name = (string)dyn.name;
                        menu.OwnerId = parent.OwnerId;
                        menu.ParentId = pid;
                        menu.STT = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                        menu.TLink = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3URL(menu.Name);
                        parent.HasChild = true;
                        context.Folder.Add(menu);
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId, id });
            return id.ToString();
        }

        [Route("api/owner/get-all-menu")]
        [HttpGet]
        public HttpResponseMessage GetAllMenu()
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var list = context.Folder.Where(a => a.OwnerId == user.OwnerId).OrderBy(a => a.STT).ToList();
                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/delete-menu")]
        [HttpPost]
        public string DeleteMenu(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                using (var context = new EF.HrCenterEntities())
                {
                    var fidText = (string)dyn.id;
                    var fid = PNUtility.Helpers.UtilityHelper.ParseGuid(fidText);
                    var findMenu = context.Folder.Where(m => m.FolderId == fid && m.OwnerId == oid).FirstOrDefault();
                    if (findMenu != null)
                    {
                        if (!findMenu.HasChild)
                        {
                            context.Folder.Remove(findMenu);
                            if (findMenu.ParentId.HasValue)
                            {
                                var count = context.Folder.Where(m => m.ParentId == findMenu.ParentId.Value && m.FolderId != findMenu.FolderId).Count();
                                if (count == 0)
                                {
                                    var parent = context.Folder.Where(m => m.FolderId == findMenu.ParentId.Value).FirstOrDefault();
                                    if (parent != null)
                                        parent.HasChild = false;
                                }
                            }
                            context.SaveChanges();
                        }
                    }
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [Route("api/owner/get-all-banners")]
        [HttpGet]
        public HttpResponseMessage GetAllBanners()
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var list = context.Banner.Where(a => a.OwnerId == user.OwnerId && a.Visible).OrderBy(a => a.STT).ToList();
                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/create-banner")]
        [HttpPost]
        public string CreateBanner(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Banner banner = new EF.Banner();
                    banner.BannerId = newId;
                    banner.Description = (string)dyn.desc;
                    banner.OwnerId = oid;
                    banner.STT = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                    banner.Visible = true;
                    var photo = (string)dyn.photo;
                    if (!string.IsNullOrEmpty(photo))
                    {
                        //var path = Helper_App.ToSystemPath("/UData/" + oid.ToString().ToLower());
                        //if (!System.IO.Directory.Exists(path))
                        //    System.IO.Directory.CreateDirectory(path);

                        var bannerPath = Helper_App.ToSystemPath("/UData/" + oid.ToString().ToLower() + "/Banners");
                        if (!System.IO.Directory.Exists(bannerPath))
                            System.IO.Directory.CreateDirectory(bannerPath);

                        var fileName = Helper_App.GetFileNameFromTemphotoFolder(photo);
                        var newPath = "/UData/" + oid.ToString().ToLower() + "/Banners/" + fileName;
                        banner.PhotoURL = newPath;
                        Helper_App.MoveFile(photo, newPath);
                        context.Banner.Add(banner);
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId, id });
            var path = (string)data.photo;
            var parseFileName = Helper_App.GetFileNameFromTemphotoFolder(path);
            return id.ToString() + "*" + "/UData/" + user.OwnerId.ToString().ToLower() + "/Banners/" + parseFileName;
        }

        [Route("api/owner/cms/update-banner")]
        [HttpPost]
        public string UpdateBanner(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var bid = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.id);
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Banner banner = context.Banner.Where(b => b.BannerId == bid && b.OwnerId == oid).FirstOrDefault();
                    if (banner != null)
                    {
                        banner.Description = (string)dyn.desc;
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId, });
            return "ok";
        }

        [Route("api/owner/cms/delete-banner")]
        [HttpPost]
        public string DeleteBanner(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                using (var context = new EF.HrCenterEntities())
                {
                    var fidText = (string)dyn.id;
                    var fid = PNUtility.Helpers.UtilityHelper.ParseGuid(fidText);
                    var findBanner = context.Banner.Where(m => m.BannerId == fid && m.OwnerId == oid).FirstOrDefault();
                    if (findBanner != null)
                    {
                        context.Banner.Remove(findBanner);
                        context.SaveChanges();
                        try
                        {
                            Helper_App.DeleteFile(findBanner.PhotoURL);
                        }
                        catch { }
                    }
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [Route("api/owner/cms/get-product-folders")]
        [HttpGet]
        public HttpResponseMessage GetProductFolders()
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var list = context.Folder.Where(a => a.OwnerId == user.OwnerId && a.IsProduct).OrderBy(a => a.STT).ToList();
                Models.JProductPageModel model = new Models.JProductPageModel();
                model.Folders = list;
                model.Tags = context.Tag.Where(a => a.OwnerId == user.OwnerId).OrderBy(a => a.Value).ToList();

                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(model);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/get-article-folders")]
        [HttpGet]
        public HttpResponseMessage GetArticleFolders()
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var list = context.Folder.Where(a => a.OwnerId == user.OwnerId && !a.IsProduct).OrderBy(a => a.STT).ToList();
                Models.JProductPageModel model = new Models.JProductPageModel();
                model.Folders = list;
                model.Tags = context.Tag.Where(a => a.OwnerId == user.OwnerId).OrderBy(a => a.Value).ToList();

                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(model);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/new-tags")]
        [HttpPost]
        public string CreateNewTags(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var text = (string)dyn.tags;
                using (var context = new EF.HrCenterEntities())
                {
                    var split = text.Split('*');
                    var myTags = context.Tag.Where(t => t.OwnerId == oid).ToList();
                    foreach (var item in split)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            var time = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now).ToString();
                            var utext = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3URL(item).ToLower();
                            var find = myTags.Where(t => t.RootCode == utext).FirstOrDefault();
                            if (find == null)
                            {
                                EF.Tag tag = new EF.Tag();
                                tag.Value = item.ToLower();
                                tag.OwnerId = oid;
                                tag.TagId = Guid.NewGuid();
                                tag.TagCode = utext + "-" + time.Substring(time.Length - 5, 5);
                                tag.RootCode = utext;
                                context.Tag.Add(tag);
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [Route("api/owner/cms/get-tags/{page}/{query?}")]
        [HttpGet]
        public HttpResponseMessage GetTagsByPage(string page, string query = null)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;

                var set = from v in context.Tag
                          select v;

                if (!string.IsNullOrEmpty(query))
                {
                    var text = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(query).ToLower();
                    set = from s in set where s.RootCode.Contains(text) select s;
                }
                var list = set.Where(a => a.OwnerId == user.OwnerId).OrderBy(a => a.Value).Skip(skip).Take(10).ToList();

                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/delete-tag")]
        [HttpPost]
        public string DeleteTag(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = (string)data.id;
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)a[2]);
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Tag tag = context.Tag.Where(s => s.TagId == newId && s.OwnerId == oid).FirstOrDefault();
                    if (tag != null)
                    {
                        context.Tag.Remove(tag);
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId, id });
            return "ok";
        }

        [Route("api/owner/cms/new-video")]
        [HttpPost]
        public string CreateNewVideo(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Video video = new EF.Video();
                    video.VideoId = newId;
                    video.VDesc = (string)dyn.desc;
                    video.CreateTime = DateTime.Now;
                    video.OwnerId = oid;
                    video.Title = (string)dyn.title;
                    video.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(video.VDesc + " " + video.Title);
                    video.YoutubeId = (string)dyn.yid;
                    context.Video.Add(video);
                    context.SaveChanges();
                }
            }, un, new object[] { data, user.OwnerId, id });
            return id.ToString();
        }

        [Route("api/owner/cms/update-video")]
        [HttpPost]
        public string UpdateVideo(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var tid = (string)dyn.id;
                var videoId = PNUtility.Helpers.UtilityHelper.ParseGuid(tid);
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Video video = context.Video.Where(v => v.VideoId == videoId && v.OwnerId == oid).FirstOrDefault();
                    if (video != null)
                    {
                        video.Title = (string)dyn.title;
                        video.VDesc = (string)dyn.desc;
                        video.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(video.VDesc + " " + video.Title);
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [Route("api/owner/cms/get-video/{page}/{query?}")]
        [HttpGet]
        public HttpResponseMessage GetVideoByPage(string page, string query = null)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;

                var set = from v in context.Video
                          select v;

                if (!string.IsNullOrEmpty(query))
                {
                    var text = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(query).ToLower();
                    set = from s in set where s.SearchData.Contains(text) select s;
                }
                var list = set.Where(a => a.OwnerId == user.OwnerId).OrderByDescending(a => a.STT).Skip(skip).Take(10).ToList();

                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/delete-video")]
        [HttpPost]
        public string DeleteVideo(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var tid = (string)dyn.id;
                var videoId = PNUtility.Helpers.UtilityHelper.ParseGuid(tid);
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Video video = context.Video.Where(v => v.VideoId == videoId && v.OwnerId == oid).FirstOrDefault();
                    if (video != null)
                    {
                        context.Video.Remove(video);
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [Route("api/owner/cms/new-link")]
        [HttpPost]
        public string CreateNewLink(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Link link = new EF.Link();
                    link.LinkId = newId;
                    link.IsBlank = ((string)dyn.blank) == "true";
                    var photo = (string)dyn.photo;
                    if (string.IsNullOrEmpty(photo))
                    {
                        link.IsPhoto = false;
                    }
                    else
                    {
                        link.IsPhoto = true;
                        Guid fid = link.LinkId;
                        var fileBytes = Convert.FromBase64String(photo);
                        string fileName = "/UData/" + oid.ToString().ToLower() + "/Banners/" + fid.ToString() + ".jpg";
                        var sysFileName = Helper_App.ToSystemPath(fileName);

                        var bannerPath = Helper_App.ToSystemPath("/UData/" + oid.ToString().ToLower() + "/Banners");
                        if (!System.IO.Directory.Exists(bannerPath))
                            System.IO.Directory.CreateDirectory(bannerPath);

                        System.IO.File.WriteAllBytes(sysFileName, fileBytes);
                    }
                    link.Text = (string)dyn.text;
                    link.URL = (string)dyn.url;
                    link.OwnerId = oid;
                    link.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(link.URL + " " + link.Text);
                    context.Link.Add(link);
                    context.SaveChanges();
                }
            }, un, new object[] { data, user.OwnerId, id });
            return id.ToString();
        }
        

        [Route("api/owner/cms/get-text-link/{page}/{query?}")]
        [HttpGet]
        public HttpResponseMessage GetTextLink(string page, string query = null)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;

                var set = from v in context.Link
                          select v;

                if (!string.IsNullOrEmpty(query))
                {
                    var text = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(query).ToLower();
                    set = from s in set where s.SearchData.Contains(text) select s;
                }
                var list = set.Where(a => a.OwnerId == user.OwnerId && !a.IsPhoto).OrderByDescending(a => a.STT).Skip(skip).Take(10).ToList();

                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/get-all-link/{page}/{query?}")]
        [HttpGet]
        public HttpResponseMessage GetAllLink(string page, string query = null)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;

                var set = from v in context.Link
                          select v;

                if (!string.IsNullOrEmpty(query))
                {
                    var text = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(query).ToLower();
                    set = from s in set where s.SearchData.Contains(text) select s;
                }
                var list = set.Where(a => a.OwnerId == user.OwnerId).OrderByDescending(a => a.STT).Skip(skip).Take(10).ToList();

                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/new-file-by-directly")]
        [HttpPost]
        public string CreateNewFile(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    EF.File file = new EF.File();
                    file.FileId = newId;
                    file.Link = (string)dyn.url;
                    file.Type = (int)dyn.type;
                    file.Name = (string)dyn.name;
                    file.OwnerId = oid;
                    file.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(file.Name);
                    file.IsUpload = false;
                    context.File.Add(file);
                    context.SaveChanges();
                }
            }, un, new object[] { data, user.OwnerId, id });
            return id.ToString();
        }

        [Route("api/owner/cms/get-file/{page}/{query?}")]
        [HttpGet]
        public HttpResponseMessage GetFiles(string page, string query = null)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;

                var set = from v in context.File
                          select v;

                if (!string.IsNullOrEmpty(query))
                {
                    var text = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(query).ToLower();
                    set = from s in set where s.SearchData.Contains(text) select s;
                }
                var list = set.Where(a => a.OwnerId == user.OwnerId).OrderByDescending(a => a.STT).Skip(skip).Take(10).ToList();

                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/post-new-thread")]
        [HttpPost]
        public string CreateNewThread(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var oidText = oid.ToString();
                var newId = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    EF.OPost post = new EF.OPost();
                    JPostModel jpostDetail = new JPostModel();
                    post.CreateBy = k;
                    post.CreateDate = DateTime.Now;
                    post.LastUpdate = post.CreateDate;
                    var time = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(post.CreateDate);
                    post.XOrder = time;
                    post.IsProduct = true;
                    post.OwnerId = oid;
                    post.OPostID = newId;

                    var folderId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.folder);
                    var foldername = "";
                    if (folderId != Guid.Empty)
                    {
                        var folders = CacheCenter.GetFoldersByOwner(oid);
                        var find = folders.Where(f => f.FolderId == folderId).FirstOrDefault();
                        if (find != null)
                        {
                            foldername = find.Name;
                            post.FolderId = folderId;
                        }
                    }
                    post.Title = (string)dyn.title;

                    var photo = (string)data.photo;
                    if (!string.IsNullOrEmpty(photo))
                    {
                        UpdateMultiPhotoModel pictureTool = new UpdateMultiPhotoModel(oidText, "", photo);

                        var pictures = pictureTool.GetResultText();

                        pictureTool.ProcessImagesWithAvatar(oidText);

                        post.Avatar = pictureTool.FirstPhoto;

                        jpostDetail.Photo = pictures;
                    }

                    post.ShortDesc = (string)dyn.desc;
                    post.Keywords = (string)dyn.keywords;
                    jpostDetail.Detail = (string)dyn.content;

                    post.Price = (decimal)DataHelper.DoubleParse((string)dyn.price, 0);
                    post.IsVND = (string)dyn.priceDVT == "vnd";
                    var store = DataHelper.FloatN100Parse((string)dyn.store);
                    post.Count = store.HasValue ? store.Value : 0;
                    var sale = DataHelper.FloatN100Parse((string)dyn.sale);
                    post.SaleNum = sale.HasValue ? sale.Value : 0;


                    post.DVT = (string)dyn.dvt;
                    post.PCode = (string)dyn.code;

                    jpostDetail.Note = (string)dyn.note;

                    jpostDetail.Videos = (string)dyn.videos;
                    jpostDetail.Links = (string)dyn.links;
                    jpostDetail.Files = (string)dyn.files;

                    var tags = (string)dyn.tags;
                    post.Tags = string.IsNullOrEmpty(tags) ? "" : tags;

                    post.TLink = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3URL(post.Title) + "-" + time;

                    var plainText = foldername + " " + Helper_App.StripHTML(jpostDetail.Detail);
                    var search = plainText + " " + post.PCode + post.Title + post.ShortDesc;
                    var searchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(search);
                    if (searchData.Length >= 3000)
                        searchData = searchData.Substring(0, 2999);

                    post.SaleType = DataHelper.ByteParse((string)dyn.saleType, 0);
                    if (post.SaleType == 1)
                    {
                        post.SaleValue = (decimal)DataHelper.DoubleParse((string)dyn.saleVal, 0);
                    }
                    if (post.SaleType == 2)
                    {
                        post.SaleNum = DataHelper.IntParse((string)dyn.salePer, 0);
                    }

                    post.ViewType = DataHelper.ByteParse((string)dyn.viewType, 0);

                    post.SearchData = searchData;
                    context.OPost.Add(post);
                    context.SaveChanges();

                    var folderPath = "/UData/" + oidText + "/files";
                    var filename = newId.ToString() + ".txt";
                    var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(jpostDetail);
                    Helper_App.WriteTextFile(folderPath, filename, jdata);
                }
            }, un, new object[] { data, user.OwnerId, id });
            return id.ToString();
        }

        [Route("api/owner/cms/post-update-thread")]
        [HttpPost]
        public string UpdateThread(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = PNUtility.Helpers.UtilityHelper.ParseGuid((string)data.id);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var oidText = oid.ToString();
                var postId = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    var folderPath = "/UData/" + oidText + "/files";
                    var filename = postId.ToString() + ".txt";
                    var sysPath = Helper_App.ToSystemPath(folderPath + "/" + filename);
                    var text = "";
                    try
                    {
                        text = System.IO.File.ReadAllText(sysPath);

                    }
                    catch { }
                    EF.OPost post = context.OPost.Where(p => p.OPostID == postId && p.OwnerId == oid).FirstOrDefault();
                    JPostModel jpostDetail = string.IsNullOrEmpty(text) ? new JPostModel() : PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<JPostModel>(text); ;

                    post.LastUpdate = DateTime.Now;

                    var time = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(post.CreateDate);
                    //post.XOrder = time;
                    post.IsProduct = true;

                    var folderId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.folder);
                    var foldername = "";
                    if (folderId != Guid.Empty)
                    {
                        var folders = CacheCenter.GetFoldersByOwner(oid);
                        var find = folders.Where(f => f.FolderId == folderId).FirstOrDefault();
                        if (find != null)
                        {
                            foldername = find.Name;
                            post.FolderId = folderId;
                        }
                    }
                    post.Title = (string)dyn.title;

                    var photo = (string)data.photo;
                    if (!string.IsNullOrEmpty(photo))
                    {
                        UpdateMultiPhotoModel pictureTool = new UpdateMultiPhotoModel(oidText, jpostDetail.Photo, photo);

                        var pictures = pictureTool.GetResultText();

                        pictureTool.ProcessImagesWithAvatar(oidText);

                        post.Avatar = pictureTool.FirstPhoto;

                        jpostDetail.Photo = pictures;
                    }
                    else
                    {
                        post.Avatar = "";
                        jpostDetail.Photo = "";
                    }

                    post.ShortDesc = (string)dyn.desc;
                    post.Keywords = (string)dyn.keywords;
                    jpostDetail.Detail = (string)dyn.content;

                    post.Price = (decimal)DataHelper.DoubleParse((string)dyn.price, 0);
                    post.IsVND = (string)dyn.priceDVT == "vnd";
                    var store = DataHelper.FloatN100Parse((string)dyn.store);
                    post.Count = store.HasValue ? store.Value : 0;
                    var sale = DataHelper.FloatN100Parse((string)dyn.sale);
                    post.SaleNum = sale.HasValue ? sale.Value : 0;


                    post.DVT = (string)dyn.dvt;
                    post.PCode = (string)dyn.code;

                    jpostDetail.Note = (string)dyn.note;

                    jpostDetail.Videos = (string)dyn.videos;
                    jpostDetail.Links = (string)dyn.links;
                    jpostDetail.Files = (string)dyn.files;

                    var tags = (string)dyn.tags;
                    post.Tags = string.IsNullOrEmpty(tags) ? "" : tags;

                    post.TLink = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3URL(post.Title) + "-" + time;

                    var plainText = foldername + " " + Helper_App.StripHTML(jpostDetail.Detail);
                    var search = plainText + " " + post.PCode + post.Title + post.ShortDesc;
                    var searchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(search);
                    if (searchData.Length >= 3000)
                        searchData = searchData.Substring(0, 2999);

                    post.SaleType = DataHelper.ByteParse((string)dyn.saleType, 0);
                    if (post.SaleType == 1)
                    {
                        post.SaleValue = (decimal)DataHelper.DoubleParse((string)dyn.saleVal, 0);
                    }
                    if (post.SaleType == 2)
                    {
                        post.SaleNum = DataHelper.IntParse((string)dyn.salePer, 0);
                    }

                    post.ViewType = DataHelper.ByteParse((string)dyn.viewType, 0);

                    post.SearchData = searchData;
                    context.SaveChanges();

                    var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(jpostDetail);
                    Helper_App.WriteTextFile(folderPath, filename, jdata);
                }
            }, un, new object[] { data, user.OwnerId, id });
            return id.ToString();
        }

        [Route("api/owner/cms/get-products/{page}/{query?}")]
        [HttpGet]
        public HttpResponseMessage GetProducts(string page, string query = null)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;

                var set = from v in context.OPost
                          select v;

                if (!string.IsNullOrEmpty(query))
                {
                    var text = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(query).ToLower();
                    set = from s in set where s.SearchData.Contains(text) select s;
                }
                var list = set.Where(a => a.OwnerId == user.OwnerId && a.IsProduct).OrderByDescending(a => a.XOrder).Skip(skip).Take(10).ToList();

                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/post-new-article")]
        [HttpPost]
        public string CreateNewArticle(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var oidText = oid.ToString();
                var newId = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    EF.OPost post = new EF.OPost();
                    JPostModel jpostDetail = new JPostModel();
                    post.CreateBy = k;
                    post.CreateDate = DateTime.Now;
                    post.LastUpdate = post.CreateDate;
                    var time = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(post.CreateDate);
                    post.XOrder = time;
                    post.IsProduct = false;
                    post.OwnerId = oid;
                    post.OPostID = newId;

                    var folderId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.folder);
                    var foldername = "";
                    if (folderId != Guid.Empty)
                    {
                        var folders = CacheCenter.GetFoldersByOwner(oid);
                        var find = folders.Where(f => f.FolderId == folderId).FirstOrDefault();
                        if (find != null)
                        {
                            foldername = find.Name;
                            post.FolderId = folderId;
                        }
                    }
                    post.Title = (string)dyn.title;

                    var photo = (string)data.photo;
                    if (!string.IsNullOrEmpty(photo))
                    {
                        UpdateMultiPhotoModel pictureTool = new UpdateMultiPhotoModel(oidText, "", photo);

                        var pictures = pictureTool.GetResultText();

                        pictureTool.ProcessImagesWithAvatar(oidText);

                        post.Avatar = pictureTool.FirstPhoto;

                        jpostDetail.Photo = pictures;
                    }

                    post.ShortDesc = (string)dyn.desc;
                    post.Keywords = (string)dyn.keywords;
                    jpostDetail.Detail = (string)dyn.content;


                    jpostDetail.Videos = (string)dyn.videos;
                    jpostDetail.Links = (string)dyn.links;
                    jpostDetail.Files = (string)dyn.files;

                    var tags = (string)dyn.tags;
                    post.Tags = string.IsNullOrEmpty(tags) ? "" : tags;

                    post.TLink = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3URL(post.Title) + "-" + time;

                    var plainText = foldername + " " + Helper_App.StripHTML(jpostDetail.Detail);
                    var search = plainText + " " + post.PCode + post.Title + post.ShortDesc;
                    var searchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(search);
                    if (searchData.Length >= 3000)
                        searchData = searchData.Substring(0, 2999);
                    
                    post.SearchData = searchData;
                    context.OPost.Add(post);
                    context.SaveChanges();

                    var folderPath = "/UData/" + oidText + "/files";
                    var filename = newId.ToString() + ".txt";
                    var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(jpostDetail);
                    Helper_App.WriteTextFile(folderPath, filename, jdata);
                }
            }, un, new object[] { data, user.OwnerId, id });
            return id.ToString();
        }

        [Route("api/owner/cms/post-update-article")]
        [HttpPost]
        public string UpdateArticle(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = PNUtility.Helpers.UtilityHelper.ParseGuid((string)data.id);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var oidText = oid.ToString();
                var postId = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    var folderPath = "/UData/" + oidText + "/files";
                    var filename = postId.ToString() + ".txt";
                    var sysPath = Helper_App.ToSystemPath(folderPath + "/" + filename);
                    var text = "";
                    try
                    {
                        text = System.IO.File.ReadAllText(sysPath);

                    }
                    catch { }
                    EF.OPost post = context.OPost.Where(p => p.OPostID == postId && p.OwnerId == oid).FirstOrDefault();
                    JPostModel jpostDetail = string.IsNullOrEmpty(text) ? new JPostModel() : PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<JPostModel>(text); ;

                    post.LastUpdate = DateTime.Now;

                    var time = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(post.CreateDate);
                    //post.XOrder = time;
                    post.IsProduct = false;

                    var folderId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.folder);
                    var foldername = "";
                    if (folderId != Guid.Empty)
                    {
                        var folders = CacheCenter.GetFoldersByOwner(oid);
                        var find = folders.Where(f => f.FolderId == folderId).FirstOrDefault();
                        if (find != null)
                        {
                            foldername = find.Name;
                            post.FolderId = folderId;
                        }
                    }
                    post.Title = (string)dyn.title;

                    var photo = (string)data.photo;
                    if (!string.IsNullOrEmpty(photo))
                    {
                        UpdateMultiPhotoModel pictureTool = new UpdateMultiPhotoModel(oidText, jpostDetail.Photo, photo);

                        var pictures = pictureTool.GetResultText();

                        pictureTool.ProcessImagesWithAvatar(oidText);

                        post.Avatar = pictureTool.FirstPhoto;

                        jpostDetail.Photo = pictures;
                    }
                    else
                    {
                        post.Avatar = "";
                        jpostDetail.Photo = "";
                    }

                    post.ShortDesc = (string)dyn.desc;
                    post.Keywords = (string)dyn.keywords;
                    jpostDetail.Detail = (string)dyn.content;

                   
                    jpostDetail.Videos = (string)dyn.videos;
                    jpostDetail.Links = (string)dyn.links;
                    jpostDetail.Files = (string)dyn.files;

                    var tags = (string)dyn.tags;
                    post.Tags = string.IsNullOrEmpty(tags) ? "" : tags;

                    post.TLink = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3URL(post.Title) + "-" + time;

                    var plainText = foldername + " " + Helper_App.StripHTML(jpostDetail.Detail);
                    var search = plainText + " " + post.PCode + post.Title + post.ShortDesc;
                    var searchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(search);
                    if (searchData.Length >= 3000)
                        searchData = searchData.Substring(0, 2999);

                    post.SearchData = searchData;
                    context.SaveChanges();

                    var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(jpostDetail);
                    Helper_App.WriteTextFile(folderPath, filename, jdata);
                }
            }, un, new object[] { data, user.OwnerId, id });
            return id.ToString();
        }

        [Route("api/owner/cms/get-articles/{page}/{query?}")]
        [HttpGet]
        public HttpResponseMessage GetArticles(string page, string query = null)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;

                var set = from v in context.OPost
                          select v;

                if (!string.IsNullOrEmpty(query))
                {
                    var text = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(query).ToLower();
                    set = from s in set where s.SearchData.Contains(text) select s;
                }
                var list = set.Where(a => a.OwnerId == user.OwnerId && !a.IsProduct).OrderByDescending(a => a.XOrder).Skip(skip).Take(10).ToList();

                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }



        [Route("api/owner/cms/do-xoder")]
        [HttpPost]
        public string DoXOder(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var postID = PNUtility.Helpers.UtilityHelper.ParseGuid((string)data.id);
                var findPost = context.OPost.Where(a => a.OPostID == postID && a.OwnerId == user.OwnerId).FirstOrDefault();
                if (findPost == null)
                    return "-1";
                else
                {
                    var type = (string)data.type;
                    EF.OPost target;
                    if (type == "1")
                    {
                        target = context.OPost.Where(a => a.OwnerId == user.OwnerId && a.FolderId == findPost.FolderId && a.XOrder > findPost.XOrder).OrderBy(a => a.XOrder).FirstOrDefault();
                        if (target == null)
                            return "-2";
                        else
                        {
                            MyTasks.DBTask.AddTask((a, k) =>
                            {
                                var cid = (Guid)a[0];
                                var tid = (Guid)a[1];
                                using (var ctx = new EF.HrCenterEntities())
                                {
                                    var cpost = ctx.OPost.Where(p => p.OPostID == cid).FirstOrDefault();
                                    var tpost = ctx.OPost.Where(p => p.OPostID == tid).FirstOrDefault();
                                    var temp = cpost.XOrder;
                                    cpost.XOrder = tpost.XOrder;
                                    tpost.XOrder = temp;
                                    ctx.SaveChanges();
                                }

                            }, "", new object[] { findPost.OPostID, target.OPostID });
                            return target.Title;
                        }
                    }
                    else if (type == "2")
                    {
                        target = context.OPost.Where(a => a.OwnerId == user.OwnerId && a.FolderId == findPost.FolderId && a.XOrder > findPost.XOrder).OrderByDescending(a => a.XOrder).FirstOrDefault();
                        if (target == null)
                            return "-2";
                        else
                        {
                            MyTasks.DBTask.AddTask((a, k) =>
                            {
                                var cid = (Guid)a[0];
                                var tid = (Guid)a[1];
                                using (var ctx = new EF.HrCenterEntities())
                                {
                                    var cpost = ctx.OPost.Where(p => p.OPostID == cid).FirstOrDefault();
                                    var tpost = ctx.OPost.Where(p => p.OPostID == tid).FirstOrDefault();
                                    cpost.XOrder = tpost.XOrder + 1;
                                    ctx.SaveChanges();
                                }

                            }, "", new object[] { findPost.OPostID, target.OPostID });
                            return target.Title;
                        }
                    }
                    else if (type == "3")
                    {
                        target = context.OPost.Where(a => a.OwnerId == user.OwnerId && a.FolderId == findPost.FolderId && a.XOrder < findPost.XOrder).OrderByDescending(a => a.XOrder).FirstOrDefault();
                        if (target == null)
                            return "-2";
                        else
                        {
                            MyTasks.DBTask.AddTask((a, k) =>
                            {
                                var cid = (Guid)a[0];
                                var tid = (Guid)a[1];
                                using (var ctx = new EF.HrCenterEntities())
                                {
                                    var cpost = ctx.OPost.Where(p => p.OPostID == cid).FirstOrDefault();
                                    var tpost = ctx.OPost.Where(p => p.OPostID == tid).FirstOrDefault();
                                    var temp = cpost.XOrder;
                                    cpost.XOrder = tpost.XOrder;
                                    tpost.XOrder = temp;
                                    ctx.SaveChanges();
                                }

                            }, "", new object[] { findPost.OPostID, target.OPostID });
                            return target.Title;
                        }
                    }
                    else if (type == "4")
                    {
                        target = context.OPost.Where(a => a.OwnerId == user.OwnerId && a.FolderId == findPost.FolderId && a.XOrder < findPost.XOrder).OrderBy(a => a.XOrder).FirstOrDefault();
                        if (target == null)
                            return "-2";
                        else
                        {
                            MyTasks.DBTask.AddTask((a, k) =>
                            {
                                var cid = (Guid)a[0];
                                var tid = (Guid)a[1];
                                using (var ctx = new EF.HrCenterEntities())
                                {
                                    var cpost = ctx.OPost.Where(p => p.OPostID == cid).FirstOrDefault();
                                    var tpost = ctx.OPost.Where(p => p.OPostID == tid).FirstOrDefault();
                                    cpost.XOrder = tpost.XOrder - 1;
                                    ctx.SaveChanges();
                                }

                            }, "", new object[] { findPost.OPostID, target.OPostID });
                            return target.Title;
                        }
                    }
                    else
                        return "-3";

                }
            }
        }

        [Route("api/owner/cms/delete-post")]
        [HttpPost]
        public string DeletePost(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) => {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var postID = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.id);
                using (var context = new EF.HrCenterEntities())
                {
                    var findPost = context.OPost.Where(p => p.OPostID == postID && p.OwnerId == oid).FirstOrDefault();
                    if(findPost != null)
                    {
                        var folderPath = "/UData/" + oid.ToString() + "/files";
                        var filename = findPost.OPostID.ToString() + ".txt";
                        var filePath = Helper_App.ToSystemPath(folderPath + "/" + filename);
                        var text = System.IO.File.ReadAllText(filePath);
                        var detailModel = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<Models.JPostModel>(text);
                        var photo = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<Models.PhotoItem>>(detailModel.Photo);
                        Helper_App.DeleteFile(folderPath + "/" + filename);
                        foreach(var p in photo)
                        {
                            Helper_App.DeleteFile(p.URL);
                        }
                        context.OPost.Remove(findPost);
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [Route("api/owner/cms/set-visible")]
        [HttpPost]
        public string SetVisiblePost(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) => {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var postID = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.id);
                var show = (string)dyn.value;
                using (var context = new EF.HrCenterEntities())
                {
                    var findPost = context.OPost.Where(p => p.OPostID == postID && p.OwnerId == oid).FirstOrDefault();
                    if (findPost != null)
                    {
                        findPost.Disabled = show == "y";
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [Route("api/owner/cms/get-albums/{page}/{query?}")]
        [HttpGet]
        public HttpResponseMessage GetArlbums(string page, string query = null)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            using (var context = new EF.HrCenterEntities())
            {
                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;

                var set = from v in context.Album
                          select v;

                if (!string.IsNullOrEmpty(query))
                {
                    var text = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(query).ToLower();
                    set = from s in set where s.SearchData.Contains(text) || s.SearchInName.Contains(text) select s;
                }
                var list = set.Where(a => a.OwnerId == user.OwnerId).OrderByDescending(a => a.CreateDate).Skip(skip).Take(10).ToList();

                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                responseMessage.Content = new StringContent(jdata);
                return responseMessage;
            }
        }

        [Route("api/owner/cms/new-album")]
        [HttpPost]
        public string CreateNewAlbum(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = (Guid)a[2];
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Album album = new EF.Album();
                    album.CBY = k;
                    album.AlbumId = newId;
                    album.CreateDate = DateTime.Now;
                    album.Name = (string)dyn.name;
                    album.OwnerId = oid;
                    album.Keywords = (string)dyn.keywords;
                    album.SDesc = (string)dyn.desc;
                    album.SearchData = "";
                    var time = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(album.CreateDate);
                    album.TLink = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3URL("album " + album.Name + " " + time.ToString());
                    album.SearchInName = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(album.Name + " " + album.Keywords + " " + album.SDesc);
                    context.Album.Add(album);
                    context.SaveChanges();
                }
            }, un, new object[] { data, user.OwnerId, id });
            return id.ToString();
        }

        [Route("api/owner/cms/update-album")]
        [HttpPost]
        public string UpdateAlbum(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = (string)data.id;
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)a[2]);
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Album album = context.Album.Where(s => s.AlbumId == newId && s.OwnerId == oid).FirstOrDefault();
                    if (album != null)
                    {
                        album.Name = (string)dyn.name;
                        album.Keywords = (string)dyn.keywords;
                        album.SDesc = (string)dyn.desc;
                        album.SearchInName = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(album.Name + " " + album.Keywords + " " + album.SDesc);
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId, id });
            return "ok";
        }

        [Route("api/owner/cms/delete-album")]
        [HttpPost]
        public string DeleteAlbum(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = (string)data.id;
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)a[2]);
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Album album = context.Album.Where(s => s.AlbumId == newId && s.OwnerId == oid).FirstOrDefault();
                    if (album != null)
                    {
                        context.Album.Remove(album);
                        context.SaveChanges();
                        var basePath = "/UData/" + oid.ToString() + "/albums/";
                        if (album.AKey.HasValue)
                        {
                            var path = basePath + "covers/" + album.AKey.ToString() + ".jpg";
                            Helper_App.DeleteFile(path);
                        }

                        var txtPath = basePath + "txt/" + album.AlbumId.ToString() + ".txt";
                        Helper_App.DeleteFile(txtPath);

                        var thumPath = basePath + "thumnails/" + album.AlbumId.ToString();
                        Helper_App.DeleteFolder(thumPath);

                        var picPath = basePath + "pictures/" + album.AlbumId.ToString();
                        Helper_App.DeleteFolder(picPath);

                    }
                }
            }, un, new object[] { data, user.OwnerId, id });
            return "ok";
        }

        [Route("api/owner/cms/import-multi-photo")]
        [HttpPost]
        public HttpResponseMessage ImportMultiImages()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var resList = new List<Models.UploadPhotoModel>();
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var aid = HttpContext.Current.Request.Form["album"];
            for (int i = 0; i < 2; i++)
            {
                var text = HttpContext.Current.Request.Form["photo" + i.ToString()];
                var uid = HttpContext.Current.Request.Form["uid" + i.ToString()];
                var size = HttpContext.Current.Request.Form["size" + i.ToString()];               
                var time = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                if (!string.IsNullOrEmpty(text))
                {
                    var split = size.Split('*');
                    var newfile = Convert.FromBase64String(HttpContext.Current.Request.Form["photo" + i.ToString()]);
                    string folderPath = "/UData/" + user.OwnerId.ToString() + "/albums/pictures/" + aid;
                    var sysFolderPath = Helper_App.ToSystemPath(folderPath);
                    if (!System.IO.Directory.Exists(sysFolderPath))
                    {
                        System.IO.Directory.CreateDirectory(sysFolderPath);
                    }

                    string fileName = folderPath + "/" + uid + ".jpg";
                    string sysFileName = Helper_App.ToSystemPath(fileName);
                    System.IO.File.WriteAllBytes(sysFileName, newfile);
                    Models.UploadPhotoModel upload = new UploadPhotoModel();
                    upload.Pid = uid;
                    upload.Time = time;
                    upload.W = PNUtility.Helpers.UtilityHelper.ParseInt(split[0]);
                    upload.H = PNUtility.Helpers.UtilityHelper.ParseInt(split[1]);
                    upload.U = un;
                    resList.Add(upload);
                    MyTasks.FileTask.AddTask((a, k) => {
                        var albumid = (string)a[0];
                        var fileid = (string)a[1];
                        var sourceFile =  "/UData/" + k + "/albums/pictures/" + albumid + "/" + fileid + ".jpg";
                        sourceFile = Helper_App.ToSystemPath(sourceFile);
                        var targetFile = "/UData/" + k + "/albums/thumnails/" + albumid + "/" + fileid + ".jpg";
                        targetFile = Helper_App.ToSystemPath(targetFile);
                        var pic = Bitmap.FromFile(sourceFile);
                        var w = (150 * pic.Width) / pic.Height;
                        var isize = new Size(w, 150);
                        var resized = Helper_App.ResizeImage(pic, isize);

                        var encoder = ImageCodecInfo.GetImageEncoders().First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                        var encParams = new EncoderParameters(1);
                        encParams.Param[0] = new EncoderParameter(Encoder.Quality, 30L);
                        resized.Save(targetFile, encoder, encParams);
                        pic.Dispose();

                    }, user.OwnerId.ToString(), new object[] { aid, uid });
                }
            }
            var resText = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(resList);
            response.Content = new StringContent(resText);
            return response;
        }

        [Route("api/owner/cms/save-album-photo")]
        [HttpPost]
        public string SaveAlbumPhoto(dynamic data)
        {
            
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);       
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var albumText = (string)dyn.album;
                var albumId = PNUtility.Helpers.UtilityHelper.ParseGuid(albumText);

                using (var context = new EF.HrCenterEntities())
                {
                    EF.Album album = context.Album.Where(s => s.AlbumId == albumId && s.OwnerId == oid).FirstOrDefault();


                    var newIds = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<Models.UploadPhotoModel>>((string)dyn.newPics);
                    var path = "/UData/" + oid.ToString() + "/albums/txt/" + albumId.ToString() + ".txt";
                    var sysPath = Helper_App.ToSystemPath(path);
                    List<Models.UploadPhotoModel> pictures = new List<Models.UploadPhotoModel>();
                    if (System.IO.File.Exists(sysPath))
                    {
                        var text = System.IO.File.ReadAllText(sysPath);
                        pictures = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<Models.UploadPhotoModel>>(text);
                    }

                    pictures = newIds.Concat(pictures).ToList();
                    var saveText = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(pictures);
                    System.IO.File.WriteAllText(sysPath, saveText);
                    album.PicCount = pictures.Count;

                    var newKey = Guid.NewGuid();
                    if (album.AKey.HasValue)
                    {
                        var coverPath = "/UData/" + oid.ToString() + "/albums/covers/" + album.AKey.Value.ToString().ToLower() + ".jpg";
                        Helper_App.DeleteFile(coverPath);

                    }

                    album.AKey = newKey;
                    var search = "";
                    foreach (var item in pictures)
                    {
                        search = search + item.Desc + " ";
                    }
                    album.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(search);
                    context.SaveChanges();

                    buildCoverPhoto(newKey.ToString().ToLower(), oid.ToString(), albumId.ToString(), pictures);
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        private void buildCoverPhoto(string newid, string oid, string aid, List<Models.UploadPhotoModel> pictures)
        {
            Bitmap target = new Bitmap(600, 400);
            var count = pictures.Count;
            if (count == 0)
            {

            }
            else if (count == 1)
            {
                var IM1 = pictures.ElementAt(0);
                var realPath1 = IM1.GetPictureURL(oid, aid);
                var pic1 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath1));
                var resPic1 = Helper_App.CropImage(pic1, 600, 400);
                target.MakeTransparent();
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.Clear(Color.White);

                    g.DrawImage(resPic1, new Rectangle(0, 0, 600, 400),
                                     new Rectangle(0, 0, resPic1.Width, resPic1.Height),
                                     GraphicsUnit.Pixel);
                }
                var encoder = ImageCodecInfo.GetImageEncoders()
                            .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                var encParams = new EncoderParameters(1);
                encParams.Param[0] = new EncoderParameter(Encoder.Quality, 70L);
                target.Save(Helper_App.ToSystemPath("/UData/" + oid + "/albums/covers/" + newid + ".jpg"), encoder, encParams);
                pic1.Dispose();
            }
            else if (count == 2)
            {
                var IM1 = pictures.ElementAt(0);
                var realPath1 = IM1.GetPictureURL(oid, aid);
                var pic1 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath1));

                var IM2 = pictures.ElementAt(1);
                var realPath2 = IM2.GetPictureURL(oid, aid);
                var pic2 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath2));

                var resPic1 = Helper_App.CropImage(pic1, 299, 400);
                var resPic2 = Helper_App.CropImage(pic2, 299, 400);
                target.MakeTransparent();
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.Clear(Color.White);

                    g.DrawImage(resPic1, new Rectangle(0, 0, 299, 400),
                                     new Rectangle(0, 0, resPic1.Width, resPic1.Height),
                                     GraphicsUnit.Pixel);
                    g.DrawImage(resPic2, new Rectangle(301, 0, 299, 400),
                                     new Rectangle(0, 0, resPic2.Width, resPic2.Height),
                                     GraphicsUnit.Pixel);
                }
                var encoder = ImageCodecInfo.GetImageEncoders()
                            .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                var encParams = new EncoderParameters(1);
                encParams.Param[0] = new EncoderParameter(Encoder.Quality, 70L);
                target.Save(Helper_App.ToSystemPath("/UData/" + oid + "/albums/covers/" + newid + ".jpg"), encoder, encParams);
                pic1.Dispose();
                pic2.Dispose();
            }
            else if (count == 3)
            {
                var IM1 = pictures.ElementAt(0);
                var realPath1 = IM1.GetPictureURL(oid, aid);

                var IM2 = pictures.ElementAt(1);
                var realPath2 = IM2.GetPictureURL(oid, aid); ;

                var IM3 = pictures.ElementAt(2);
                var realPath3 = IM3.GetPictureURL(oid, aid);

                var pic1 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath1));
                var pic2 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath2));
                var pic3 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath3));

                var resPic1 = Helper_App.CropImage(pic1, 299, 400);
                var resPic2 = Helper_App.CropImage(pic2, 299, 199);
                var resPic3 = Helper_App.CropImage(pic3, 299, 199);
                target.MakeTransparent();
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.Clear(Color.White);

                    g.DrawImage(resPic1, new Rectangle(0, 0, 299, 400),
                                     new Rectangle(0, 0, resPic1.Width, resPic1.Height),
                                     GraphicsUnit.Pixel);
                    g.DrawImage(resPic2, new Rectangle(301, 0, 299, 199),
                                     new Rectangle(0, 0, resPic2.Width, resPic2.Height),
                                     GraphicsUnit.Pixel);
                    g.DrawImage(resPic3, new Rectangle(301, 201, 299, 199),
                                     new Rectangle(0, 0, resPic3.Width, resPic3.Height),
                                     GraphicsUnit.Pixel);
                }
                var encoder = ImageCodecInfo.GetImageEncoders()
                            .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                var encParams = new EncoderParameters(1);
                encParams.Param[0] = new EncoderParameter(Encoder.Quality, 70L);
                target.Save(Helper_App.ToSystemPath("/UData/" + oid + "/albums/covers/" + newid + ".jpg"), encoder, encParams);
                pic1.Dispose();
                pic2.Dispose();
                pic3.Dispose();
            }
            else
            {
                var IM1 = pictures.ElementAt(0);
                var realPath1 = IM1.GetPictureURL(oid, aid);

                var IM2 = pictures.ElementAt(1);
                var realPath2 = IM2.GetPictureURL(oid, aid); ;

                var IM3 = pictures.ElementAt(2);
                var realPath3 = IM3.GetPictureURL(oid, aid);

                var IM4 = pictures.ElementAt(3);
                var realPath4 = IM4.GetPictureURL(oid, aid);

                var pic1 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath1));
                var pic2 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath2));
                var pic3 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath3));
                var pic4 = Bitmap.FromFile(Helper_App.ToSystemPath(realPath4));

                var resPic1 = Helper_App.CropImage(pic1, 299, 199);
                var resPic2 = Helper_App.CropImage(pic2, 299, 199);
                var resPic3 = Helper_App.CropImage(pic3, 299, 199);
                var resPic4 = Helper_App.CropImage(pic4, 299, 199);
                target.MakeTransparent();
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.Clear(Color.White);

                    g.DrawImage(resPic1, new Rectangle(0, 0, 299, 199),
                                     new Rectangle(0, 0, resPic1.Width, resPic1.Height),
                                     GraphicsUnit.Pixel);

                    g.DrawImage(resPic4, new Rectangle(0, 201, 299, 199),
                                     new Rectangle(0, 0, resPic4.Width, resPic4.Height),
                                     GraphicsUnit.Pixel);

                    g.DrawImage(resPic2, new Rectangle(301, 0, 299, 199),
                                     new Rectangle(0, 0, resPic2.Width, resPic2.Height),
                                     GraphicsUnit.Pixel);
                    g.DrawImage(resPic3, new Rectangle(301, 201, 299, 199),
                                     new Rectangle(0, 0, resPic3.Width, resPic3.Height),
                                     GraphicsUnit.Pixel);

                    //if (count > 4)
                    //{
                    //    g.FillEllipse(Brushes.White, new Rectangle(420, 270, 60, 60));
                    //    var o = count - 4;
                    //    g.DrawString("+" + o.ToString(), new Font("Arial", 30), Brushes.Red, 417, 280, StringFormat.GenericDefault);
                    //}
                }
                var encoder = ImageCodecInfo.GetImageEncoders()
                            .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                var encParams = new EncoderParameters(1);
                encParams.Param[0] = new EncoderParameter(Encoder.Quality, 70L);
                target.Save(Helper_App.ToSystemPath("/UData/" + oid + "/albums/covers/" + newid + ".jpg"), encoder, encParams);
                pic1.Dispose();
                pic2.Dispose();
                pic3.Dispose();
                pic4.Dispose();
            }
        }

        [Route("api/owner/cms/update-photo-desc")]
        [HttpPost]
        public string UpdatePhotoDesc(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var albumId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.albumId);
                var photoId = (string)dyn.photoId;
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Album album = context.Album.Where(s => s.AlbumId == albumId && s.OwnerId == oid).FirstOrDefault();
                    var path = "/UData/" + oid.ToString() + "/albums/txt/" + albumId.ToString() + ".txt";
                    var sysPath = Helper_App.ToSystemPath(path);
                    List<Models.UploadPhotoModel> pictures = new List<Models.UploadPhotoModel>();
                    if (System.IO.File.Exists(sysPath))
                    {
                        var text = System.IO.File.ReadAllText(sysPath);
                        pictures = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<Models.UploadPhotoModel>>(text);
                    }
                    var search = "";
                    foreach(var item in pictures)
                    {                        
                        if(item.Pid == photoId)
                        {
                            item.Desc = (string)dyn.desc;
                        }
                        search = search + item.Desc + " ";
                    }
                    album.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(search);
                    context.SaveChanges();
                    System.IO.File.WriteAllText(sysPath, PNUtility.Helpers.JSONHelper.Jsoner.Serialize(pictures));
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [Route("api/owner/cms/delete-photo")]
        [HttpPost]
        public string DeletePhoto(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var albumId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.albumId);
                var photoId = (string)dyn.photoId;
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Album album = context.Album.Where(s => s.AlbumId == albumId && s.OwnerId == oid).FirstOrDefault();
                    var path = "/UData/" + oid.ToString() + "/albums/txt/" + albumId.ToString() + ".txt";
                    var sysPath = Helper_App.ToSystemPath(path);
                    List<Models.UploadPhotoModel> pictures = new List<Models.UploadPhotoModel>();
                    if (System.IO.File.Exists(sysPath))
                    {
                        var text = System.IO.File.ReadAllText(sysPath);
                        pictures = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<Models.UploadPhotoModel>>(text);
                    }
                    var search = "";
                    Models.UploadPhotoModel find = null;
                    foreach (var item in pictures)
                    {
                        if (item.Pid == photoId)
                        {
                            find = item;
                        }
                        else
                        {
                            search = search + item.Desc + " ";
                        }
                    }
                    if (find != null)
                    {
                        pictures.Remove(find);
                        var picURL = find.GetPictureURL(oid.ToString(), albumId.ToString());
                        Helper_App.DeleteFile(picURL);

                        var thumURL = find.GetThumnailURL(oid.ToString(), albumId.ToString());
                        Helper_App.DeleteFile(thumURL);
                    }

                    album.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(search);
                    album.PicCount = pictures.Count;
                    context.SaveChanges();
                    System.IO.File.WriteAllText(sysPath, PNUtility.Helpers.JSONHelper.Jsoner.Serialize(pictures));
                }
            }, un, new object[] { data, user.OwnerId });
            return "ok";
        }

        [HttpPost]
        [Route("api/owner/cms/upload-file")]
        public string UploadFile()
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);

            if (HttpContext.Current.Request.ContentLength <= 10485760 && HttpContext.Current.Request.Files.AllKeys.Any())
            {
                try
                {
                    var httpPostedFile = HttpContext.Current.Request.Files["UploadedFile"];
                    var extension = Path.GetExtension(httpPostedFile.FileName);
                    var l = extension.Length + 1;
                    var filename = httpPostedFile.FileName.Substring(0, httpPostedFile.FileName.Length - l);
                    if (httpPostedFile != null)
                    {
                        var newGUID = Guid.NewGuid();
                        var path = "/UData/" + user.OwnerId.ToString() + "/files/" + newGUID.ToString() + extension;

                        var fileSavePath = Helper_App.ToSystemPath(path);
                        httpPostedFile.SaveAs(fileSavePath);

                        MyTasks.DBTask.AddTask((a, k) =>
                        {
                            var oid = (Guid)a[0];
                            var fid = (Guid)a[1];
                            var fileNameTask = (string)a[2];
                            var extensionTask = (string)a[3];
                            using (var context = new EF.HrCenterEntities())
                            {
                                EF.File file = new EF.File();
                                file.IsUpload = true;
                                file.Name = (string)a[2] + extensionTask;
                                file.FullName = (string)a[2] + extensionTask;
                                file.FileId = fid;
                                file.Link = "";
                                file.OwnerId = oid;
                                file.Type = getFileType(extensionTask);
                                file.Extension = extensionTask;
                                file.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(file.Name);
                                context.File.Add(file);
                                context.SaveChanges();
                            }
                        }, un, new object[] { user.OwnerId, newGUID, filename, extension });
                    }
                    return extension;
                }
                catch(Exception exx)
                {
                    return exx.Message;
                }
            }
            else
                return "fail";
        }

        [Route("api/owner/cms/delete-file")]
        [HttpPost]
        public string DeleteFile(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = (string)data.id;
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)a[2]);
                using (var context = new EF.HrCenterEntities())
                {
                    EF.File file = context.File.Where(s => s.FileId == newId && s.OwnerId == oid).FirstOrDefault();
                    if (file != null)
                    {
                        context.File.Remove(file);
                        context.SaveChanges();
                        if (file.IsUpload)
                        {
                            var path = "/UData/" + oid.ToString() + "/files/" + file.FileId.ToString() + file.Extension;
                            Helper_App.DeleteFile(path);
                        }

                    }
                }
            }, un, new object[] { data, user.OwnerId, id });
            return "ok";
        }

        [Route("api/owner/cms/update-file")]
        [HttpPost]
        public string UpdateFile(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = (string)data.id;
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)a[2]);
                using (var context = new EF.HrCenterEntities())
                {
                    EF.File file = context.File.Where(s => s.FileId == newId && s.OwnerId == oid).FirstOrDefault();
                    if (file != null)
                    {
                        file.Name = (string)dyn.name;
                        context.SaveChanges();
                    }
                }
            }, un, new object[] { data, user.OwnerId, id });
            return "ok";
        }

        [Route("api/owner/cms/delete-link")]
        [HttpPost]
        public string DeleteLink(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = (string)data.id;
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)a[2]);
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Link link = context.Link.Where(s => s.LinkId == newId && s.OwnerId == oid).FirstOrDefault();
                    if (link != null)
                    {
                        context.Link.Remove(link);
                        context.SaveChanges();
                        if (link.IsPhoto)
                        {
                            var path = "/UData/" + oid.ToString() + "/banners/" + link.LinkId.ToString() + ".jpg";
                            Helper_App.DeleteFile(path);
                        }

                    }
                }
            }, un, new object[] { data, user.OwnerId, id });
            return "ok";
        }

        [Route("api/owner/cms/update-link")]
        [HttpPost]
        public string UpdateLink(dynamic data)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var id = (string)data.id;
            MyTasks.DBTask.AddTask((a, k) =>
            {
                var dyn = (dynamic)a[0];
                var oid = (Guid)a[1];
                var newId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)a[2]);
                using (var context = new EF.HrCenterEntities())
                {
                    EF.Link link = context.Link.Where(s => s.LinkId == newId && s.OwnerId == oid).FirstOrDefault();
                    if(link != null)
                    {
                        link.IsBlank = ((string)dyn.blank) == "true";                        
                        link.Text = (string)dyn.text;
                        link.URL = (string)dyn.url;
                        link.SearchData = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(link.URL + " " + link.Text);
                        context.SaveChanges();
                    }                    
                }
            }, un, new object[] { data, user.OwnerId, id });
            return "ok";            
        }

        private int getFileType(string ext)
        {
            if (ext == ".pdf")
                return 0;
            else if (ext == ".doc" || ext == ".docx")
                return 1;
            else if (ext == ".xls" || ext == ".xlsx")
                return 2;
            else if (ext == ".ppt" || ext == ".pptx")
                return 3;
            else if (ext == ".rar" || ext == ".zip")
                return 4;
            else if (ext == ".exe")
                return 5;
            else if (ext == ".html")
                return 6;
            else if (ext == ".jpg" || ext == ".png" || ext == ".bmp" || ext == ".gif" || ext == ".svg")
                return 7;
            else
                return 8;
        }

    }
}
