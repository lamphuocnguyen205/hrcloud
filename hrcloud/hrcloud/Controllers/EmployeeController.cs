﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrcloud.Controllers
{
    [Authorize(Roles = "C_EMP")]
    public class EmployeeController : Controller
    {
        // GET: HRM

        [Route("hr-cloud/employee/overview")]
        [Authorize]
        public ActionResult EmployeeOverview()
        {
            return View();
        }
       
    }
}