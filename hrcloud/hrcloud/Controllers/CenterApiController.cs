﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace hrcloud.Controllers
{
    [Authorize(Roles = "M_ADM")]
    public class CenterApiController : ApiController
    {
        [Route("api/management/create-owner")]
        [HttpPost]
        public string CreateOwner(dynamic data)
        {

            var nid = Guid.NewGuid().ToString();
            MyTasks.DBTask.AddTask((d, k) => {
                var obj = (dynamic)d[0];
                var name = (string)obj.name;
                var domain = (string)obj.domain;
                var server = (string)obj.server;
                using(var context = new EF.HrCenterEntities())
                {
                    EF.Owner o = new EF.Owner();
                    o.OwnerId = Guid.Parse(k);
                    o.Name = name;
                    o.ServerId = int.Parse(server);
                    o.Domain = domain;
                    o.Status = 0;
                    o.CreateDate = DateTime.Now;
                    context.Owner.Add(o);
                    context.SaveChanges();
                }
            }, nid, new object[] { data });
            return nid;
        }

        [Route("api/management/create-extra-function")]
        [HttpPost]
        public string CreateExtraFunction(dynamic data)
        {
            Guid id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[1];
                var newId = (Guid)args[0];
                var oid = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.oid);
                var name = (string)dyn.name;
                using(var context = new EF.HrCenterEntities())
                {
                    var widget = new EF.Widget();
                    widget.OwnerId = oid;
                    widget.Name = name;
                    widget.WidgetId = newId;
                    context.Widget.Add(widget);
                    context.SaveChanges();
                }
            }, "", new object[] { id, data });
            return id.ToString();
            
        }
    }
}
