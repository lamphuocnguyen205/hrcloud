﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrcloud.Controllers
{
    [Authorize(Roles = "C_ADM,C_EMP")]
    public class OnBoardController : Controller
    {
        // GET: OnBoard
        [Route("hr-cloud/on-board/feeds")]
        public ActionResult OnBoardOverview()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Tin cập nhật";
            return View();
        }

        [Route("hr-cloud/on-board/day-working")]
        public ActionResult OnBoardDayWorking()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Chấm công làm việc";
            return View();
        }

        [Route("hr-cloud/on-board/advance")]
        public ActionResult OnBoardAdvance()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Tạm ứng tiền";
            return View();
        }

        [Route("hr-cloud/my-account")]
        [Authorize]
        public ActionResult MyAccount()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "My Account";
            if (Session["change-password"] != null && (string)Session["change-password"] == "ok")
            {
                ViewBag.Message = "Đổi mật khẩu thành công";
                Session["change-password"] = "";
                HttpCookie cpass = new HttpCookie("sign_pass");
                cpass.Value = Session["password"].ToString();
                cpass.Expires = DateTime.Now.AddMonths(1);
                Response.Cookies.Add(cpass);
            }
            else if (Session["change-password"] != null && (string)Session["change-password"] == "no")
            {
                ViewBag.Message = "Không thể đổi mật khẩu, vui lòng thử lại!";
                Session["change-password"] = "";
            }
            else
                ViewBag.Message = "";

            return View();
        }

        private ClientModel GetClientModel()
        {
            var username = User.Identity.Name;
            var index = username.IndexOf('_');
            var domain = username.Substring(0, index);
            var token = AccessTokenManager.GetAccessToken(username);
            ClientModel cm = new ClientModel();
            cm.Token = token;
            cm.Username = username;
            var owner = CacheCenter.DomainMapping[domain];
            cm.Domain = domain;
            cm.HostURL = owner.Server.URL;
            var user = CacheCenter.GetUserCached(username);
            cm.Owner = owner;
            cm.QUser = user;
            cm.Changes = CacheCenter.GetChangeDataModel(domain);
            cm.UChanges = CacheCenter.GetUserChanges(username);
            cm.Timestamp = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
            return cm;
        }
    }
}