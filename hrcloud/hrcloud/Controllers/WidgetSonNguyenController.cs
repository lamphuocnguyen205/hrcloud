﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrcloud.Controllers
{
    public class WidgetSonNguyenController : Controller
    {
        // GET: WidgetSonNguyen
        [Route("hr-cloud/widget/sn/add-home-working")]
        [Authorize]
        public ActionResult AddHomeWorking()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Thêm gói gia công";
            var me = User.Identity.Name;
            bool isManager = false;
            var manager = Services.Widgets.SonNguyenCacheService.ManagerHUsers.Where(a => a == me).FirstOrDefault();
            if (!string.IsNullOrEmpty(manager))
            {
                isManager = true;
            }

            ViewBag.Manager = isManager;
            return View();
        }

        [Authorize(Roles = "C_ADM")]
        [Route("hr-cloud/widget/sn/cai-dat-gia-cong")]
        public ActionResult SettingHomeWorking()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Thiết lập gia công";
            using(var context = new EF.HrCenterEntities())
            {
                var id = PNUtility.Helpers.UtilityHelper.ParseGuid("012B8304-659F-4B07-9688-299EF1AA68DA");
                var widget = context.Widget.Where(a => a.WidgetId == id).FirstOrDefault();
                Models.SonNguyen.HWUsers hwuser = new Models.SonNguyen.HWUsers();
                if (!string.IsNullOrEmpty(widget.ConfigValue))
                    hwuser = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<Models.SonNguyen.HWUsers>(widget.ConfigValue);

                ViewBag.Config = hwuser;
            }
            return View();
        }

        [Route("hr-cloud/widget/sn/gia-cong")]
        [Authorize]
        public ActionResult HomeWorkingPage(string username, string fromDate, string toDate, string nail, string grit, string status, string page, string view)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            long vfrom = 0;
            var now = DateTime.Now;
            long vto = 0;
            var me = User.Identity.Name;
            bool isManager = false;
            var manager = Services.Widgets.SonNguyenCacheService.ManagerHUsers.Where(a => a == me).FirstOrDefault();
            if (!string.IsNullOrEmpty(manager))
            {
                isManager = true;
            }
            else
                username = me;
            ViewBag.Manager = isManager;

            if (string.IsNullOrEmpty(fromDate))
            {
                var text = "01/" + (now.Month <10 ? "0" + now.Month.ToString() : now.Month.ToString()) + "/" + now.Year.ToString();
                var time = DateTime.ParseExact(text, "dd/MM/yyyy", provider).AddDays(-30);
                vfrom = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(time);
                ViewBag.FromDate = time.ToString("dd/MM/yyyy");
            }
            else
            {
                var text = fromDate.Replace('-', '/');
                ViewBag.FromDate = text;
                var time = DateTime.ParseExact(text, "dd/MM/yyyy", provider);
                vfrom = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(time);
            }

            if (!string.IsNullOrEmpty(toDate))
            {
                var text = toDate.Replace('-', '/');
                var time = DateTime.ParseExact(text, "dd/MM/yyyy", provider);
                time = time.AddHours(24);
                vto = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(time);
                ViewBag.ToDate = text;
            }
            else
            {
                vto = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(now);
                var text = (now.Day < 10 ? "0" + now.Day.ToString() : now.Day.ToString()) + "/" + (now.Month < 10 ? "0" + now.Month.ToString() : now.Month.ToString()) + "/" + now.Year.ToString();
                ViewBag.ToDate = text;
            }

            int vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
            vpage = vpage <= 0 ? 1 : vpage;
            int take = 20;
            int skip = (vpage - 1) * take;
            using(var context = new EFWIDGET.HrWidgetEntities())
            {
                var dataSource = context.SN_Package.Where(a => a.CreateDate >= vfrom && a.CreateDate <= vto);
                
                if (!string.IsNullOrEmpty(username))
                {
                    dataSource = dataSource.Where(a => a.AssignTo == username);
                    ViewBag.Username = username;
                }
                else
                    ViewBag.Username = "0";
                if (!string.IsNullOrEmpty(nail) && nail != "0")
                {
                    int vnail = PNUtility.Helpers.UtilityHelper.ParseInt(nail);
                    dataSource = dataSource.Where(a => a.NailId == vnail);
                    ViewBag.Nail = nail;
                }
                else
                    ViewBag.Nail = "0";

                if (!string.IsNullOrEmpty(grit) && grit != "0")
                {
                    int vgrit = PNUtility.Helpers.UtilityHelper.ParseInt(grit);
                    dataSource = dataSource.Where(a => a.GritId == vgrit);
                    ViewBag.Grit = grit;
                }
                else
                    ViewBag.Grit = "0";

                if (string.IsNullOrEmpty(status))
                {
                    dataSource = dataSource.Where(a => a.IsCancel == false && a.IsCompleted == false);
                    ViewBag.Status = "1";
                }
                else
                {
                    var vstatus = PNUtility.Helpers.UtilityHelper.ParseInt(status);
                    if (vstatus == 0)
                        dataSource = dataSource.Where(a => a.IsCancel == false);
                    else if (vstatus == 1)
                        dataSource = dataSource.Where(a => a.IsCancel == false && a.IsCompleted == false);
                    else if (vstatus == 2)
                        dataSource = dataSource.Where(a => a.IsCancel == false && a.IsCompleted == true);
                    else if (vstatus == 3)
                        dataSource = dataSource.Where(a => a.IsCancel == true);
                    ViewBag.Status = vstatus;
                }

                var cm = GetClientModel();
                ViewBag.Owner = cm.Owner;
                ViewBag.ClientModel = cm;
                ViewBag.PageName = "Trang Chủ Gia Công";
                if(view == "all")
                {
                    var results = dataSource.OrderByDescending(a => a.CreateDate).ToList();
                    ViewBag.Display = "2";
                    return View(results);
                }
                else
                {
                    if (string.IsNullOrEmpty(view))
                    {
                        take = 50;
                        ViewBag.Display = "0";
                    }
                    else
                    {
                        take = 200;
                        ViewBag.Display = "1";
                    }
                       
                    var results = dataSource.OrderByDescending(a => a.CreateDate).Skip(skip).Take(take).ToList();
                    return View(results);
                }                
            }



        }

        [Route("hr-cloud/widget/sn/import-fuel")]
        [Authorize]
        public ActionResult ImportFuel()
        {
            return View();
        }

        [Route("hr-cloud/my-home-working-account")]
        [Authorize]
        public ActionResult MyHomeWorkingAccount()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "My Account";
            if (Session["change-password"] != null && (string)Session["change-password"] == "ok")
            {
                ViewBag.Message = "Đổi mật khẩu thành công";
                Session["change-password"] = "";
                HttpCookie cpass = new HttpCookie("sign_pass");
                cpass.Value = Session["password"].ToString();
                cpass.Expires = DateTime.Now.AddMonths(1);
                Response.Cookies.Add(cpass);
            }
            else if (Session["change-password"] != null && (string)Session["change-password"] == "no")
            {
                ViewBag.Message = "Không thể đổi mật khẩu, vui lòng thử lại!";
                Session["change-password"] = "";
            }
            else
                ViewBag.Message = "";
            ViewBag.Manager = false;
            return View();
        }


        [Route("hr-cloud/widget/sn/user-summary")]
        [Authorize]
        public ActionResult UserSummary(string username)
        {
            var me = User.Identity.Name;
            

            bool isManager = false;
            var manager = Services.Widgets.SonNguyenCacheService.ManagerHUsers.Where(a => a == me).FirstOrDefault();
            if (!string.IsNullOrEmpty(manager))
            {
                isManager = true;
            }
            else
                username = me;
            ViewBag.Manager = isManager;

            if (!isManager)
            {
                if (!string.IsNullOrEmpty(username))
                {
                    if(me != username)
                        return RedirectToAction("NotPermission");
                }
            }

            if (string.IsNullOrEmpty(username))
                username = me;

            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var user = context.SN_PUser.Where(a => a.UserId == username && a.IsActive).FirstOrDefault();
                if (user == null)
                    return RedirectToAction("NotFound");
                else
                {
                    var orders = context.SN_Package.Where(a => a.AssignTo == username && !a.IsCancel && !a.IsCompleted).OrderByDescending(a=>a.CreateDate).ToList();
                    ViewBag.Orders = orders;
                    var cm = GetClientModel();
                    ViewBag.Owner = cm.Owner;
                    ViewBag.ClientModel = cm;
                    ViewBag.PageName = "Thông Tin Gia Công";
                    ViewBag.Username = username;
                    return View(user);
                }
            }
        }

        [Route("hr-cloud/widget/sn/user-latest-approved")]
        [Authorize]
        public ActionResult UserLatestApproved(string username)
        {
            var me = User.Identity.Name;
            bool isManager = false;
            var manager = Services.Widgets.SonNguyenCacheService.ManagerHUsers.Where(a => a == me).FirstOrDefault();
            if (me == "sn_dongnguyenxuan")
            {
                manager = "sn_dongnguyenxuan";
            }

            if (!string.IsNullOrEmpty(manager))
            {
                isManager = true;
            }
            else
                username = me;            

            ViewBag.Manager = isManager;            

            if (!isManager)
            {
                if (!string.IsNullOrEmpty(username))
                {
                    if (me != username)
                        return RedirectToAction("NotPermission");
                }
            }

            if (string.IsNullOrEmpty(username))
                username = me;

            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var user = context.SN_PUser.Where(a => a.UserId == username && a.IsActive).FirstOrDefault();
                if (user == null)
                    return RedirectToAction("NotFound");
                else
                {
                    var orders = context.SN_Package.Where(a => a.AssignTo == username && !a.IsCancel && a.IsCompleted).OrderByDescending(a => a.ApproveDate).Take(50).ToList();
                    ViewBag.Orders = orders;
                    var cm = GetClientModel();
                    ViewBag.Owner = cm.Owner;
                    ViewBag.ClientModel = cm;
                    ViewBag.PageName = "Đã hoàn thành";
                    ViewBag.Username = username;
                    return View(user);
                }
            }
        }

        [Route("hr-cloud/widget/sn/user-transactions")]
        [Authorize]
        public ActionResult Transactions(string username)
        {
            var me = User.Identity.Name;

            bool isManager = false;
            var manager = Services.Widgets.SonNguyenCacheService.ManagerHUsers.Where(a => a == me).FirstOrDefault();

            if (me == "sn_dongnguyenxuan")
            {
                manager = "sn_dongnguyenxuan";
            }
            
            if (!string.IsNullOrEmpty(manager))
            {
                isManager = true;
            }
            else
                username = me;
            ViewBag.Manager = isManager;

            if (!isManager)
            {
                if (!string.IsNullOrEmpty(username))
                {
                    if (me != username)
                        return RedirectToAction("NotPermission");
                }
            }

            if (string.IsNullOrEmpty(username))
                username = me;

            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var user = context.SN_PUser.Where(a => a.UserId == username && a.IsActive).FirstOrDefault();
                if (user == null)
                    return RedirectToAction("NotFound");
                else
                {
                    var trans = context.SN_Transaction.Where(a => a.Username == username).OrderByDescending(a => a.CreateDate).ToList();
                    ViewBag.Trans = trans;
                    var cm = GetClientModel();
                    ViewBag.Owner = cm.Owner;
                    ViewBag.ClientModel = cm;
                    ViewBag.PageName = "Lịch sử thanh toán";
                    ViewBag.Username = username;
                    return View(user);
                }
            }
        }

        [Route("hr-cloud/widget/sn/user-package-changes")]
        [Authorize]
        public ActionResult UserPackageChanges(string username)
        {
            var me = User.Identity.Name;

            bool isManager = false;
            var manager = Services.Widgets.SonNguyenCacheService.ManagerHUsers.Where(a => a == me).FirstOrDefault();
            
            if (me == "sn_dongnguyenxuan")
            {
                manager = "sn_dongnguyenxuan";
            }

            if (!string.IsNullOrEmpty(manager))
            {
                isManager = true;
            }
            else
                username = me;
            ViewBag.Manager = isManager;

            if (!isManager)
            {
                if (!string.IsNullOrEmpty(username))
                {
                    if (me != username)
                        return RedirectToAction("NotPermission");
                }
            }

            if (string.IsNullOrEmpty(username))
                username = me;

            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var user = context.SN_PUser.Where(a => a.UserId == username && a.IsActive).FirstOrDefault();
                if (user == null)
                    return RedirectToAction("NotFound");
                else
                {
                    var changes = context.SN_Change.Where(a => a.GCUsername == username).OrderByDescending(a => a.Time).ToList();
                    ViewBag.Changes = changes;
                    var cm = GetClientModel();
                    ViewBag.Owner = cm.Owner;
                    ViewBag.ClientModel = cm;
                    ViewBag.PageName = "Lịch sử thay đổi";
                    ViewBag.Username = username;
                    return View(user);
                }
            }
        }

        [Route("hr-cloud/widget/sn/printer/thanh-toan-gia-cong/{id}")]
        [Authorize(Roles = "C_ADM")]
        public ActionResult PrinterPayment(string id)
        {
            using(var context = new EFWIDGET.HrWidgetEntities())
            {
                var time = PNUtility.Helpers.UtilityHelper.ParseLong(id);
                var find = context.SN_Transaction.Where(a => a.CreateDate == time).FirstOrDefault();
                if (find != null)
                {
                    var cm = GetClientModel();
                    ViewBag.Owner = cm.Owner;
                    ViewBag.ClientModel = cm;
                    ViewBag.PageName = "Lịch sử thanh toán";
                    ViewBag.Username = User.Identity.Name;
                    var user = context.SN_PUser.Where(a => a.UserId == find.Username).FirstOrDefault();
                    ViewBag.GUSER = user;
                    return View(find);
                }
                else
                    return RedirectToAction("NotFoundPrinter");

            }
        }

        [Route("hr-cloud/widget/sn/order/management")]
        [Authorize(Roles = "C_ADM")]
        public ActionResult OrderManagement()
        {
            string username = "";
            bool isManager = false;
            var me = User.Identity.Name;
            Services.Widgets.SonNguyenCacheService.ResetOrder();
            var manager = Services.Widgets.SonNguyenCacheService.ManagerHUsers.Where(a => a == me).FirstOrDefault();

            if (me == "sn_dongnguyenxuan")
            {
                manager = "sn_dongnguyenxuan";
            }

            if (!string.IsNullOrEmpty(manager))
            {
                isManager = true;
            }
            else
                username = me;
            ViewBag.Manager = isManager;

            if (!isManager)
            {
                if (!string.IsNullOrEmpty(username))
                {
                    if (me != username)
                        return RedirectToAction("NotPermission");
                }
            }

            if (string.IsNullOrEmpty(username))
                username = me;

            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var find = context.Order.OrderByDescending(a => a.CreateDate).Take(10).ToList();
                if (find != null)
                {
                    var cm = GetClientModel();
                    ViewBag.Owner = cm.Owner;
                    ViewBag.ClientModel = cm;
                    ViewBag.PageName = "Quản lý đơn hàng";
                    ViewBag.Username = User.Identity.Name;                    
                    return View(find);
                }
                else
                    return RedirectToAction("NotFoundPrinter");

            }
        }
        [Route("hr-cloud/widget/sn/order/detail/{id}")]
        [Authorize(Roles = "C_ADM")]
        public ActionResult OrderDetail(string id)
        {
            string username = "";
            bool isManager = false;
            var me = User.Identity.Name;

            var manager = Services.Widgets.SonNguyenCacheService.ManagerHUsers.Where(a => a == me).FirstOrDefault();

            if (me == "sn_dongnguyenxuan")
            {
                manager = "sn_dongnguyenxuan";
            }

            if (!string.IsNullOrEmpty(manager))
            {
                isManager = true;
            }
            else
                username = me;
            ViewBag.Manager = isManager;

            if (!isManager)
            {
                if (!string.IsNullOrEmpty(username))
                {
                    if (me != username)
                        return RedirectToAction("NotPermission");
                }
            }

            if (string.IsNullOrEmpty(username))
                username = me;

            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var find = context.Order.Where(a => a.OrderId == gid).FirstOrDefault();
                if (find != null)
                {
                    var cm = GetClientModel();
                    ViewBag.Owner = cm.Owner;
                    ViewBag.ClientModel = cm;
                    ViewBag.PageName = "Chi tiết đơn hàng";
                    ViewBag.Username = User.Identity.Name;
                    ViewBag.Details = context.OrderDetail.Where(a => a.OrderId == find.OrderId).OrderBy(a=>a.Time).ToList();
                    return View(find);
                }
                else
                    return RedirectToAction("NotFoundPrinter");

            }
        }

        [Route("hr-cloud/events/create-event")]
        [Authorize]
        public ActionResult CreateEvent()
        {
            var me = User.Identity.Name;
            var username = me;            

            if (string.IsNullOrEmpty(username))
                username = me;

            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var orders = context.SN_Package.Where(a => a.AssignTo == username && !a.IsCancel && a.IsCompleted).OrderByDescending(a => a.ApproveDate).Take(50).ToList();
                ViewBag.Orders = orders;
                var cm = GetClientModel();
                ViewBag.Owner = cm.Owner;
                ViewBag.ClientModel = cm;
                ViewBag.PageName = "Đã hoàn thành";
                ViewBag.Username = username;
                ViewBag.Manager = true;
                return View();
            }
        }

        private ClientModel GetClientModel()
        {
            var username = User.Identity.Name;
            var index = username.IndexOf('_');
            var domain = username.Substring(0, index);
            var token = AccessTokenManager.GetAccessToken(username);
            ClientModel cm = new ClientModel();
            cm.Token = token;
            cm.Username = username;
            var owner = CacheCenter.DomainMapping[domain];
            cm.Domain = domain;
            cm.HostURL = owner.Server.URL;
            var user = CacheCenter.GetUserCached(username);
            cm.Owner = owner;
            cm.QUser = user;
            cm.Changes = CacheCenter.GetChangeDataModel(domain);
            cm.UChanges = CacheCenter.GetUserChanges(username);
            return cm;
        }


        private UDataModel GetUDataModel()
        {
            UDataModel ud = new UDataModel();
            var username = User.Identity.Name;
            var user = CacheCenter.GetUserCached(username);
            var owner = CacheCenter.DomainMapping[user.Domain];
            ud.User = user;
            ud.Owner = owner;
            return ud;
        }
    }
}