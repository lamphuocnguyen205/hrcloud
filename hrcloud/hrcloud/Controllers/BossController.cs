﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrcloud.Controllers
{
    [Authorize(Roles = "C_BOS")]
    public class BossController : Controller
    {
        // GET: Boss
        [Route("hr-cloud/boss/overview")]
        public ActionResult BossOverview()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Tổng quan quản trị";
            return View();
        }

        [Route("hr-cloud/boss/gia-cong")]
        public ActionResult BossHomeWorking()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Gia Công";
            using(var context = new EFWIDGET.HrWidgetEntities())
            {
                var money = context.SN_PUser.Where(a => a.IsActive).Sum(b => b.Account);
                ViewBag.Money = money;
                var count = context.SN_Package.Where(a => !a.IsCompleted && !a.IsCancel).Count();
                ViewBag.OCount = count;
            }
            return View();
        }

        [Route("hr-cloud/boss/home-working-salary")]
        public ActionResult SalaryHomeWorking()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Lương Gia Công";
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var users = context.SN_PUser.Where(a => a.IsActive).ToList();
                return View(users);
            }            
        }

        [Route("hr-cloud/boss/sn/user-summary")]
        [Authorize]
        public ActionResult BossUserSummary(string username)
        {
            ViewBag.Manager = true;

            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var user = context.SN_PUser.Where(a => a.UserId == username && a.IsActive).FirstOrDefault();
                if (user == null)
                    return RedirectToAction("NotFound");
                else
                {
                    var orders = context.SN_Package.Where(a => a.AssignTo == username && !a.IsCancel && !a.IsCompleted).OrderByDescending(a => a.CreateDate).ToList();
                    ViewBag.Orders = orders;
                    var cm = GetClientModel();
                    ViewBag.Owner = cm.Owner;
                    ViewBag.ClientModel = cm;
                    ViewBag.PageName = "Thông Tin Gia Công";
                    ViewBag.Username = username;
                    return View(user);
                }
            }
        }

        [Route("hr-cloud/boss/fund-admin")]
        public ActionResult FundAdministrator()
        {
            CultureInfo provider = CultureInfo.InvariantCulture;

            var now = DateTime.Now;
            var beginDay = now.AddDays(-3);
            var text = (beginDay.Day < 10 ? "0" + beginDay.Day.ToString() : beginDay.Day.ToString()) + "/" + (beginDay.Month < 10 ? "0" + beginDay.Month.ToString() : beginDay.Month.ToString()) + "/" + beginDay.Year.ToString();
            ViewBag.FromDate = text;

            ViewBag.ToDate = now.ToString("dd/MM/yyyy");

            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Quản lý tiền quỹ";
            return View();
        }

        private ClientModel GetClientModel()
        {
            var username = User.Identity.Name;
            var index = username.IndexOf('_');
            var domain = username.Substring(0, index);
            var token = AccessTokenManager.GetAccessToken(username);
            ClientModel cm = new ClientModel();
            cm.Token = token;
            cm.Username = username;
            var owner = CacheCenter.DomainMapping[domain];
            cm.Domain = domain;
            cm.HostURL = owner.Server.URL;
            var user = CacheCenter.GetUserCached(username);
            cm.Owner = owner;
            cm.QUser = user;
            cm.Changes = CacheCenter.GetChangeDataModel(domain);
            cm.UChanges = CacheCenter.GetUserChanges(username);
            cm.Timestamp = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
            return cm;
        }
    }
}