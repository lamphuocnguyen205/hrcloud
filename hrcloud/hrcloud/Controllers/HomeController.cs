﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrcloud.Controllers
{
    public class HomeController : Controller
    {
        [Route("")]
        public ActionResult Index()
        {
            //string Domain = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
            //HostingService.CreateHostingUser(null);
            //var link = "/hr-cloud/widget/sn/add-home-working";
            //var name = "Thêm Gói Gia Công";
            //List<Models.WMenu> menu = new List<Models.WMenu>();
            //Models.WMenu mn = new Models.WMenu();
            //mn.L = link;
            //mn.N = name;
            //menu.Add(mn);
            //using(var context = new EF.HrCenterEntities())
            //{
            //    var wid = PNUtility.Helpers.UtilityHelper.ParseGuid("012B8304-659F-4B07-9688-299EF1AA68DA");
            //    var widget = context.Widget.Where(a => a.WidgetId == wid).FirstOrDefault();
            //    if(widget != null)
            //    {
            //        widget.ActionData = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(menu);
            //        context.SaveChanges();
            //    }
            //}

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page";
            return View();
        }

        [Route("hr-cloud/widget/nt/customer/{id}")]
        public ActionResult LinkCustomer(string id)
        {
            using(var context = new EFWIDGET.HrWidgetEntities())
            {
                var cid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var customer = context.NTCustomer.Where(a => a.CustomerId == cid).FirstOrDefault();
                ViewBag.CName = customer.Name;
                ViewBag.CID = id;
                HttpCookie cookie = HttpContext.Request.Cookies[customer.CustomerId.ToString()];
                if (cookie != null)
                    ViewBag.CSEC = cookie.Value;
                else
                    ViewBag.CSEC = "";


                var now = DateTime.Now;
                var beginDay = now.AddDays(-60);
                var text = (beginDay.Day < 10 ? "0" + beginDay.Day.ToString() : beginDay.Day.ToString()) + "/" + (beginDay.Month < 10 ? "0" + beginDay.Month.ToString() : beginDay.Month.ToString()) + "/" + beginDay.Year.ToString();
                ViewBag.FromDate = text;

                ViewBag.ToDate = now.ToString("dd/MM/yyyy");
                return View();
            }
        }

        [Route("hr-cloud/widget/nt/customer-support/{id}")]
        public ActionResult LinkCustomerSupport(string id)
        {
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var cid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var customer = context.NTCustomer.Where(a => a.CustomerId == cid).FirstOrDefault();
                ViewBag.CName = customer.Name;
                ViewBag.CID = id;
                HttpCookie cookie = HttpContext.Request.Cookies[customer.CustomerId.ToString()];
                if (cookie != null)
                    ViewBag.CSEC = cookie.Value;
                else
                    ViewBag.CSEC = "";
                return View();
            }
        }

        [Route("hr-cloud/widget/nt/my-orders/{id}")]
        public ActionResult LinkCustomerOrder(string id)
        {
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var cid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var customer = context.NTCustomer.Where(a => a.CustomerId == cid).FirstOrDefault();
                ViewBag.CName = customer.Name;
                ViewBag.CID = id;
                HttpCookie cookie = HttpContext.Request.Cookies[customer.CustomerId.ToString()];
                if (cookie != null)
                    ViewBag.CSEC = cookie.Value;
                else
                    ViewBag.CSEC = "";

                CultureInfo provider = CultureInfo.InvariantCulture;

                var now = DateTime.Now;
                var beginDay = now.AddDays(-60);
                var text = (beginDay.Day < 10 ? "0" + beginDay.Day.ToString() : beginDay.Day.ToString()) + "/" + (beginDay.Month < 10 ? "0" + beginDay.Month.ToString() : beginDay.Month.ToString()) + "/" + beginDay.Year.ToString();
                ViewBag.FromDate = text;

                ViewBag.ToDate = now.ToString("dd/MM/yyyy");
                ViewBag.PageName = "ĐƠN HÀNG";
                return View();
            }
        }

        [Route("hr-cloud/widget/nt/my-import-histories")]
        public ActionResult NTCustomerImportHistories(string cusid, string oid, string fromday, string today, string keyword, string page, string count)
        {
            ViewBag.PageName = "XE HÀNG ĐI";
            var me = User.Identity.Name;
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var cid = PNUtility.Helpers.UtilityHelper.ParseGuid(cusid);
                var customer = context.NTCustomer.Where(a => a.CustomerId == cid).FirstOrDefault();
                ViewBag.CName = customer.Name;
                ViewBag.CID = cusid;
                HttpCookie cookie = HttpContext.Request.Cookies[customer.CustomerId.ToString()];
                if (cookie != null)
                    ViewBag.CSEC = cookie.Value;
                else
                    ViewBag.CSEC = "";

                int vcount = 0;
                int rcount = 0;
                int.TryParse(count, out vcount);
                vcount = 20;
                if (vcount == 0)
                    rcount = 20;
                else
                    rcount = vcount;

                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * vcount;
                ViewBag.Page = vpage;

                var set = from h in context.NTImport
                          select h;

                if (!string.IsNullOrEmpty(oid))
                {
                    var orderid = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                    set = from s in set where s.OrderId == orderid select s;
                    ViewBag.OID = oid;
                }

                if (!string.IsNullOrEmpty(cusid))
                {
 
                    var listOrder = context.NTOrder.Where(d => d.CustomerId == cid).Select(i => i.NTOrderID).ToArray();                   
                    set = from s in set where listOrder.Contains(s.OrderId) select s;
                }

                if (!string.IsNullOrEmpty(fromday) && !string.IsNullOrEmpty(today))
                {

                    DateTime dtf = DateTime.ParseExact(fromday, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.FromDate = fromday.Replace("-", "/");

                    DateTime dtt = DateTime.ParseExact(today, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.ToDate = today.Replace("-", "/");

                    dtt = dtt.AddHours(24);

                    set = from s in set where s.CreateDate >= dtf && s.CreateDate < dtt select s;
                }
                else
                {
                    var now = DateTime.Now;
                    var text = now.ToString("dd-MM-yyyy");
                    DateTime dtt = DateTime.ParseExact(text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.ToDate = text.Replace("-", "/");

                    var fromDate = now.AddYears(-1);
                    text = fromDate.ToString("dd-MM-yyyy");
                    var dtf = DateTime.ParseExact(text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.FromDate = text.Replace("-", "/");

                    dtt = dtt.AddHours(24);
                    set = from s in set where s.CreateDate >= dtf && s.CreateDate < dtt select s;

                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    var query = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(keyword);
                    set = from s in set where s.SearchData.Contains(query) select s;
                    ViewBag.Query = keyword;
                }
                if (vcount > 0)
                {
                    var result = set.OrderByDescending(a => a.CreateDate).Skip(skip).Take(rcount).ToList();
                    return View(result);
                }
                else
                {
                    var result = set.OrderByDescending(a => a.CreateDate).ToList();
                    return View(result);
                }
            }
        }

        [Route("hr-cloud/widget/nt/my-export-histories")]
        public ActionResult NTCustomerExportHistories(string cusid, string oid, string fromday, string today, string keyword, string page, string count)
        {
            ViewBag.PageName = "XE HÀNG VỀ";
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var cid = PNUtility.Helpers.UtilityHelper.ParseGuid(cusid);
                var customer = context.NTCustomer.Where(a => a.CustomerId == cid).FirstOrDefault();
                ViewBag.CName = customer.Name;
                ViewBag.CID = cusid;
                HttpCookie cookie = HttpContext.Request.Cookies[customer.CustomerId.ToString()];
                if (cookie != null)
                    ViewBag.CSEC = cookie.Value;
                else
                    ViewBag.CSEC = "";

                int vcount = 0;
                int rcount = 0;
                int.TryParse(count, out vcount);
                vcount = 20;
                if (vcount == 0)
                    rcount = 20;
                else
                    rcount = vcount;

                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * vcount;
                ViewBag.Page = vpage;

                var set = from h in context.NTTransport
                          select h;

                if (!string.IsNullOrEmpty(oid))
                {
                    var orderid = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                    set = from s in set where s.NTOrderId == orderid select s;
                    ViewBag.OID = oid;
                }

                if (!string.IsNullOrEmpty(cusid))
                {

                    var listOrder = context.NTOrder.Where(d => d.CustomerId == cid).Select(i => i.NTOrderID).ToArray();
                    set = from s in set where listOrder.Contains(s.NTOrderId) select s;
                }

                if (!string.IsNullOrEmpty(fromday) && !string.IsNullOrEmpty(today))
                {

                    DateTime dtf = DateTime.ParseExact(fromday, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.FromDate = fromday.Replace("-", "/");

                    DateTime dtt = DateTime.ParseExact(today, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.ToDate = today.Replace("-", "/");

                    dtt = dtt.AddHours(24);

                    set = from s in set where s.STime >= dtf && s.STime < dtt select s;
                }
                else
                {
                    var now = DateTime.Now;
                    var text = now.ToString("dd-MM-yyyy");
                    DateTime dtt = DateTime.ParseExact(text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.ToDate = text.Replace("-", "/");

                    var fromDate = now.AddYears(-1);
                    text = fromDate.ToString("dd-MM-yyyy");
                    var dtf = DateTime.ParseExact(text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.FromDate = text.Replace("-", "/");

                    dtt = dtt.AddHours(24);
                    set = from s in set where s.STime >= dtf && s.STime < dtt select s;

                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    var query = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(keyword);
                    set = from s in set where s.SearchData.Contains(query) select s;
                    ViewBag.Query = keyword;
                }
                if (vcount > 0)
                {
                    var result = set.OrderByDescending(a => a.STime).Skip(skip).Take(rcount).ToList();
                    return View(result);
                }
                else
                {
                    var result = set.OrderByDescending(a => a.STime).ToList();
                    return View(result);
                }
            }
        }

        [Route("hr-cloud/widget/nt/my-payment-histories")]
        public ActionResult NTCustomerPaymentHistories(string cusid, string page)
        {
            ViewBag.PageName = "LỊCH SỬ THANH TOÁN";
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var cid = PNUtility.Helpers.UtilityHelper.ParseGuid(cusid);
                var customer = context.NTCustomer.Where(a => a.CustomerId == cid).FirstOrDefault();
                ViewBag.CName = customer.Name;
                ViewBag.CID = cusid;
                HttpCookie cookie = HttpContext.Request.Cookies[customer.CustomerId.ToString()];
                if (cookie != null)
                    ViewBag.CSEC = cookie.Value;
                else
                    ViewBag.CSEC = "";

                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;
                ViewBag.Page = vpage;

                var payments = context.NTPayment.OrderByDescending(a => a.CreateDate).Where(a => a.CustomerId == cid).Skip(skip).Take(10).ToList();
                ViewBag.Pays = payments;
                return View(customer);
            }
        }

        [Route("hr-cloud/widget/nt/my-account-changes-histories")]
        public ActionResult NTCustomerChangesHistories(string cusid, string type, string page)
        {
            ViewBag.PageName = "LỊCH SỬ CÔNG NỢ";
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var cid = PNUtility.Helpers.UtilityHelper.ParseGuid(cusid);
                var customer = context.NTCustomer.Where(a => a.CustomerId == cid).FirstOrDefault();
                ViewBag.CName = customer.Name;
                ViewBag.CID = cusid;
                HttpCookie cookie = HttpContext.Request.Cookies[customer.CustomerId.ToString()];
                if (cookie != null)
                    ViewBag.CSEC = cookie.Value;
                else
                    ViewBag.CSEC = "";

                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;
                ViewBag.Page = vpage;

                var set = from h in context.NTHistory
                          select h;

                var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(cusid);
                set = from s in set where s.CustomerId == gid select s;
                if (!string.IsNullOrEmpty(type) && type != "-1")
                {
                    var vtype = PNUtility.Helpers.UtilityHelper.ParseInt(type);
                    if (vtype < 4)
                        set = from s in set where s.Type == vtype select s;
                    else
                    {
                        if (vtype == 4)
                            set = from s in set where s.Type == 4 && s.IsInc select s;
                        else
                            set = from s in set where s.Type == 4 && !s.IsInc select s;
                    }
                }

                var changes = set.OrderByDescending(a => a.CreateDate).Where(a => a.CustomerId == gid).Skip(skip).Take(10).ToList();
                ViewBag.Changes = changes;
                return View(customer);
            }
            
        }
    }
}