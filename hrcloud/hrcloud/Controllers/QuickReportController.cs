﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrcloud.Controllers
{
    public class QuickReportController : Controller
    {
        // GET: QuickReport
        [Route("hr-cloud/quick-report/fund-by-day/{domain}/{day}")]
        public ActionResult FundReportByDay(string domain, string day, string skey)
        {
            var owner = CacheCenter.DomainMapping[domain];
            if (owner != null && skey == "SonNguyenSkey2021")
            {               
                ViewBag.Owner = owner;
                ViewBag.Domain = domain;
                ViewBag.Day = day;
                ViewBag.Host = owner.Server.URL;
                return View();
            }
            else
                return RedirectToAction("NotFound");
        }

        [Route("hr-cloud/quick-report/order-status/{id}")]
        public ActionResult ViewOrderStatus(string id)
        {
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var find = context.Order.Where(a => a.OrderId == gid).FirstOrDefault();
                if (find != null)
                {
                    ViewBag.PageName = "Chi tiết đơn hàng";
                    ViewBag.Title = "Chi tiết đơn hàng - " + find.Name;
                    ViewBag.Details = context.OrderDetail.Where(a => a.OrderId == find.OrderId).OrderBy(a => a.Time).ToList();
                    return View(find);
                }
                else
                    return RedirectToAction("NotFoundPrinter");

            }
        }
    }
}