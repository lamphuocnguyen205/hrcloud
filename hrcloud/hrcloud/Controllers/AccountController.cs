﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using hrcloud.Models;
using Microsoft.AspNet.Identity.EntityFramework;


namespace hrcloud.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [Route("hr-cloud/logout")]
        [HttpPost]
        public ActionResult Logout()
        {
            var AuthenticationManager = HttpContext.GetOwinContext().Authentication;
            AuthenticationManager.SignOut();
            return RedirectToAction("HRCloudLogin");
        }

        [Route("owner/logout")]
        public ActionResult OwnerLogout()
        {
            var AuthenticationManager = HttpContext.GetOwinContext().Authentication;
            AuthenticationManager.SignOut();
            return RedirectToAction("OwnerLogin");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        [Route("account-test-function/create-role")]
        [AllowAnonymous]
        public async Task<ActionResult> CreateRoles()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            //var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            
            // In Startup iam creating first Admin Role and creating a default Admin User     
            if (!roleManager.RoleExists("C_ADM"))
            {
                // first we create Admin rool    
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "C_ADM";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("C_EMP"))
            {

                // first we create Admin rool    
                var role2 = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role2.Name = "C_EMP";
                roleManager.Create(role2);
            }
            if (!roleManager.RoleExists("C_BOS"))
            {

                // first we create Admin rool    
                var role2 = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role2.Name = "C_BOS";
                roleManager.Create(role2);
            }
            return View();
        }

        [Route("muser/create")]
        [AllowAnonymous]
        public async Task<ActionResult> MUserRegisterAsync()
        {
            //var user = new ApplicationUser { UserName = "mr.nguyenlam", Email = "vodanhonline88@gmail.com" };
            //var result = await UserManager.CreateAsync(user, "Chanhdao@88");
            //if (result.Succeeded)
            //{
            //    UserManager.AddToRole(user.Id, "M_ADM");
            //}
            //using(var context = new EF.HrCenterEntities())
            //{
            //    EF.MUser user = new EF.MUser();
            //    user.MUserId = "mr.nguyenlam";
            //    user.Name = "NGuyên Lâm";
            //    user.Password = "Chanhdao@88";
            //    user.Role = "M_ADM";
            //    user.CreateDate = DateTime.Now;
            //    context.MUser.Add(user);
            //    context.SaveChanges();
            //}

            return View();
        }

        

        //[Route("user/post-register")]
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> PostRegister(string phone, string password, string rpassword, string uref, string roomname)
        //{
        //    var hasAccount = PNUtility.Helpers.FileHelper.CheckFileExisted(Helper_App.ToSystemPath("/App_Data/Checking/" + phone + ".txt"));
        //    if (hasAccount)
        //        return RedirectToAction("Register", new { err = "Số điện thoại đã tồn tại" });

        //    if (password != rpassword)
        //        return RedirectToAction("Register", new { err = "Mật khẩu không khớp" });

        //    var user = new ApplicationUser { UserName = phone, Email = phone + "@aa.vn" };
        //    var result = await UserManager.CreateAsync(user, password);
        //    if (result.Succeeded)
        //    {
        //        ApplicationDbContext context = new ApplicationDbContext();

        //        var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
        //        var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
        //        var re = UserManager.AddToRole(user.Id, "USR");

        //        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //        Services.AccountService.CreateUser(phone, password, "USR", uref);
        //        try
        //        {
        //            PNUtility.Helpers.FileHelper.WriteText(Helper_App.ToSystemPath("/App_Data/Checking/" + phone + ".txt"), "");
        //        }
        //        catch (Exception) { }
        //        if (string.IsNullOrEmpty(roomname))
        //            return RedirectToAction("MemberSummary", "Member", new { task = "new" });
        //        else
        //            return RedirectToAction("MemberJoining", "Member", new { roomname = roomname });
        //    }
        //    else
        //        return RedirectToAction("Register", new { err = "Không thể tạo tài khoản vào lúc này, vui lòng thử lại!" });
        //}

        [Route("center-management/login")]
        public ActionResult CMLogin()
        {
            return View();
        }

        [Route("hr-cloud/login/{domain?}")]
        public ActionResult HRCloudLogin(string domain, string returnUrl)
        {
            ViewBag.URL = returnUrl;
            if (!string.IsNullOrEmpty(domain) && CacheCenter.DomainMapping.ContainsKey(domain))
            {                
                ViewBag.Owner = CacheCenter.DomainMapping[domain];
                return View();
            }
            else
                return View();
        }

        [Route("hr-cloud/force-login")]
        public async Task<ActionResult> HRCloudForceLoginAsync(string returnUrl)
        {
            Session["returnURL"] = returnUrl;
            if (Request.Cookies["sign_name"] != null && Request.Cookies["sign_pass"] != null)
            {
                var name = Request.Cookies["sign_name"].Value;
                var pass = Request.Cookies["sign_pass"].Value;
                var rname = Helper_App.PNDecode(name);
                var rpass = Helper_App.PNDecode(pass);
                if (rname == "nt_vanlam" || rname == "nt_mylinh")
                {
                    try
                    {
                        var AuthenticationManager = HttpContext.GetOwinContext().Authentication;
                        AuthenticationManager.SignOut();
                    }
                    catch { }
                    return Redirect("/");
                }
                else
                {
                    var result = await SignInManager.PasswordSignInAsync(rname, rpass, true, shouldLockout: false);
                    if (result == SignInStatus.Success)
                    {
                        Session["username"] = rname;
                        Session["cname"] = name;
                        Session["cpass"] = pass;
                        Session["returnURL"] = returnUrl;
                        return RedirectToAction("BeginRouting");
                    }
                    else
                    {
                        return RedirectToAction("HRCloudLogin", new { err = "Số điện thoại hoặc mật khẩu không đúng" });
                    }
                }
            }
            else
                return RedirectToAction("HRCloudLogin", "Account", new { returnUrl = returnUrl });
        }

        [Route("center-management/post-login")]
        public async Task<ActionResult> CMPostLoginAsync(string username, string password, string returnUrl)
        {

            var user = UserManager.FindByEmail("vodanhonline88@gmail.com");
            var token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            var result1 = UserManager.ResetPasswordAsync(user.Id, token, "Hoasen@123");
            var r = result1.Result;

            var result = await SignInManager.PasswordSignInAsync(username, password, true, shouldLockout: false);
            if (result == SignInStatus.Success)
            {
                if (!string.IsNullOrEmpty(returnUrl))
                    return RedirectToLocal(returnUrl);
                else
                {
                    return RedirectToAction("Overview", "CenterManagement");
                }
            }
            else
            {
                return RedirectToAction("CMLogin", new { err = "Số điện thoại hoặc mật khẩu không đúng" });
            }
        }

        [Route("hr-cloud/post-login")]
        public async Task<ActionResult> HrCloudPostLoginAsync(string username, string password, string returnUrl)
        {

            if (username == "nt_vanlam" || username == "nt_mylinh")
            {
                try
                {
                    var AuthenticationManager = HttpContext.GetOwinContext().Authentication;
                    AuthenticationManager.SignOut();
                }
                catch { }
                return Redirect("/");
            }
            else
            {
                var result = await SignInManager.PasswordSignInAsync(username, password, true, shouldLockout: false);

                if (result == SignInStatus.Success)
                {
                    Session["username"] = username;
                    Session["cname"] = Helper_App.PNEncode(username);
                    Session["cpass"] = Helper_App.PNEncode(password);
                    Session["returnURL"] = returnUrl;
                    return RedirectToAction("BeginRouting");
                }
                else
                {
                    return RedirectToAction("HRCloudLogin", new { err = "Số điện thoại hoặc mật khẩu không đúng" });
                }
            }
        }

        [Route("hr-cloud/begin-routing")]
        public ActionResult BeginRouting()
        {
            var username = (string)Session["username"];
            HttpCookie cname = new HttpCookie("sign_name");
            cname.Value = Session["cname"].ToString();
            cname.Expires = DateTime.Now.AddMonths(1);
            HttpCookie cpass = new HttpCookie("sign_pass");
            cpass.Value = Session["cpass"].ToString();
            cpass.Expires = DateTime.Now.AddMonths(1);
            Response.Cookies.Add(cpass);
            Response.Cookies.Add(cname);
            var returnUrl = Session["returnURL"].ToString();
            
            if (!string.IsNullOrEmpty(returnUrl))
                ViewBag.URL = returnUrl;
            else
            {
                var me = CacheCenter.GetUserCached(username);
                if (!string.IsNullOrEmpty(me.HomePath))
                    ViewBag.URL = me.HomePath;
                else if(me.Role == "C_BOS")
                {
                    ViewBag.URL = "/hr-cloud/boss/overview";
                }
                else
                    ViewBag.URL = "/hr-cloud/on-board/feeds";
            }
            return View();
        }

        [Authorize(Roles = "M_ADM")]
        [Route("center-management/new-user-for-owner")]
        public async Task<ActionResult> NewUserForOwnerAsync(string username, string name, string password, string role, string oid)
        {
            using(var context = new EF.HrCenterEntities())
            {
                var vid = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                var owner = context.Owner.Where(a => a.OwnerId == vid).FirstOrDefault();
                if(owner != null)
                {
                    var loginUsername = owner.Domain + '_' + username;
                    var user = new ApplicationUser { UserName = loginUsername, Email = loginUsername + "@aa.vn" };
                    var result = await UserManager.CreateAsync(user, password);
                    if (result.Succeeded)
                    {          
                        UserManager.AddToRole(user.Id, role);
                        EF.QUser qu = new EF.QUser();
                        qu.QUserId = loginUsername;
                        qu.Password = password;
                        qu.OwnerId = vid;
                        qu.Role = role;
                        qu.Name = name;
                        qu.Domain = owner.Domain;
                        qu.ServerId = owner.ServerId;
                        qu.CreateDate = DateTime.Now;
                        MyTasks.DBTask.AddTask((a, k) =>
                        {
                            var u = (EF.QUser)a[0];
                            using (var ctx = new EF.HrCenterEntities())
                            {
                                ctx.QUser.Add(u);
                                ctx.SaveChanges();
                                HostingService.CreateHostingUser(u);
                            }
                        }, owner.Domain, new object[] { qu });
                    }                    
                }
            }
            return Redirect("/center/management/detail-owner/" + oid);
        }

        [Route("owner/register")]
        public ActionResult CTRegister()
        {
            return View();
        }

        [Route("owner/post-register")]
        [HttpPost]
        public async Task<ActionResult> CTPostRegisterAsync(string phone, string pass)
        {
            var user = CacheCenter.GetUserCached(phone);
            if(user == null && phone.Length == 10 && pass.Length >=8)
            {
                var appUser = new ApplicationUser { UserName = phone, Email = phone + "@a.vn" };
                var result = await UserManager.CreateAsync(appUser, pass);
                if (result.Succeeded)
                {
                    ApplicationDbContext applicationContext = new ApplicationDbContext();
                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(applicationContext));
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(applicationContext));
                    var re = UserManager.AddToRole(appUser.Id, "C_ADM");
                    await SignInManager.SignInAsync(appUser, isPersistent: false, rememberBrowser: false);
                    MyTasks.DBTask.AddTask((a, k) => {
                        var loginUsername = (string)a[0];
                        using (var context = new EF.HrCenterEntities())
                        {
                            using (var transaction = context.Database.BeginTransaction())
                            {
                                try
                                {
                                    EF.Owner owner = new EF.Owner();
                                    owner.CreateDate = DateTime.Now;
                                    owner.Domain = "";
                                    owner.OwnerId = Guid.NewGuid();
                                    owner.ServerId = CacheCenter.CurrentServerId;
                                    owner.Status = 0;
                                    
                                    context.Owner.Add(owner);

                                    EF.QUser qu = new EF.QUser();
                                    qu.QUserId = loginUsername;
                                    qu.Password = (string)a[1];
                                    qu.OwnerId = owner.OwnerId;
                                    qu.Role = "C_ADM";
                                    qu.Name = "";
                                    qu.Domain = owner.Domain;
                                    qu.ServerId = owner.ServerId;
                                    qu.CreateDate = DateTime.Now;
                                    context.QUser.Add(qu);

                                    context.SaveChanges();
                                    transaction.Commit();

                                    System.IO.Directory.CreateDirectory(Helper_App.ToSystemPath("/UData/" + owner.OwnerId.ToString().ToLower()));

                                }
                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                }

                            }
                        }
                    }, "", new string[] { phone, pass });

                    var code = Guid.NewGuid().ToString().ToLower();
                    var path = Helper_App.ToSystemPath("/App_Data/Validations/" + code + ".txt");
                    System.IO.File.WriteAllText(path, phone);
                    return Redirect("/account/register-validation?code=" + code);
                }
                else
                {
                    return Redirect("/owner/register");
                }                
            }
            else
            {
                return Redirect("/owner/register");
            }
        }

        [Route("account/register-validation")]
        public ActionResult RegisterValidation(string code)
        {
            var path = Helper_App.ToSystemPath("/App_Data/Validations/" + code + ".txt");
            var username = "";
            try
            {
                username = System.IO.File.ReadAllText(path);
            }
            catch { }
            ViewBag.Code = string.IsNullOrEmpty(username) ? "" : code;
            return View();
        }

        [Route("hr-cloud/account/change-password")]
        [HttpPost]
        public async Task<ActionResult> ChangePassword(string oldPass, string newPass, string hw)
        {
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), oldPass, newPass);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    Session["change-password"] = "ok";
                    Session["password"] = Helper_App.PNEncode(newPass);
                    MyTasks.DBTask.AddTask((ar, k) => {
                    using(var context = new EF.HrCenterEntities())
                        {
                            var qu = context.QUser.Where(a => a.QUserId == k).FirstOrDefault();
                            if(qu != null)
                            {
                                qu.Password = (string)ar[0];
                                context.SaveChanges();
                            }
                        }
                    }, user.UserName, new string[] { newPass });
                }
                if (hw != "y")
                    return RedirectToAction("MyAccount", "OnBoard");
                else
                    return RedirectToAction("MyHomeWorkingAccount", "WidgetSonNguyen");
            }
            else
            {
                Session["change-password"] = "no";
                if (hw != "y")
                    return RedirectToAction("MyAccount", "OnBoard");
                else
                    return RedirectToAction("MyHomeWorkingAccount", "WidgetSonNguyen");
            }

        }


        [Route("owner/login")]
        public ActionResult OwnerLogin()
        {
            return View();
        }

        [Route("owner/post-login")]
        public async Task<ActionResult> OwnerPostLogin(string phone, string pass, string returnUrl)
        {
            var result = await SignInManager.PasswordSignInAsync(phone, pass, true, shouldLockout: false);
            if (result == SignInStatus.Success)
            {
                if (!string.IsNullOrEmpty(returnUrl))
                    return RedirectToLocal(returnUrl);
                else
                {
                    return RedirectToAction("DashBoard", "Owner");
                }
            }
            else
            {
                return RedirectToAction("OwnerLogin", new { err = "Số điện thoại hoặc mật khẩu không đúng" });
            }
        }
    }
}