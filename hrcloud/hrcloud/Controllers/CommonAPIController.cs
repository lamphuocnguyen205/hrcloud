﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace hrcloud.Controllers
{
    [Authorize]
    public class CommonAPIController : ApiController
    {
        [Route("api/member/reset-access-token")]
        [HttpPost]
        public string ResetAccessToken()
        {
            string id;
            id = User.Identity.Name;
            AccessTokenManager.ResetAccessToken(id);
            return "OK";
        }
    }
}
