﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;

namespace hrcloud.Controllers
{
    public class WidgetSonNguyenAPIController : ApiController
    {
        [Route("api/widget/son-nguyen/set-home-working-users")]
        [Authorize(Roles = "C_ADM")]
        public string SetHomeWorking(dynamic data)
        {
            var users = (string)data.users;
            var musers = (string)data.musers;
            string id;
            id = User.Identity.Name;
            MyTasks.DBTask.AddTask((args, key) => {
                var split = ((string)args[0]).Split(',');
                var msplit = ((string)args[1]).Split(',');
                var index = key.IndexOf('_');
                var domain = key.Substring(0, index);
                var owner = CacheCenter.DomainMapping[domain];
                var wid = PNUtility.Helpers.UtilityHelper.ParseGuid("012B8304-659F-4B07-9688-299EF1AA68DA");
                using (var context = new EF.HrCenterEntities())
                {
                    var widget = context.Widget.Where(a => a.OwnerId == owner.OwnerId && a.WidgetId == wid).FirstOrDefault();
                    if(widget != null)
                    {
                        Models.SonNguyen.HWUsers hwusers = new Models.SonNguyen.HWUsers();
                        if (!string.IsNullOrEmpty(widget.ConfigValue))
                        {
                            hwusers = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<Models.SonNguyen.HWUsers>(widget.ConfigValue);                           
                        }
                        // Outside
                        List<string> newOutside = new List<string>();
                        List<string> removed = new List<string>();
                        foreach (var item in hwusers.OutsideUsers)
                        {
                            if (!split.Contains(item))
                            {
                                var user = context.QUser.Where(a => a.QUserId == item).FirstOrDefault();
                                if (user != null)
                                {
                                    removed.Add(item);
                                    user.HomePath = "";
                                }
                            }
                        }
                        foreach (var item in split)
                        {
                            var sitem = item.ToLower();
                            if (!string.IsNullOrEmpty(item))
                            {                                                              
                                var find = hwusers.OutsideUsers.Where(u => u == sitem).FirstOrDefault();
                                if (string.IsNullOrEmpty(find))
                                {
                                    var user = context.QUser.Where(a => a.QUserId == sitem).FirstOrDefault();
                                    if (user != null)
                                    {                                        
                                        user.HomePath = "/hr-cloud/widget/sn/user-summary";
                                    }
                                }
                                newOutside.Add(sitem);
                            }
                           
                        }
                        hwusers.OutsideUsers = newOutside;

                        //manage user
                        List<string> newManage = new List<string>();
                        
                        foreach (var item in hwusers.ManageUsers)
                        {
                            if (!msplit.Contains(item))
                            {
                                var filename = "/WidgetData/Users/" + wid.ToString().ToLower() + "_" + item + ".txt";
                                var serverfile = HostingEnvironment.MapPath(filename);
                                try
                                {
                                    System.IO.File.Delete(serverfile);
                                }
                                catch { };
                            }
                        }
                        foreach (var item in msplit)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                var sitem = item.ToLower();                                
                                var find = hwusers.ManageUsers.Where(u => u == sitem).FirstOrDefault();
                                if (string.IsNullOrEmpty(find))
                                {
                                    var user = context.QUser.Where(a => a.QUserId == sitem).FirstOrDefault();
                                    if (user != null)
                                    {                                        
                                        var filename = "/WidgetData/Users/" + wid.ToString().ToLower() + "_" + sitem + ".txt";
                                        var serverfile = HostingEnvironment.MapPath(filename);
                                        Models.ExtraFunctionModel model = new Models.ExtraFunctionModel();
                                        model.TopMenu = new List<Models.WMenu>();
                                        model.TopMenu.Add(new Models.WMenu() { N = "Gia công", L = "/hr-cloud/widget/sn/gia-cong" });
                                        model.ActionMenu = new List<Models.WMenu>();
                                        model.ActionMenu.Add(new Models.WMenu() { N = "Thêm Gói Gia Công", L = "/hr-cloud/widget/sn/add-home-working" });
                                        System.IO.File.WriteAllText(serverfile, PNUtility.Helpers.JSONHelper.Jsoner.Serialize(model));
                                    }
                                }
                                newManage.Add(sitem);
                            }
                        }
                        hwusers.ManageUsers = newManage;
                        Services.Widgets.SonNguyenCacheService.ResetManagerUser();
                        widget.ConfigValue = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(hwusers);
                        context.SaveChanges();
                        bool isReset = false;
                        using(var ctx = new EFWIDGET.HrWidgetEntities())
                        {
                            foreach(var item in removed)
                            {
                                var find = ctx.SN_PUser.Where(a => a.UserId == item).FirstOrDefault();
                                if (find != null)
                                {
                                    find.IsActive = false;
                                    isReset = true;
                                }
                            }
                            foreach(var item in hwusers.OutsideUsers)
                            {
                                var find = ctx.SN_PUser.Where(a => a.UserId == item).FirstOrDefault();
                                if (find != null && !find.IsActive)
                                {
                                    find.IsActive = true;
                                    isReset = true;
                                }
                                else
                                {
                                    if (find == null)
                                    {
                                        EFWIDGET.SN_PUser u = new EFWIDGET.SN_PUser();
                                        u.UserId = item;
                                        u.Account = 0;
                                        u.IsActive = true;
                                        u.TotalAmount = 0;
                                        ctx.SN_PUser.Add(u);
                                        isReset = true;
                                    }
                                }
                            }
                            ctx.SaveChanges();
                            if (isReset)
                                Services.Widgets.SonNguyenCacheService.ResetHomeWorkingUser();
                        }
                    }
                }
            }, id, new string[] { users, musers });
            return "ok";
        }

        [Route("api/widget/son-nguyen/payment-home-working-user")]
        [Authorize(Roles = "C_ADM")]
        public string Payment(dynamic data)
        {
            var username = User.Identity.Name;
            MyTasks.DBTask.AddTask((args, key) => {
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        var mlist = Services.Widgets.SonNguyenCacheService.ManagerHUsers;
                        var rightUser = mlist.Where(a => a == key).FirstOrDefault();
                        if (!string.IsNullOrEmpty(rightUser))
                        {
                            var dyn = (dynamic)args[0];
                            var huser = (string)dyn.huser;
                            var value = (string)dyn.value;
                            value = value.Replace(".", "");
                            var targetUser = context.SN_PUser.Where(a => a.UserId == huser).FirstOrDefault();
                            var amount = PNUtility.Helpers.UtilityHelper.ParseLong(value);
                            if(targetUser != null)
                            {
                                decimal beginvalue = 0;
                                if(targetUser.Account >= amount)
                                {
                                    beginvalue = targetUser.Account;
                                    targetUser.Account = targetUser.Account - amount;
                                    targetUser.LastPayment = DateTime.Now;
                                    var tran = new EFWIDGET.SN_Transaction();
                                    tran.TransactionId = Guid.NewGuid();
                                    tran.BeginValue = beginvalue;
                                    tran.ChangeValue = amount;
                                    tran.CreateDate = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(targetUser.LastPayment.Value);
                                    tran.DoBy = key;
                                    tran.EndValue = targetUser.Account;
                                    tran.IsPayment = true;
                                    tran.Note = (string)dyn.note;
                                    tran.Username = targetUser.UserId;
                                    context.SN_Transaction.Add(tran);
                                    context.SaveChanges();
                                    transaction.Commit();
                                }
                               
                            }
                        }
                    }
                }
            }, username, new object[] { data });
            return "ok";
        }

        [Route("api/widget/son-nguyen/charge-home-working-user")]
        [Authorize(Roles = "C_ADM")]
        public string Charge(dynamic data)
        {
            var username = User.Identity.Name;
            MyTasks.DBTask.AddTask((args, key) => {
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        var mlist = Services.Widgets.SonNguyenCacheService.ManagerHUsers;
                        var rightUser = mlist.Where(a => a == key).FirstOrDefault();
                        if (!string.IsNullOrEmpty(rightUser))
                        {
                            var dyn = (dynamic)args[0];
                            var huser = (string)dyn.huser;
                            var value = (string)dyn.value;
                            value = value.Replace(".", "");
                            var targetUser = context.SN_PUser.Where(a => a.UserId == huser).FirstOrDefault();
                            var amount = PNUtility.Helpers.UtilityHelper.ParseLong(value);
                            if (targetUser != null)
                            {
                                decimal beginvalue = 0;
                                beginvalue = targetUser.Account;
                                targetUser.Account = targetUser.Account + amount;
                                targetUser.TotalAmount = targetUser.TotalAmount + amount;
                                var tran = new EFWIDGET.SN_Transaction();
                                tran.TransactionId = Guid.NewGuid();
                                tran.BeginValue = beginvalue;
                                tran.ChangeValue = amount;
                                tran.CreateDate = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                                tran.DoBy = key;
                                tran.EndValue = targetUser.Account;
                                tran.IsPayment = false;
                                tran.Note = (string)dyn.note;
                                tran.Username = targetUser.UserId;
                                context.SN_Transaction.Add(tran);
                                context.SaveChanges();
                                transaction.Commit();

                            }
                        }
                    }
                }
            }, username, new object[] { data });
            return "ok";
        }

        [Route("api/widget/son-nguyen/post-new-home-working")]
        [Authorize]
        public string PostNewHomeWorking(dynamic data)
        {
            var username = User.Identity.Name;
            MyTasks.DBTask.AddTask((args, key) => {
                using(var context = new EFWIDGET.HrWidgetEntities())
                {
                    var mlist = Services.Widgets.SonNguyenCacheService.ManagerHUsers;
                    var rightUser = mlist.Where(a => a == key).FirstOrDefault();
                    if (!string.IsNullOrEmpty(rightUser))
                    {
                        var dyn = (dynamic)args[0];
                        var assignTo = (string)dyn.huser;
                        var nail = (string)dyn.nail;
                        var grit = (string)dyn.grit;
                        var weight = (string)dyn.weight;
                        var order = (string)dyn.order;
                        var hw = new EFWIDGET.SN_Package();
                        hw.Amount = 0;
                        hw.PackageId = Guid.NewGuid();
                        hw.AssignBy = key;
                        hw.AssignTo = assignTo;
                        hw.CommentCount = 0;
                        hw.CreateDate = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                        hw.GritId = (byte)PNUtility.Helpers.UtilityHelper.ParseInt(grit);
                        hw.NailId = (byte)PNUtility.Helpers.UtilityHelper.ParseInt(nail);
                        hw.Number = 0;
                        hw.Code = getRandomCode();
                        hw.OrderId = order;
                        weight = weight.Replace('.', ',');
                        hw.KgOut = Helper.ParseFloat(weight);
                        hw.Price = Services.Widgets.SonNguyenCacheService.Nails.Where(a => a.NailId == hw.NailId).FirstOrDefault().Price;
                        if (hw.KgOut > 0)
                        {
                            context.SN_Package.Add(hw);
                            context.SaveChanges();
                        }
                    }
                }
            }, username, new object[] { data });
            return "ok";
        }

        [Route("api/widget/son-nguyen/post-edit-home-working")]
        [Authorize]
        public string PostEditHomeWorking(dynamic data)
        {
            var username = User.Identity.Name;
            MyTasks.DBTask.AddTask((args, key) => {
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var mlist = Services.Widgets.SonNguyenCacheService.ManagerHUsers;
                    var rightUser = mlist.Where(a => a == key).FirstOrDefault();
                    if (!string.IsNullOrEmpty(rightUser))
                    {
                        var dyn = (dynamic)args[0];
                        var id = (string)dyn.id;
                        var hid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                        var assignTo = (string)dyn.huser;
                        var nail = (string)dyn.nail;
                        var grit = (string)dyn.grit;
                        var weight = (string)dyn.weight;
                        var order = (string)dyn.order;
                        var hw = context.SN_Package.Where(a => a.PackageId == hid).FirstOrDefault();
                        if (hw != null && !hw.IsCompleted)
                        {
                            hw.OrderId = order;
                            hw.AssignTo = assignTo;
                            hw.GritId = (byte)PNUtility.Helpers.UtilityHelper.ParseInt(grit);
                            hw.NailId = (byte)PNUtility.Helpers.UtilityHelper.ParseInt(nail);
                            weight = weight.Replace('.', ',');
                            hw.KgOut = Helper.ParseFloat(weight);
                            hw.Price = Services.Widgets.SonNguyenCacheService.Nails.Where(a => a.NailId == hw.NailId).FirstOrDefault().Price;
                            if (hw.KgOut > 0)
                            {
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }, username, new object[] { data });
            return "ok";
        }

        [Route("api/widget/son-nguyen/post-cancel-home-working")]
        [Authorize]
        public string PostCancelHomeWorking(dynamic data)
        {
            var username = User.Identity.Name;
            MyTasks.DBTask.AddTask((args, key) => {
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var mlist = Services.Widgets.SonNguyenCacheService.ManagerHUsers;
                    var rightUser = mlist.Where(a => a == key).FirstOrDefault();
                    if (!string.IsNullOrEmpty(rightUser))
                    {
                        var dyn = (dynamic)args[0];
                        var id = (string)dyn.id;
                        var hid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                        
                        var hw = context.SN_Package.Where(a => a.PackageId == hid).FirstOrDefault();
                        if (hw != null && !hw.IsCompleted && hw.AssignBy == key)
                        {
                            hw.IsCancel = true;
                            context.SaveChanges();
                        }
                    }
                }
            }, username, new object[] { data });
            return "ok";
        }

        [Route("api/widget/son-nguyen/approve-home-working")]
        [Authorize]
        public string ApproveHomeWorking(dynamic data)
        {
            var username = User.Identity.Name;
            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                var id = (string)dyn.hid;
                var weight = (string)dyn.weight;
                weight = weight.Replace('.', ',');
                var number = (string)dyn.number;
                using(var context = new EFWIDGET.HrWidgetEntities())
                {
                    var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    var working = context.SN_Package.Where(a => a.PackageId == gid).FirstOrDefault();
                    if(working != null && !working.IsCompleted && !working.IsCancel)
                    {
                        working.IsCompleted = true;
                        working.Number = PNUtility.Helpers.UtilityHelper.ParseInt(number);
                        working.KgBack = Helper.ParseFloat(weight);
                        working.ApproveBy = key;
                        working.ApproveDate = DateTime.Now;
                        working.Amount = working.Price * working.Number;

                        var hwuser = context.SN_PUser.Where(a => a.UserId == working.AssignTo).FirstOrDefault();
                        if(hwuser!= null)
                        {
                            hwuser.TotalAmount += working.Amount;
                            hwuser.Account += working.Amount;
                        }
                        if (!string.IsNullOrEmpty(working.OrderId))
                        {
                            var orderID = PNUtility.Helpers.UtilityHelper.ParseGuid(working.OrderId);
                            var detail = context.OrderDetail.Where(a => a.NailId == working.NailId && a.GritId == working.GritId && a.OrderId == orderID).FirstOrDefault();
                            if(detail != null)
                            {
                                detail.Complete += working.Number;
                                var tracking = new EFWIDGET.NailTracking();
                                tracking.DoDate = DateTime.Now;
                                tracking.IsGC = true;
                                tracking.ODetailID = detail.ODetailID;
                                tracking.Val = working.Number;
                                var user = hrcloud.Services.Widgets.SonNguyenCacheService.Users.Where(a => a.UserId == working.AssignTo).FirstOrDefault();
                                if(user != null)
                                {
                                    var text = "GC #" + user.UserId + " #" + working.Number + "(+)";
                                    tracking.Description = text;
                                }
                                context.NailTracking.Add(tracking);
                            }
                        }
                        context.SaveChanges();
                    }
                }

            }, username, new object[] { data });
            return DateTime.Now.ToString("dd/MM/yyyy - HH:mm");
        }

        [Route("api/widget/son-nguyen/change-approve")]
        [Authorize]
        public string ChangeApprove(dynamic data)
        {
            var username = User.Identity.Name;
            MyTasks.DBTask.AddTask((args, key) => {
                var dyn = (dynamic)args[0];
                var id = (string)dyn.hid;
                var number = (string)dyn.number;
                var reason = (string)dyn.reason;
                var pname = (string)dyn.pname;
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    var working = context.SN_Package.Where(a => a.PackageId == gid).FirstOrDefault();
                    if (working != null && working.IsCompleted)
                    {
                        working.IsCompleted = true;
                        var oldNumber = working.Number;
                        working.Number = PNUtility.Helpers.UtilityHelper.ParseInt(number);
                        working.HasChanged = true;
                        working.Amount = working.Price * working.Number;
                        var oldAmmount = working.Price * oldNumber;
                        var hwuser = context.SN_PUser.Where(a => a.UserId == working.AssignTo).FirstOrDefault();
                        if (hwuser != null)
                        {
                            hwuser.TotalAmount -= oldAmmount;
                            hwuser.Account -= oldAmmount;
                            hwuser.TotalAmount += working.Amount;
                            hwuser.Account += working.Amount;
                        }
                        EFWIDGET.SN_Change change = new EFWIDGET.SN_Change();
                        change.ChangeId = Guid.NewGuid();
                        change.Amount = Math.Abs(oldAmmount - working.Amount);
                        change.DoBy = key;
                        change.GCUsername = working.AssignTo;
                        change.NewNumber = working.Number;
                        change.Note = reason;
                        change.OldNumber = oldNumber;
                        change.Time = DateTime.Now;
                        change.PackageId = working.PackageId;
                        change.PName = pname;
                        context.SN_Change.Add(change);
                        context.SaveChanges();
                    }
                }

            }, username, new object[] { data });
            return DateTime.Now.ToString("ok");
        }

        [Route("api/widget/son-nguyen/get-order-completed-50/{id}")]
        [Authorize(Roles = "C_ADM")]
        public HttpResponseMessage GetOrder50(string id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var nid = PNUtility.Helpers.UtilityHelper.ParseInt(id);
            using(var context = new EFWIDGET.HrWidgetEntities())
            {
                var list = context.SN_Package.Where(a => a.NailId == nid && a.IsCompleted).OrderByDescending(a => a.ApproveDate).Take(50).Select(p => new { kg = p.KgOut.Value, nu = p.Number }).ToList();
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list));
                return response;
            }
        }

        [Route("api/widget/son-nguyen/set-avg")]
        [Authorize(Roles = "C_ADM")]
        public string SetAVGValue(dynamic data)
        {
            MyTasks.DBTask.AddTask((args, key) => {
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var dyn = (dynamic)args[0];
                    var nid = (string)dyn.nid;
                    var nailID = PNUtility.Helpers.UtilityHelper.ParseInt(nid);
                    var nail = context.SN_Nail.Where(a => a.NailId == nailID).FirstOrDefault();
                    if(nail != null)
                    {
                        nail.AVG = PNUtility.Helpers.UtilityHelper.ParseInt((string)dyn.av);
                        nail.Perc = PNUtility.Helpers.UtilityHelper.ParseInt((string)dyn.delta);
                        context.SaveChanges();
                        hrcloud.Services.Widgets.SonNguyenCacheService.ResetNails();
                    }
                }
            }, "", new object[] { data });            

            return "ok";
        }

        [Route("api/widget/son-nguyen/create-order")]
        [Authorize(Roles = "C_ADM")]
        public string CreateOrder(dynamic data)
        {
            Guid id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((args, key) => {
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var dyn = (dynamic)args[0];
                    var day = (string)dyn.day;
                    var dayNum = PNUtility.Helpers.UtilityHelper.ParseInt(day);
                    EFWIDGET.Order o = new EFWIDGET.Order();
                    o.CreateDate = DateTime.Now;
                    o.ExportDate = o.CreateDate.AddDays(dayNum);
                    o.IsComplete = false;
                    o.Name = (string)dyn.name;
                    o.Note = (string)dyn.note;
                    o.OrderId = (Guid)args[1];
                    context.Order.Add(o);
                    context.SaveChanges();
                }
            }, "", new object[] { data, id });

            return id.ToString();
        }

        [Route("api/widget/son-nguyen/add-order-nail-file")]
        [Authorize(Roles = "C_ADM")]
        public string AddOrderNailFile(dynamic data)
        {
            Guid id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((args, key) => {
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var dyn = (dynamic)args[0];
                    var oid = (string)dyn.oid;
                    var nail = (string)dyn.nail;
                    var grit = (string)dyn.grit;
                    var count = (string)dyn.count;
                    var guidID = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                    var vnail = PNUtility.Helpers.UtilityHelper.ParseInt(nail);
                    var vgrit = PNUtility.Helpers.UtilityHelper.ParseInt(grit);
                    var vcount = PNUtility.Helpers.UtilityHelper.ParseInt(count);
                    EFWIDGET.OrderDetail dt = new EFWIDGET.OrderDetail();
                    dt.ODetailID = Guid.NewGuid();
                    dt.OrderId = guidID;
                    dt.Note = (string)dyn.note;
                    dt.Complete = 0;
                    dt.Count = vcount;
                    dt.GritId = (byte)vgrit;
                    dt.NailId = (byte)vnail;
                    dt.Time = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                    context.OrderDetail.Add(dt);
                    context.SaveChanges();
                }
            }, "", new object[] { data, id });

            return id.ToString();
        }

        [Route("api/widget/son-nguyen/add-product-to-detail")]
        [Authorize(Roles = "C_ADM")]
        public string AddProductToDetail(dynamic data)
        {
            Guid id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((args, key) => {
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var dyn = (dynamic)args[0];
                    var oid = (string)dyn.id;
                    var content = (string)dyn.content;
                    var count = (string)dyn.count;
                    var detailID = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                    var vcount = PNUtility.Helpers.UtilityHelper.ParseInt(count);
                    var dt = context.OrderDetail.Where(a => a.ODetailID == detailID).FirstOrDefault();
                    if (dt != null)
                    {
                        dt.Complete+= vcount;

                        EFWIDGET.NailTracking tracking = new EFWIDGET.NailTracking();
                        tracking.Description = "SET: " + content + " (+" + count + ")";
                        tracking.DoDate = DateTime.Now;
                        tracking.IsGC = false;
                        tracking.ODetailID = dt.ODetailID;
                        tracking.Val = vcount;
                        context.NailTracking.Add(tracking);
                        context.SaveChanges();
                    }

                }
            }, "", new object[] { data, id });

            return id.ToString();
        }

        [Route("api/widget/son-nguyen/less-product-to-detail")]
        [Authorize(Roles = "C_ADM")]
        public string LessProductToDetail(dynamic data)
        {
            Guid id = Guid.NewGuid();
            MyTasks.DBTask.AddTask((args, key) => {
                using (var context = new EFWIDGET.HrWidgetEntities())
                {
                    var dyn = (dynamic)args[0];
                    var oid = (string)dyn.id;
                    var content = (string)dyn.content;
                    var count = (string)dyn.count;
                    var detailID = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                    var vcount = PNUtility.Helpers.UtilityHelper.ParseInt(count);
                    var dt = context.OrderDetail.Where(a => a.ODetailID == detailID).FirstOrDefault();
                    if (dt != null)
                    {
                        dt.Complete -= vcount;

                        EFWIDGET.NailTracking tracking = new EFWIDGET.NailTracking();
                        tracking.Description = "SET: " + content + " (-" + count + ")";
                        tracking.DoDate = DateTime.Now;
                        tracking.IsGC = false;
                        tracking.ODetailID = dt.ODetailID;
                        tracking.Val = vcount * -1;
                        context.NailTracking.Add(tracking);
                        context.SaveChanges();
                    }

                }
            }, "", new object[] { data, id });

            return id.ToString();
        }

        private string getRandomCode()
        {
            var text = "ABCDEFGHIJKLMNOPQRSTUVXYZ0123456789";
            var r = text.OrderBy(a => Guid.NewGuid()).Take(6);
            return new string(r.ToArray());
        }

        //[Route("api/son-nguyen/get-event-photo")]
        //[Authorize()]
        //public string GetEventPhoto()
        //{
        //    using (var context = new EFWIDGET.HrWidgetEntities())
        //    {
        //        var list = context.EPhoto.Select(a => a.PhotoId).ToList();
        //        return PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
        //    }
        //}

        //[Route("api/son-nguyen/create-event-photo")]
        //[Authorize()]
        //public string CreateEventPhoto(dynamic data)
        //{
        //    string id = Guid.NewGuid().ToString();
        //    MyTasks.DBTask.AddTask((args, key) => {
        //        using (var context = new EFWIDGET.HrWidgetEntities())
        //        {
        //            try
        //            {
        //                var guid = PNUtility.Helpers.UtilityHelper.ParseGuid(key);
        //                var dyn = (dynamic)args[0];
        //                var photo = new EFWIDGET.EPhoto();
        //                photo.DateVal = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
        //                photo.Note = (string)dyn.note;
        //                photo.PKEY = guid;
        //                photo.Path = (string)dyn.path;
        //                var lat = (string)dyn.posLat;
        //                var lng = (string)dyn.posLong;
        //                if (lat != "0" && !string.IsNullOrEmpty(lat))
        //                {
        //                    var vlat = PNUtility.Helpers.UtilityHelper.ParseDouble(lat);
        //                    var vlong = PNUtility.Helpers.UtilityHelper.ParseDouble(lng);
        //                    photo.Location = CreatePoint(vlat, vlong);
        //                }
        //                var cap = (string)dyn.cap;
        //                bool isCap = cap == "true" || cap == "True" || cap == "TRUE";
        //                photo.IsTakePhoto = isCap;
        //                context.EPhoto.Add(photo);
        //                context.SaveChanges();
        //            }
        //            catch (Exception exx)
        //            {
        //                Services.LogService.Log("create-photo-db", exx.Message);
        //            }
        //        }
        //    }, id, new object[] { data });

        //    return id;
        //}

        public static DbGeography CreatePoint(double lat, double lon, int srid = 4326)
        {
            string wkt = String.Format("POINT({0} {1})", lon, lat);

            return DbGeography.PointFromText(wkt, srid);
        }
    }
}
