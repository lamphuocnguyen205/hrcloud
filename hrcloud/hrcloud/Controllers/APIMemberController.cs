﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using hrcloud.Models;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace hrcloud.Controllers
{
    public class APIMemberController : ApiController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public APIMemberController()
        {
        }

        public APIMemberController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [Route("api/admin/create-user")]
        [Authorize(Roles = "C_ADM")]
        [HttpPost]
        public async System.Threading.Tasks.Task<HttpResponseMessage> CreateUserAsync(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var username = (string)data.username;
            var domain = (string)data.domain;
            var owner = CacheCenter.DomainMapping[domain];
            var find = CacheCenter.GetUserCached(domain + "_" + username);
            if (find != null)
            {
                var obj = new { Id = "", Code = -1 };
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(obj));
            }
            else
            {  
                var firstName = (string)data.firstName;
                var lastName = (string)data.lastName;
                var password = (string)data.password;
                var loginUsername = domain + '_' + username;
                var user = new ApplicationUser { UserName = loginUsername, Email = loginUsername + "@aa.vn" };
                var result = await UserManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "C_EMP");
                    EF.QUser qu = new EF.QUser();
                    qu.QUserId = loginUsername;
                    qu.Password = password;
                    qu.OwnerId = owner.OwnerId;
                    qu.Role = "C_EMP";
                    qu.FirstName = firstName;
                    qu.LastName = lastName;
                    qu.Domain = owner.Domain;
                    qu.ServerId = owner.ServerId;
                    qu.CreateDate = DateTime.Now;
                    MyTasks.DBTask.AddTask((a, k) =>
                    {
                        var u = (EF.QUser)a[0];
                        using (var ctx = new EF.HrCenterEntities())
                        {
                            ctx.QUser.Add(u);
                            ctx.SaveChanges();
                            HostingService.CreateHostingUser(u);
                        }
                    }, owner.Domain, new object[] { qu });
                    var obj = new { Id = qu.QUserId, Code = 1 };
                    response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(obj));
                }
                else
                {
                    var obj = new { Id = "", Code = -2 };
                    response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(obj));
                }
            }
            return response;
        }

        [Route("api/owner/config-domain")]
        [HttpPost]
        [Authorize]
        public string ConfigDomain(dynamic data)
        {
            var domain = (string)data.domain;
            var name = (string)data.name;
            var code = (string)data.code;
            var owner = CacheCenter.DomainMapping.ContainsKey(domain) ? CacheCenter.DomainMapping[domain]: null;
            if (owner != null)
            {
                return "-1";
            }
            else
            {
                var check = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(domain);
                if (check.Length != domain.Length || string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(domain))
                    return "-2";
                else
                {
                    var u = User.Identity.Name;

                    MyTasks.DBTask.AddTask((a, k) =>
                    {
                        var vdomain = (dynamic)a[0];
                        var vname = (dynamic)a[1];
                        var vcode = (dynamic)a[2];
                        using (var context = new EF.HrCenterEntities())
                        {
                            var qu = context.QUser.Include(s => s.Owner).Where(q => q.QUserId == k).FirstOrDefault();
                            if(qu != null)
                            {
                                qu.Owner.Domain = vdomain;
                                qu.Owner.Name = vname;
                                context.SaveChanges();
                                CacheCenter.DomainMapping = null;
                                var path = Helper_App.ToSystemPath("/App_Data/Validations/" + vcode + ".txt");
                                try
                                {
                                    System.IO.File.Delete(path);
                                }
                                catch { }
                            }
                        }
                    }, u, new string[] { domain, name, code });
                    return "ok";
                }
            }
        }
    }
}
