﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace hrcloud.Controllers
{
    public class PublicApiController : ApiController
    {
        [Route("api/public/get-all-provinces")]
        [HttpGet]
        public HttpResponseMessage GetAllProvince()
        {
            HttpResponseMessage res = new HttpResponseMessage();
            res.Content = new StringContent("province list");
            return res;
        }

        [Route("api/main/post-photo")]
        [HttpPost]
        public string UploadImages()
        {
            try
            {
                if (HttpContext.Current.Request.Form["photo"] != null)
                {
                    Guid fid = Guid.NewGuid();
                    var newfile = Convert.FromBase64String(HttpContext.Current.Request.Form["photo"]);
                    string fileName = "/TempPhoto/" + fid.ToString() + ".png";
                    System.IO.File.WriteAllBytes(HostingEnvironment.MapPath(fileName), newfile);
                    return "/TempPhoto/" + fid.ToString() + ".png";
                }
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message + "-x";
            }

        }

        [Route("api/main/post-multi-photo")]
        [HttpPost]
        public string UploadMultiImages()
        {
            string res = "";
            for (int i = 0; i < 2; i++)
            {
                var text = HttpContext.Current.Request.Form["photo" + i.ToString()];
                if (!string.IsNullOrEmpty(text))
                {
                    Guid fid = Guid.NewGuid();
                    var newfile = Convert.FromBase64String(HttpContext.Current.Request.Form["photo" + i.ToString()]);
                    string fileName = "/TempPhoto/" + fid.ToString() + ".png";
                    System.IO.File.WriteAllBytes(HostingEnvironment.MapPath(fileName), newfile);
                    res = res + "/TempPhoto/" + fid.ToString() + ".png" + "*";
                }
            }
            return res;
        }

        [Route("api/main/post-event-photo")]
        [HttpPost]
        public string UploadEventImages()
        {
            try
            {
                if (HttpContext.Current.Request.Form["photo"] != null)
                {
                    Guid fid = Guid.NewGuid();
                    var newfile = Convert.FromBase64String(HttpContext.Current.Request.Form["photo"]);
                    string fileName = "/EventPhoto/" + fid.ToString() + ".jpg";
                    System.IO.File.WriteAllBytes(HostingEnvironment.MapPath(fileName), newfile);
                    return "/EventPhoto/" + fid.ToString() + ".jpg";
                }
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message + "-x";
            }

        }

        [Route("api/public/check-exist-username/{u}")]
        [HttpGet]
        public string CheckExistUsername(string u)
        {
            var user = CacheCenter.GetUserCached(u);
            return user == null ? "false" : "true";
        }

        [Route("api/widget/nguyen-trang/get-customer-data/{id}")]
        [HttpGet]
        [Authorize(Roles = "C_ADM")]
        public HttpResponseMessage GetCustomerData(string id, string key)
        {
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var customers = context.NTCustomer.Where(a => a.Status >= 0 && a.CustomerId == gid && a.Note == key).FirstOrDefault();
                var plastics = context.NTPlastic.ToList();
                var orders = context.NTOrder.Where(a => a.CustomerId == gid).ToList();
                var model = new { customers = customers, plastics = plastics, orders = orders };
                HttpResponseMessage respone = new HttpResponseMessage();
                respone.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(model));
                return respone;
            }
        }
    }
}
