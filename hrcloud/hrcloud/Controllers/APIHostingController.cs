﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace hrcloud.Controllers
{
    public class APIHostingController : ApiController
    {
        private const string skey = "Hoasen@123";
        private bool isHostingCalled()
        {
            var hd = Request.Headers.GetValues("API-Token");
            return hd.Count() > 0 && hd.ElementAt(0) == skey;
        }

        [Route("api/hosting/update-changes")]
        public string UpdateChanges(dynamic data)
        {
            if (isHostingCalled())
            {
                var jdata = (string)data.jdata;
                var cmodel = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<AChanges>(jdata);
                if(cmodel.Type == "AREA")
                {
                    var changeModel = CacheCenter.GetChangeDataModel(cmodel.Domain);
                    if (changeModel == null)
                        changeModel = new ChangeDataModel();

                    changeModel.AreaChanged = cmodel.Time;
                    changeModel.SaveChanges(cmodel.Domain);
                }
                else if (cmodel.Type == "SHIFT")
                {
                    var changeModel = CacheCenter.GetChangeDataModel(cmodel.Domain);
                    if (changeModel == null)
                        changeModel = new ChangeDataModel();

                    changeModel.ShiftChanged = cmodel.Time;
                    changeModel.SaveChanges(cmodel.Domain);
                }
                else if (cmodel.Type == "LABEL")
                {
                    var changeModel = CacheCenter.GetChangeDataModel(cmodel.Domain);
                    if (changeModel == null)
                        changeModel = new ChangeDataModel();

                    changeModel.LabelChanged = cmodel.Time;
                    changeModel.SaveChanges(cmodel.Domain);
                }
                else if (cmodel.Type == "POSITION")
                {
                    var changeModel = CacheCenter.GetChangeDataModel(cmodel.Domain);
                    if (changeModel == null)
                        changeModel = new ChangeDataModel();

                    changeModel.PositionChanged = cmodel.Time;
                    changeModel.SaveChanges(cmodel.Domain);
                }
                else if (cmodel.Type == "USERS")
                {
                    var changeModel = CacheCenter.GetChangeDataModel(cmodel.Domain);
                    if (changeModel == null)
                        changeModel = new ChangeDataModel();

                    changeModel.UserListChanged = cmodel.Time;
                    changeModel.SaveChanges(cmodel.Domain);
                }
                else if(cmodel.Type == "Owner")
                {
                    var changeModel = CacheCenter.GetChangeDataModel(cmodel.Domain);
                    if (changeModel == null)
                        changeModel = new ChangeDataModel();

                    changeModel.OwnerChanged = cmodel.Time;
                    changeModel.SaveChanges(cmodel.Domain);
                }
                else if (cmodel.Type == "Working")
                {
                    var changeModel = CacheCenter.GetChangeDataModel(cmodel.Domain);
                    if (changeModel == null)
                        changeModel = new ChangeDataModel();

                    changeModel.OwnerChanged = cmodel.Time;
                    changeModel.SaveChanges(cmodel.Domain);
                }

                if(cmodel.SubChanges != null)
                {
                    foreach(var item in cmodel.SubChanges)
                    {
                        if(item.Type == "Working")
                        {
                            var userchanges = CacheCenter.GetUserChanges(item.Id);
                            userchanges.WorkingChanged = cmodel.Time;
                            CacheCenter.SaveUserChanges(item.Id, userchanges);
                        }
                    }
                }
                return "1";
            }
            else
                return "-1";
        }
    }
}
