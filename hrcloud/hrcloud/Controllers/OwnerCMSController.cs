﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrcloud.Controllers
{
    [Authorize(Roles = "C_ADM")]
    public class OwnerCMSController : Controller
    {
        [Route("owner/cms/categories")]
        public ActionResult Categories()
        {
            ViewBag.Am = "folder";
            return View();
        }

        [Route("owner/cms/banners")]
        public ActionResult Banners()
        {
            ViewBag.Am = "banner";
            return View();
        }

        [Route("owner/cms/products")]
        public ActionResult Products()
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            ViewBag.Am = "product";
            ViewBag.OwnerId = user.OwnerId.ToString();
            return View();
        }

        [Route("owner/cms/add-product")]
        public ActionResult AddProduct()
        {
            ViewBag.Am = "product";
            return View();
        }

        [Route("owner/cms/edit-product/{id}")]
        public ActionResult EditProduct(string id)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            ViewBag.Am = "product";
            var oid = user.OwnerId.ToString();
            ViewBag.OwnerId = oid;
            using(var context = new EF.HrCenterEntities())
            {
                var pid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var findPost = context.OPost.Where(a=>a.OPostID == pid && a.OwnerId == user.OwnerId && a.IsProduct).FirstOrDefault();
                if (findPost != null)
                {
                    var path = "/UData/" + oid + "/files/" + findPost.OPostID.ToString() + ".txt";
                    var text = System.IO.File.ReadAllText(Helper_App.ToSystemPath(path));
                    ViewBag.Detail = text;
                    return View(findPost);
                }                   
                else
                    return RedirectToAction("NotFound");
            }
        }

        [Route("owner/cms/articles")]
        public ActionResult Articles()
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            ViewBag.Am = "article";
            ViewBag.OwnerId = user.OwnerId.ToString();
            return View();
        }

        [Route("owner/cms/add-article")]
        public ActionResult AddArticle()
        {
            ViewBag.Am = "article";
            return View();
        }

        [Route("owner/cms/edit-article/{id}")]
        public ActionResult EditArticle(string id)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            ViewBag.Am = "article";
            var oid = user.OwnerId.ToString();
            ViewBag.OwnerId = oid;
            using (var context = new EF.HrCenterEntities())
            {
                var pid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var findPost = context.OPost.Where(a => a.OPostID == pid && a.OwnerId == user.OwnerId && !a.IsProduct).FirstOrDefault();
                if (findPost != null)
                {
                    var path = "/UData/" + oid + "/files/" + findPost.OPostID.ToString() + ".txt";
                    var text = System.IO.File.ReadAllText(Helper_App.ToSystemPath(path));
                    ViewBag.Detail = text;
                    return View(findPost);
                }
                else
                    return RedirectToAction("NotFound");
            }
        }

        [Route("owner/cms/albums")]
        public ActionResult Albums()
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            ViewBag.Am = "album";
            var idText = user.OwnerId.ToString();
            ViewBag.OwnerId = idText;
            var path = "/UData/" + idText + "/albums";
            var sysPath = Helper_App.ToSystemPath(path);
            if (!System.IO.Directory.Exists(sysPath))
            {
                System.IO.Directory.CreateDirectory(sysPath);
                var coverPath = path + "/covers";
                System.IO.Directory.CreateDirectory(Helper_App.ToSystemPath(coverPath));

                var picturePath = path + "/pictures";
                System.IO.Directory.CreateDirectory(Helper_App.ToSystemPath(picturePath));

                var thumnailPath = path + "/thumnails";
                System.IO.Directory.CreateDirectory(Helper_App.ToSystemPath(thumnailPath));

                var txtPath = path + "/txt";
                System.IO.Directory.CreateDirectory(Helper_App.ToSystemPath(txtPath));
            }
            return View();
        }

        [Route("owner/cms/album-detail/{id}")]
        public ActionResult AlbumDetail(string id)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            ViewBag.Am = "album";
            var idText = user.OwnerId.ToString();
            ViewBag.OwnerId = idText;
            
            var aid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
            using(var context = new EF.HrCenterEntities())
            {
                var album = context.Album.Where(a => a.AlbumId == aid && a.OwnerId == user.OwnerId).FirstOrDefault();
                if (album != null)
                {
                    var path = "/UData/" + idText + "/albums/txt/" + aid.ToString() + ".txt";
                    var sysPath = Helper_App.ToSystemPath(path);
                    if (System.IO.File.Exists(sysPath))
                    {
                        var text = System.IO.File.ReadAllText(sysPath);
                        ViewBag.JData = text;
                    }
                    else
                        ViewBag.JData = "[]";
                    string thumnailPath = "/UData/" + user.OwnerId.ToString() + "/albums/thumnails/" + aid;
                    var sysFolderPath = Helper_App.ToSystemPath(thumnailPath);
                    if (!System.IO.Directory.Exists(sysFolderPath))
                    {
                        System.IO.Directory.CreateDirectory(sysFolderPath);
                    }
                    return View(album);
                }
                return RedirectToAction("NotFound");
            }
        }

        [Route("owner/cms/video")]
        public ActionResult Video()
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            ViewBag.Am = "video";
            ViewBag.OwnerId = user.OwnerId.ToString();
            return View();
        }

        [Route("owner/cms/files")]
        public ActionResult Files()
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            ViewBag.Am = "file";
            ViewBag.OwnerId = user.OwnerId.ToString();
            return View();
        }

        [Route("owner/cms/links")]
        public ActionResult Links()
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            ViewBag.Am = "link";
            ViewBag.OwnerId = user.OwnerId.ToString();
            return View();
        }

        [Route("owner/cms/tags")]
        public ActionResult Tags()
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            ViewBag.Am = "tag";
            ViewBag.OwnerId = user.OwnerId.ToString();
            return View();
        }
    }
}