﻿using hrcloud.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Hosting;
using System.Web.Http;

namespace hrcloud.Controllers
{
    public class WidgetNguyenTrangPublicAPIController : ApiController
    {

        [Route("api/widget/nguyen-trang/get-cp-data-by-customer/{id}")]
        [HttpGet]
        public HttpResponseMessage GetCustomerAndPlasticByCustomer(string id)
        {
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var vcus = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var customers = context.NTCustomer.Where(a => a.CustomerId == vcus).ToList();
                var plastics = context.NTPlastic.ToList();
                var orders = context.NTOrder.Where(a => a.CustomerId == vcus).ToList();
                var model = new { customers = customers, plastics = plastics, orders = orders };
                HttpResponseMessage respone = new HttpResponseMessage();
                respone.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(model));
                return respone;
            }
        }

        [Route("api/widget/nguyen-trang/get-order-filter-by-customer")]
        [HttpGet]
        public HttpResponseMessage GetOrderByFilter(string cusid, string fromday, string today, string type, string keyword)
        {
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var set = from h in context.NTOrder
                          select h;

                if (!string.IsNullOrEmpty(fromday) && !string.IsNullOrEmpty(today))
                {

                    DateTime dtf = DateTime.ParseExact(fromday, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    DateTime dtt = DateTime.ParseExact(today, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    dtt = dtt.AddHours(24);

                    set = from s in set where s.CreateDate >= dtf && s.CreateDate < dtt select s;
                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    var query = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(keyword);
                    set = from s in set where s.SearchData.Contains(query) select s;
                }

                if (!string.IsNullOrEmpty(type))
                {
                    var status = PNUtility.Helpers.UtilityHelper.ParseInt(type);
                    if (status >= 0)
                        set = from s in set where s.Status == status select s;
                }

                if (!string.IsNullOrEmpty(cusid))
                {
                    var vcus = PNUtility.Helpers.UtilityHelper.ParseGuid(cusid);
                    set = from s in set where s.CustomerId == vcus select s;

                }

                var model = set.OrderByDescending(a => a.CreateDate).ToList();
                HttpResponseMessage respone = new HttpResponseMessage();
                respone.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(model));
                return respone;
            }
        }
    }
}
