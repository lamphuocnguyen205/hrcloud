﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrcloud.Controllers
{
    public class WidgetNguyenTrangController : Controller
    {

        [Route("hr-cloud/widget/nt/delete-nt-data")]
        [Authorize]
        public ActionResult DeleteAllData()
        {
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                //var back = from c in context.NTBack select c;
                //context.NTBack.RemoveRange(back);

                //var his = from c in context.NTHistory select c;
                //context.NTHistory.RemoveRange(his);

                //var pay = from c in context.NTPayment select c;
                //context.NTPayment.RemoveRange(pay);

                //var tran = from c in context.NTTransport select c;
                //context.NTTransport.RemoveRange(tran);

                //var imp = from c in context.NTImport select c;
                //context.NTImport.RemoveRange(imp);

                //var ord = from c in context.NTOrder select c;
                //context.NTOrder.RemoveRange(ord);

                //var plas = from c in context.NTPlastic select c;
                //context.NTPlastic.RemoveRange(plas);

                //var cus = from c in context.NTCustomer select c;
                //context.NTCustomer.RemoveRange(cus);

                //context.SaveChanges();
                return View();
            }
        }

        // GET: WidgetSonNguyen
        [Route("hr-cloud/widget/nt/customers")]
        [Authorize]
        public ActionResult Customers()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Quản lý khách hàng";
            var me = User.Identity.Name;
            using(var context = new EFWIDGET.HrWidgetEntities())
            {
                var list = context.NTCustomer.Where(a => a.Status >= 0).ToList();
                //foreach(var item in list)
                //{
                //    item.Money = 0;
                //    item.CMoney = 0;
                //    item.OCount = 0;
                //    item.WCount = 0;
                //    item.WCountS = 0;
                //    item.CBack = 0;                    
                //}
                //var all = from c in context.NTImport select c;
                //context.NTImport.RemoveRange(all);

                //var all2 = from c in context.NTTransport select c;
                //context.NTTransport.RemoveRange(all2);

                //var all3 = from c in context.NTOrder select c;
                //context.NTOrder.RemoveRange(all3);

                //context.SaveChanges();
                //var products = context.NTPlastic.ToList();
                //foreach(var item in products)
                //{
                //    item.WCount = 0;                    
                //}
                //context.SaveChanges();
                return View(list);
            }
        }
        
        [Route("hr-cloud/widget/nt/add-customer")]
        [Authorize]
        public ActionResult AddCustomer()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Thêm khách hàng";
            var me = User.Identity.Name;
            return View();
        }

        [Route("hr-cloud/widget/nt/edit-customer/{id}")]
        [Authorize]
        public ActionResult EditCustomer(string id)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Cập nhật khách hàng";
            var me = User.Identity.Name;
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var customer = context.NTCustomer.Where(a => a.CustomerId == gid).FirstOrDefault();
                return View(customer);
            }
        }

        [Route("hr-cloud/widget/nt/customer-detail/{id}")]
        [Authorize]
        public ActionResult CustomerDetail(string id)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Thông tin khách hàng";
            var me = User.Identity.Name;
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var customer = context.NTCustomer.Where(a => a.CustomerId == gid).FirstOrDefault();
                return View(customer);
            }
        }

        [Route("hr-cloud/widget/nt/make-payment/{id}")]
        [Authorize]
        public ActionResult CustomerPayment(string id)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Tạo thanh toán khách hàng";
            var me = User.Identity.Name;
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var customer = context.NTCustomer.Where(a => a.CustomerId == gid).FirstOrDefault();
                return View(customer);
            }
        }

        [Route("hr-cloud/widget/nt/customer-histories/{id}")]
        [Authorize]
        public ActionResult CustomerHistories(string id, string page)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Lịch sử thanh toán";
            var me = User.Identity.Name;
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;
                ViewBag.Page = vpage;

                var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                var customer = context.NTCustomer.Where(a => a.CustomerId == gid).FirstOrDefault();
                var payments = context.NTPayment.OrderByDescending(a => a.CreateDate).Where(a => a.CustomerId == gid).Skip(skip).Take(10).ToList();
                ViewBag.Pays = payments;
                return View(customer);
            }
        }

        [Route("hr-cloud/widget/nt/customer-account-changes/{id}")]
        [Authorize]
        public ActionResult CustomerChangesHistories(string id, string type, string page)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Lịch sử thay đổi công nợ";
            var me = User.Identity.Name;
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * 10;
                ViewBag.Page = vpage;

                var set = from h in context.NTHistory
                          select h;

                var gid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                set = from s in set where s.CustomerId == gid select s;
                if(!string.IsNullOrEmpty(type) && type != "-1")
                {
                    var vtype = PNUtility.Helpers.UtilityHelper.ParseInt(type);
                    if(vtype < 4)
                        set = from s in set where s.Type == vtype select s;
                    else
                    {
                        if(vtype == 4)
                            set = from s in set where s.Type == 4 && s.IsInc select s;
                        else
                            set = from s in set where s.Type == 4 && !s.IsInc select s;
                    }
                }
                var customer = context.NTCustomer.Where(a => a.CustomerId == gid).FirstOrDefault();

                var changes = set.OrderByDescending(a => a.CreateDate).Where(a => a.CustomerId == gid).Skip(skip).Take(10).ToList();
                ViewBag.Changes = changes;
                return View(customer);
            }
        }

        [Route("hr-cloud/widget/nt/plastics")]
        [Authorize]
        public ActionResult Plastics()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Quản lý nguyên liệu";
            var me = User.Identity.Name;
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var list = context.NTPlastic.ToList();
                return View(list);
            }
        }

        [Route("hr-cloud/widget/nt/orders")]
        [Authorize]
        public ActionResult Orders(string status, string fromday, string today, string keyword, string page, string count)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Quản lý đơn hàng";
            var me = User.Identity.Name;

            CultureInfo provider = CultureInfo.InvariantCulture;

            var now = DateTime.Now;
            var beginDay = now.AddDays(-60);
            var text = (beginDay.Day < 10 ? "0" + beginDay.Day.ToString() : beginDay.Day.ToString()) + "/" + (beginDay.Month < 10 ? "0" + beginDay.Month.ToString() : beginDay.Month.ToString()) + "/" + beginDay.Year.ToString();
            ViewBag.FromDate = text;

            ViewBag.ToDate = now.ToString("dd/MM/yyyy");

            return View();
        }

        [Route("hr-cloud/widget/nt/add-plastic")]
        [Authorize]
        public ActionResult AddPlastic()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Thêm nguyên liệu";
            var me = User.Identity.Name;
            return View();
        }

        [Route("hr-cloud/widget/nt/edit-plastic/{id}")]
        [Authorize]
        public ActionResult EditPlastic(string id)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Cập nhật nguyên liệu";
            var me = User.Identity.Name;
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var gid = PNUtility.Helpers.UtilityHelper.ParseInt(id);
                var customer = context.NTPlastic.Where(a => a.PlasticId == gid).FirstOrDefault();
                return View(customer);
            }
        }

        [Route("hr-cloud/widget/nt/import-plastic")]
        [Authorize]
        public ActionResult ImportPlastic()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Nhập hàng";
            var me = User.Identity.Name;
            return View();
        }

        [Route("hr-cloud/widget/nt/export-plastic")]
        [Authorize]
        public ActionResult ExportPlastic(string oid)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Xuất hàng";
            var me = User.Identity.Name;
            ViewBag.OID = !string.IsNullOrEmpty(oid) ? oid.ToLower() : "";
            return View();
        }

        [Route("hr-cloud/widget/nt/goback-plastic")]
        [Authorize]
        public ActionResult GoBackPlastic(string oid)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Trả hàng";
            var me = User.Identity.Name;
            ViewBag.OID = oid;
            return View();
        }

        [Route("hr-cloud/widget/nt/directly-accounting")]
        [Authorize]
        public ActionResult DirectlyAccounting(string oid)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Công nợ trực tiếp";
            var me = User.Identity.Name;
            ViewBag.OID = oid;
            return View();
        }

        [Route("hr-cloud/widget/nt/import-histories")]
        [Authorize]
        public ActionResult ImportHistories(string oid, string fromday, string today, string keyword, string page, string count)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Lịch sử nhập hàng";
            var me = User.Identity.Name;
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                int vcount = 0;
                int rcount = 0;
                int.TryParse(count, out vcount);
                vcount = 20;
                if (vcount == 0)
                    rcount = 20;
                else
                    rcount = vcount;

                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * vcount;
                ViewBag.Page = vpage;

                var set = from h in context.NTImport
                          select h;

                if (!string.IsNullOrEmpty(oid))
                {
                    var orderid = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                    set = from s in set where s.OrderId == orderid select s;
                    ViewBag.OID = oid;
                }

                if (!string.IsNullOrEmpty(fromday) && !string.IsNullOrEmpty(today))
                {

                    DateTime dtf = DateTime.ParseExact(fromday, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.FromDate = fromday.Replace("-", "/");

                    DateTime dtt = DateTime.ParseExact(today, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.ToDate = today.Replace("-", "/");

                    dtt = dtt.AddHours(24);

                    set = from s in set where s.CreateDate >= dtf && s.CreateDate < dtt select s;
                }
                else
                {
                    var now = DateTime.Now;
                    var text = now.ToString("dd-MM-yyyy");
                    DateTime dtt = DateTime.ParseExact(text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.ToDate = text.Replace("-", "/");

                    var fromDate = now.AddYears(-1);
                    text = fromDate.ToString("dd-MM-yyyy");
                    var dtf = DateTime.ParseExact(text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.FromDate = text.Replace("-", "/");

                    dtt = dtt.AddHours(24);
                    set = from s in set where s.CreateDate >= dtf && s.CreateDate < dtt select s;

                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    var query = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(keyword);
                    set = from s in set where s.SearchData.Contains(query) select s;
                    ViewBag.Query = keyword;
                }
                if (vcount > 0)
                {
                    var result = set.OrderByDescending(a => a.DateIn).Skip(skip).Take(rcount).ToList();
                    return View(result);
                }
                else
                {
                    var result = set.OrderByDescending(a => a.DateIn).ToList();
                    return View(result);
                }
            }
        }

        [Route("hr-cloud/widget/nt/export-histories")]
        [Authorize]
        public ActionResult ExportHistories(string oid, string fromday, string today, string keyword, string page, string count)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Lịch sử xuất hàng";
            var me = User.Identity.Name;
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                int vcount = 0;
                int rcount = 0;
                int.TryParse(count, out vcount);
                vcount = 20;
                if (vcount == 0)
                    rcount = 20;
                else
                    rcount = vcount;

                var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                if (vpage == 0)
                    vpage = 1;
                int skip = (vpage - 1) * vcount;
                ViewBag.Page = vpage;

                var set = from h in context.NTTransport
                          select h;

                if (!string.IsNullOrEmpty(oid))
                {
                    var orderid = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                    set = from s in set where s.NTOrderId == orderid select s;
                    ViewBag.OID = oid;
                }

                if (!string.IsNullOrEmpty(fromday) && !string.IsNullOrEmpty(today))
                {

                    DateTime dtf = DateTime.ParseExact(fromday, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.FromDate = fromday.Replace("-", "/");

                    DateTime dtt = DateTime.ParseExact(today, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.ToDate = today.Replace("-", "/");

                    dtt = dtt.AddHours(24);

                    set = from s in set where s.STime >= dtf && s.STime < dtt select s;
                }
                else
                {
                    var now = DateTime.Now;
                    var text = now.ToString("dd-MM-yyyy");
                    DateTime dtt = DateTime.ParseExact(text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.ToDate = text.Replace("-", "/");

                    var fromDate = now.AddYears(-1);
                    text = fromDate.ToString("dd-MM-yyyy");
                    var dtf = DateTime.ParseExact(text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ViewBag.FromDate = text.Replace("-", "/");

                    dtt = dtt.AddHours(24);
                    set = from s in set where s.STime >= dtf && s.STime < dtt select s;

                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    var query = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(keyword);
                    set = from s in set where s.SearchData.Contains(query) select s;
                    ViewBag.Query = keyword;
                }
                if (vcount > 0)
                {
                    var result = set.OrderByDescending(a => a.STime).Skip(skip).Take(rcount).ToList();
                    return View(result);
                }
                else
                {
                    var result = set.OrderByDescending(a => a.STime).ToList();
                    return View(result);
                }
            }
        }

        [Route("hr-cloud/widget/nt/back-histories")]
        [Authorize]
        public ActionResult BackHistories(string oid)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Lịch sử trả hàng";
            using (var context = new EFWIDGET.HrWidgetEntities())
            {
                var orderid = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                var list = context.NTBack.Where(a => a.NTOrderID == orderid).ToList();
                return View(list);
            }
        }

        private ClientModel GetClientModel()
        {
            var username = User.Identity.Name;
            var index = username.IndexOf('_');
            var domain = username.Substring(0, index);
            var token = AccessTokenManager.GetAccessToken(username);
            ClientModel cm = new ClientModel();
            cm.Token = token;
            cm.Username = username;
            var owner = CacheCenter.DomainMapping[domain];
            cm.Domain = domain;
            cm.HostURL = owner.Server.URL;
            var user = CacheCenter.GetUserCached(username);
            cm.Owner = owner;
            cm.QUser = user;
            cm.Changes = CacheCenter.GetChangeDataModel(domain);
            cm.UChanges = CacheCenter.GetUserChanges(username);
            return cm;
        }


        private UDataModel GetUDataModel()
        {
            UDataModel ud = new UDataModel();
            var username = User.Identity.Name;
            var user = CacheCenter.GetUserCached(username);
            var owner = CacheCenter.DomainMapping[user.Domain];
            ud.User = user;
            ud.Owner = owner;
            return ud;
        }
    }
}