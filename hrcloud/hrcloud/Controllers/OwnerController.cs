﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrcloud.Controllers
{
    [Authorize(Roles = "C_ADM")]
    public class OwnerController : Controller
    {

        [Route("owner/dashboard")]
        public ActionResult DashBoard()
        {
            return View();
        }

        [Route("account/welcome-user")]
        public ActionResult WelcomeUser(string code)
        {
            var un = User.Identity.Name;
            var user = CacheCenter.GetUserCached(un);
            var path = Helper_App.ToSystemPath("/App_Data/Validations/" + code + ".txt");
            var username = "";
            try
            {
                username = System.IO.File.ReadAllText(path);
            }
            catch { }
            if (username == user.QUserId)
            {
                ViewBag.Code = code;
                return View(user);
            }
            else
                return RedirectToAction("NotValid");
        }

        // GET: Owner
        [Route("hr-cloud/admin/overview")]
        public ActionResult OwnerOverview()
        {
            var ud = GetUDataModel();
            ViewBag.Owner = ud.Owner;
            return View();
        }

        private UDataModel GetUDataModel()
        {
            UDataModel ud = new UDataModel();
            var username = User.Identity.Name;
            var user = CacheCenter.GetUserCached(username);
            var owner = CacheCenter.DomainMapping[user.Domain];
            ud.User = user;
            ud.Owner = owner;
            return ud;
        }

        [Route("hr-cloud/admin/employee-list")]
        public ActionResult OwnerEmployeeList()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Danh sách nhân viên";
            return View();
        }

        [Route("hr-cloud/admin/area-management")]
        public ActionResult OwnerAreaManagement()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Quản lý phòng ban";
            return View();
        }

        [Route("hr-cloud/admin/shift-management")]
        public ActionResult OwnerShiftManagement()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Quản lý ca làm việc";
            return View();
        }

        [Route("hr-cloud/admin/label-management")]
        public ActionResult OwnerLabelManagement()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Quản lý nhãn";
            return View();
        }

        [Route("hr-cloud/admin/position-management")]
        public ActionResult OwnerPositionManagement()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Quản lý vai trò";
            return View();
        }

        [Route("hr-cloud/admin/add-employee")]
        public ActionResult OwnerAddEmployee()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Thêm nhân viên mới";
            return View();
        }

        [Route("hr-cloud/admin/edit-user/{username}")]
        public ActionResult EditUser(string username)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            var user = CacheCenter.GetUserCached(username);
            if (user != null)
            {
                ViewBag.PageName = user.FirstName + " " + user.LastName;
                ViewBag.Target = username;
                return View();
            }
            else
            {
                return RedirectToAction("NotFound");
            }
        }

        [Route("hr-cloud/admin/payment-user/{username}")]
        public ActionResult PaymentUser(string username)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            var user = CacheCenter.GetUserCached(username);
            if (user != null)
            {
                ViewBag.PageName = user.FirstName + " " + user.LastName;
                ViewBag.Target = username;
                return View();
            }
            else
            {
                return RedirectToAction("NotFound");
            }
        }

        [Route("hr-cloud/admin/make-working-billing/{username}")]
        public ActionResult MakeWorkingBilling(string username)
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            var user = CacheCenter.GetUserCached(username);
            if (user != null)
            {
                ViewBag.PageName = user.FirstName + " " + user.LastName;
                ViewBag.Target = username;
                ViewBag.ToDate = DateTime.Now.ToString("dd/MM/yyyy");
                var now = DateTime.Now;
                var thu = (int)now.DayOfWeek;
                int delta = 0;
                if (thu == 0)
                    delta = 6;
                else if (thu == 1)
                    delta = 0;
                else
                    delta = thu - 1;

                var begin = now.AddDays(-1 * delta);

                ViewBag.FromDate = begin.ToString("dd/MM/yyyy");
                return View();
            }
            else
            {
                return RedirectToAction("NotFound");
            }
        }

        [Route("hr-cloud/admin/payment-detail/{pid}/{uref}")]
        public ActionResult PaymentDetail(string pid, string uref)
        {
            var cm = GetClientModel();
            var targetUser = CacheCenter.GetUserCached(uref);
            ViewBag.PageName = targetUser.FirstName + " " + targetUser.LastName;
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PID = pid;
            ViewBag.Target = uref;
            return View();
        }

        [Route("hr-cloud/admin/payment-printer/{pid}/{uref}")]
        public ActionResult PaymentPrinter(string pid, string uref)
        {
            var cm = GetClientModel();
            var targetUser = CacheCenter.GetUserCached(uref);
            ViewBag.PageName = targetUser.FirstName + " " + targetUser.LastName;
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PID = pid;
            ViewBag.Target = uref;
            return View();
        }

        private ClientModel GetClientModel()
        {
            var username = User.Identity.Name;
            var index = username.IndexOf('_');
            var domain = username.Substring(0, index);
            var token = AccessTokenManager.GetAccessToken(username);
            ClientModel cm = new ClientModel();
            cm.Token = token;
            cm.Username = username;
            var owner = CacheCenter.DomainMapping[domain];
            cm.Domain = domain;
            cm.HostURL = owner.Server.URL;
            var user = CacheCenter.GetUserCached(username);
            cm.Owner = owner;
            cm.QUser = user;
            cm.Changes = CacheCenter.GetChangeDataModel(domain);
            cm.UChanges = CacheCenter.GetUserChanges(username);
            return cm;
        }

        [Route("hr-cloud/on-board/make-fund-transaction")]
        public ActionResult MakeFuncTransaction()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Xuất quỹ";
            return View();
        }

        [Route("hr-cloud/on-board/import-to-fund")]
        public ActionResult ImportToFund()
        {
            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Nạp quỹ";
            return View();
        }

        [Route("hr-cloud/on-board/fund-admin")]
        public ActionResult FundAdministrator()
        {
            CultureInfo provider = CultureInfo.InvariantCulture;

            var now = DateTime.Now;
            var beginDay = now.AddDays(-3);
            var text = (beginDay.Day < 10 ? "0" + beginDay.Day.ToString() : beginDay.Day.ToString()) + "/" + (beginDay.Month < 10 ? "0" + beginDay.Month.ToString() : beginDay.Month.ToString()) + "/" + beginDay.Year.ToString();
            ViewBag.FromDate = text;

            ViewBag.ToDate = now.ToString("dd/MM/yyyy");

            var cm = GetClientModel();
            ViewBag.Owner = cm.Owner;
            ViewBag.ClientModel = cm;
            ViewBag.PageName = "Quản lý tiền quỹ";
            return View();
        }
    }
}