﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrcloud.Controllers
{
    [Authorize(Roles = "M_ADM")]
    public class CenterMockController : Controller
    {
        // GET: CenterMock
        [Route("center/mamagement/mock/roles")]
        public ActionResult Roles()
        {
            var roleStore = new RoleStore<IdentityRole>();
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
           // roleMngr.a
            var roles = roleMngr.Roles.ToList().Select(a=>a.Name).ToList();
            return View(roles);
        }
    }
}