﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(hrcloud.Startup))]
namespace hrcloud
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
