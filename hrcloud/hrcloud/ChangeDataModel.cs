﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace hrcloud
{
    public class ChangeDataModel
    {
        public long AreaChanged { get; set; }
        public long ShiftChanged { get; set; }
        public long LabelChanged { get; set; }
        public long PositionChanged { get; set; }
        public long UserListChanged { get; set; }

        public long OwnerChanged { get; set; }
        public Dictionary<string, long> UserChanges { get; set; }

        public ChangeDataModel()
        {
            UserChanges = new Dictionary<string, long>();
        }

        public void SaveChanges(string domain)
        {
            MyTasks.FileTask.AddTask((a, k) => {
                var changeModel = (ChangeDataModel)a[0];
                var text = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(changeModel);
                var filePath = HostingEnvironment.MapPath("/App_Data/Changes/" + k + ".txt");
                System.IO.File.WriteAllText(filePath, text);
            }, domain, new object[] { this });
        }
    }
}