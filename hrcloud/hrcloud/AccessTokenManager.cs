﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace hrcloud
{
    public class AccessTokenManager
    {
        private static Dictionary<string, TokenModel> tokens;

        public static Dictionary<string, TokenModel> Tokens
        {
            get
            {
                if (tokens == null)
                    tokens = new Dictionary<string, TokenModel>();

                return tokens;
            }
            set
            {
                tokens = value;
            }
        }

        private static string GenerateNewToken()
        {
            return PNUtility.Helpers.UtilityHelper.EncodeMD5(Guid.NewGuid().ToString());
        }

        private static void saveToken(string username, TokenModel tm)
        {
            var tokenPath = HostingEnvironment.MapPath("/App_Data/Tokens/" + username + ".txt");
            var text = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(tm);
            System.IO.File.WriteAllText(tokenPath, text);
            var hosting = CacheCenter.DomainMapping[tm.Domain].Server.URL;
            var url = hosting + "/api/main/update-access-token";
            var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(tm);
            NetworkService.callPostAPI(url, jdata, (result) => {
                int j = 0;
            }, () => { });


        }
        public static string GetAccessToken(string username)
        {
            if (Tokens.ContainsKey(username))
            {
                var tm = Tokens[username];
                return tm.Token;
                //var now = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                //if(now <= tm.Create + 3300)
                //{
                //    return tm.Token;
                //}
                //else
                //{
                //    tm.Token = GenerateNewToken();
                //    tm.Create = now;
                //    saveToken(username, tm);
                //    return tm.Token;
                //}
            }
            else
            {
                var tokenPath = HostingEnvironment.MapPath("/App_Data/Tokens/" + username + ".txt");
                if (System.IO.File.Exists(tokenPath))
                {
                    var text = System.IO.File.ReadAllText(tokenPath);
                    var tm = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<TokenModel>(text);
                    var now = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                    try
                    {
                        Tokens.Add(username, tm);
                    }
                    catch{ }
                    return tm.Token;
                    //if (now <= tm.Create + 3300)
                    //{
                    //    Tokens.Add(username, tm);
                    //    return tm.Token;
                    //}
                    //else
                    //{
                    //    tm.Token = GenerateNewToken();
                    //    tm.Create = now;
                    //    saveToken(username, tm);
                    //    Tokens.Add(username, tm);
                    //    return tm.Token;
                    //}
                }
                else
                {
                    using(var context = new EF.HrCenterEntities())
                    {
                        var qu = context.QUser.Where(a => a.QUserId == username).FirstOrDefault();
                        TokenModel tm = new TokenModel();
                        tm.Create = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                        tm.Domain = qu.Domain;
                        tm.OwnerId = qu.OwnerId;
                        tm.Username = username;
                        tm.Token = GenerateNewToken();

                        saveToken(username, tm);
                        try
                        {
                            Tokens.Add(username, tm);
                        }
                        catch { }
                        return tm.Token;
                    }
                    
                }
            }
        }

        public static void ResetAccessToken(string username)
        {
            if (Tokens.ContainsKey(username))
                Tokens.Remove(username);

            var tokenPath = HostingEnvironment.MapPath("/App_Data/Tokens/" + username + ".txt");
            if (System.IO.File.Exists(tokenPath))
            {
                System.IO.File.Delete(tokenPath);
            }
        }
    }
}