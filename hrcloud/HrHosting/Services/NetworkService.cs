﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;

namespace HrHosting
{
    public class NetworkService
    {
        public static void callPostAPI(string path, string data, Action<string> callback, Action error)
        {
            var url = path;
            new Thread(new ThreadStart(() => {
                try
                {
                    HttpClient httpClient = new HttpClient();
                    httpClient.DefaultRequestHeaders.Add("API-Token", "Hoasen@123");
                    var content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(new { jdata = data }), Encoding.UTF8, "application/json");
                    var res = httpClient.PostAsync(url, content).Result;
                    string text = res.Content.ReadAsStringAsync().Result;
                    callback.Invoke(text);
                }
                catch
                {
                    error.Invoke();
                }
            })).Start();
        }

        public static void callGetAPI(string path, Action<string> callback, Action error)
        {
            var url = path;
            new Thread(new ThreadStart(() => {
                try
                {
                    HttpClient httpClient = new HttpClient();
                    httpClient.DefaultRequestHeaders.Add("API-Token", "Hoasen@123");
                    var res = httpClient.GetAsync(url).Result;
                    string text = res.Content.ReadAsStringAsync().Result;
                    callback.Invoke(text);
                }
                catch
                {
                    error.Invoke();
                }
            })).Start();
        }
    }
}