﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class TimelineService
    {
        public static void AddAbsent(EF.HrCloudEntities context, TokenModel tokenModel, EF.Working working)
        {
            var post = context.Post.Where(a => a.Domain == working.Domain && a.CreateDate == working.DateVal && a.Type == 0).FirstOrDefault();
            if(post == null)
            {
                EF.Post p = new EF.Post();
                p.Code = "AS";
                p.CreateDate = working.DateVal;
                p.Domain = working.Domain;
                p.IsPublic = true;
                p.IsSystem = false;
                p.PostId = Guid.NewGuid();
                p.Type = 0;
                p.CreateTime = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
                AbsentItem absent = new AbsentItem()
                {
                    Username = working.Username,
                    ShiftId = working.ShiftRef.Value.ToString()
                };
                List<AbsentItem> list = new List<AbsentItem>();
                list.Add(absent);
                p.JData = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
                context.Post.Add(p);
            }
            else
            {
                var list = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<AbsentItem>>(post.JData);
                AbsentItem absent = new AbsentItem()
                {
                    Username = working.Username,
                    ShiftId = working.ShiftRef.Value.ToString()
                };
                list.Add(absent);
                post.JData = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list);
            }
            context.SaveChanges();
        }
    }
}