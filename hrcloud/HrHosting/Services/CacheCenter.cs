﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace HrHosting
{
    public static class CacheCenter
    {
        private static Dictionary<string, List<EF.Employee>> cacheEmployee;

        public static List<EF.Employee> GetFullEmployeeList(string domain)
        {
            if(cacheEmployee == null)
                cacheEmployee = new Dictionary<string, List<EF.Employee>>();

            if (cacheEmployee.ContainsKey(domain))
                return cacheEmployee[domain];
            else
            {
                using(var context = new EF.HrCloudEntities())
                {
                    var list = context.Employee.Include(b=>b.EmployeeDetail).Where(a => a.Domain == domain && a.Status < 5).ToList();
                    try
                    {
                        if (!string.IsNullOrEmpty(domain))
                            cacheEmployee.Add(domain, list);
                    }
                    catch { }
                    return list;
                }
            }
        }

        public static void ResetEmployee(string domain)
        {
            if (cacheEmployee == null)
                cacheEmployee = new Dictionary<string, List<EF.Employee>>();

            if (cacheEmployee.ContainsKey(domain))
                cacheEmployee.Remove(domain);
        }

        private static Dictionary<string, TokenModel> tokens;

        public static Dictionary<string, TokenModel> Tokens
        {
            get
            {
                if (tokens == null)
                    tokens = new Dictionary<string, TokenModel>();
                if (tokens.Count == 0)
                {
                    var now = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now) - 3600;
                    using (var context = new EF.HrCloudEntities())
                    {
                        tokens = context.Token.Select(t => new TokenModel
                        {
                            Token = t.TokenId,
                            Create = t.CreateDate,
                            Domain = t.Domain,
                            OwnerId = t.OwnerId,
                            Username = t.Username
                        }).ToDictionary((k) => { return k.Token; });

                        return tokens;
                    }
                }
                else
                    return tokens;
            }
            set
            {
                tokens = value;
            }
        }

        public static TokenModel GetTokenModel(string token)
        {
            if (Tokens.ContainsKey(token))
                return Tokens[token];
            else
                return null;
        }

        private static Dictionary<string, List<EF.Area>> mapArea;

        public static List<EF.Area> GetAllActiveArea(string domain)
        {
            if (mapArea == null)
                mapArea = new Dictionary<string, List<EF.Area>>();
            if (mapArea.ContainsKey(domain))
                return mapArea[domain];
            else
            {
                using(var context = new EF.HrCloudEntities())
                {
                    var list = context.Area.Include(b=>b.Shift).Where(a => !a.Hide && a.Domain == domain).ToList();
                    mapArea.Add(domain, list);
                    return list;
                }
            }
        }

        public static void ResetArea(string domain)
        {
            if (mapArea.ContainsKey(domain))
                mapArea.Remove(domain);
        }

        private static Dictionary<string, List<EF.Label>> mapLabel;

        public static List<EF.Label> GetAllLabels(string domain)
        {
            if (mapLabel == null)
                mapLabel = new Dictionary<string, List<EF.Label>>();
            if (mapLabel.ContainsKey(domain))
                return mapLabel[domain];
            else
            {
                using (var context = new EF.HrCloudEntities())
                {
                    var list = context.Label.Where(a => a.Domain == domain).ToList();
                    mapLabel.Add(domain, list);
                    return list;
                }
            }
        }

        public static void ResetLabel(string domain)
        {
            if (mapLabel != null && mapLabel.ContainsKey(domain))
                mapLabel.Remove(domain);
        }

        public static void AddNewArea(EF.Area area)
        {
            if (mapArea == null)
                mapArea = new Dictionary<string, List<EF.Area>>();
            if (mapArea.ContainsKey(area.Domain))
            {
                mapArea[area.Domain].Add(area);
            }
        }

        private static Dictionary<string, List<EF.Postion>> mapPosition;

        public static List<EF.Postion> GetAllPositons(string domain)
        {
            if (mapPosition == null)
                mapPosition = new Dictionary<string, List<EF.Postion>>();
            if (mapPosition.ContainsKey(domain))
                return mapPosition[domain];
            else
            {
                using (var context = new EF.HrCloudEntities())
                {
                    var list = context.Postion.Where(a => a.Domain == domain).ToList();
                    mapPosition.Add(domain, list);
                    return list;
                }
            }
        }

        public static void ResetPosition(string domain)
        {
            if (mapPosition!= null && mapPosition.ContainsKey(domain))
                mapPosition.Remove(domain);
        }

        private static Dictionary<string, List<WorkingModel>> _mapWorking;

        public static List<WorkingModel> GetWorking(string key)
        {
            if (_mapWorking == null)
                _mapWorking = new Dictionary<string, List<WorkingModel>>();

            if (_mapWorking.ContainsKey(key))
                return _mapWorking[key];
            else
            {
                using(var context = new EF.HrCloudEntities())
                {
                    var split = key.Split('*');
                    var aid = PNUtility.Helpers.UtilityHelper.ParseGuid(split[0]);
                    var dv = PNUtility.Helpers.UtilityHelper.ParseLong(split[1]);
                    var list = context.Working.Where(a => a.AreaId == aid && a.DateVal == dv && a.Status == 0).Select(w => new WorkingModel() {
                        Id = w.WorkingId,
                        Accept = w.IsAccept,
                        Begin = w.WBegin,
                        End = w.WEnd,
                        IsOff = w.WorkOrOff,
                        Note = w.Notes,
                        Reason = w.Reason,
                        ShiftId = w.ShiftRef.HasValue ? w.ShiftRef.Value.ToString() : "",
                        Status = w.Status,
                        Username = w.Username,
                        AreaId = w.AreaId,
                        Rate = w.Rate.HasValue ? w.Rate.Value : 0
                    }).ToList();
                    _mapWorking.Add(key, list);
                    return list;
                }
            }
        }

        public static void ResetWorking(string area, string time)
        {
            string key = area + "*" + time;
            if(_mapWorking != null && _mapWorking.ContainsKey(key))
            {
                _mapWorking.Remove(key);
            }
        }

        private static Dictionary<string, OwnerInfo> _mapOwnerInfo;

        public static OwnerInfo GetOwnerInfo(string domain)
        {
            if (_mapOwnerInfo == null)
                _mapOwnerInfo = new Dictionary<string, OwnerInfo>();
            if (_mapOwnerInfo.ContainsKey(domain))
                return _mapOwnerInfo[domain];
            else
            {
                using(var context = new EF.HrCloudEntities())
                {
                    var rates = context.Rate.Where(a => a.Domain == domain).ToList();
                    OwnerInfo oi = new OwnerInfo();
                    oi.Rates = new List<WRate>();
                    foreach(var item in rates)
                    {
                        oi.Rates.Add(new WRate { Id = item.RateId, Name = item.Name, RateVal = item.RateVal });
                    }
                    _mapOwnerInfo.Add(domain, oi);
                    return oi;
                }
            }
        }

        private static Dictionary<string, List<PlanModel>> _planMap;

        public static List<PlanModel> GetPlanByMonth(string domain, string month)
        {
            string key = domain + "_" + month;
            if (_planMap == null)
                _planMap = new Dictionary<string, List<PlanModel>>();
            if (_planMap.ContainsKey(key))
            {
                return _planMap[key];
            }
            else
            {
                int vmonth = PNUtility.Helpers.UtilityHelper.ParseInt(month);
                using(var context = new EF.HrCloudEntities())
                {
                    var list = context.Plan.Where(a => a.Domain == domain && a.MonVal == vmonth).OrderByDescending(a=>a.CreateDate).ToList();
                    List<PlanModel> plans = new List<PlanModel>();
                    foreach(var item in list)
                    {
                        PlanModel pm = new PlanModel()
                        {
                            Id = item.PlanId,
                            FeedId = item.FeedId.HasValue ? item.FeedId.Value.ToString() : "",
                            CreateDate = item.CreateDate.ToString("dd/MM/yyyy HH:mm"),
                            Name = item.Name,
                            Total = item.Total
                        };
                        plans.Add(pm);
                        
                    }
                    _planMap.Add(key, plans);
                    return plans;
                }
            }
        }

        public static void ResetPlans(string domain, string month)
        {
            string key = domain + "_" + month;
            if(_planMap != null && _planMap.ContainsKey(key))
            {
                _planMap.Remove(key);
            }
        }

        private static Dictionary<string, List<AdvanceModel>> _advanceMap;
        public static List<AdvanceModel> GetAdvanceByMonth(string domain, string month)
        {
            string key = domain + "_" + month;
            if (_advanceMap == null)
                _advanceMap = new Dictionary<string, List<AdvanceModel>>();
            if (_advanceMap.ContainsKey(key))
            {
                return _advanceMap[key];
            }
            else
            {
                int vmonth = PNUtility.Helpers.UtilityHelper.ParseInt(month);
                using (var context = new EF.HrCloudEntities())
                {
                    var list = context.Advance.Include(p=>p.Plan).Where(a => a.Domain == domain && a.MonthVal == vmonth).OrderBy(a=>a.CreateDate).ToList();
                    List<AdvanceModel> advances = new List<AdvanceModel>();
                    foreach (var item in list)
                    {
                        AdvanceModel adv = new AdvanceModel()
                        {
                            Id = item.AdvanceId,
                            CreateDate = item.CreateDate.ToString("dd/MM/yyyy HH:mm"),
                            IsActive = item.IsActive,
                            Money = item.Money,
                            Note = item.Note,
                            PlanId = item.PlanId.HasValue ? item.PlanId.Value.ToString() : "",
                            Username = item.Username,
                            PlanName = item.PlanId.HasValue ? item.Plan.Name : ""
                        };
                        advances.Add(adv);

                    }
                    _advanceMap.Add(key, advances);
                    return advances;
                }
            }
        }
        public static void ResetAdvances(string domain, string month)
        {
            string key = domain + "_" + month;
            if (_advanceMap != null && _advanceMap.ContainsKey(key))
            {
                _advanceMap.Remove(key);
            }
        }

        public static Dictionary<Guid, EF.Fund> mapFunds;

        public static EF.Fund GetFundIdByOwner(Guid oid)
        {
            if(mapFunds == null)
            {
                mapFunds = new Dictionary<Guid, EF.Fund>();                
            }
            if (mapFunds.ContainsKey(oid))
                return mapFunds[oid];
            else
            {
                using (var context = new EF.HrCloudEntities())
                {
                    var fund = context.Fund.Where(a => a.OwnerId == oid).FirstOrDefault();
                    if (fund == null)
                    {
                        fund = new EF.Fund();
                        fund.FundID = Guid.NewGuid();
                        fund.OwnerId = oid;
                        fund.InTotal = 0;
                        fund.OutTotal = 0;
                        fund.Account = 0;
                        MyTasks.DBTask.AddTask((args, key) => {
                            var f = (EF.Fund)args[0];
                            using (var ctx = new EF.HrCloudEntities())
                            {
                                ctx.Fund.Add(f);
                                ctx.SaveChanges();
                            }
                        }, "", new object[] { fund });
                    }
                    mapFunds.Add(oid, fund);
                    return fund;
                }
            }
        }

        public static void ResetFundByOwner(Guid oid)
        {
            if (mapFunds != null && mapFunds.ContainsKey(oid))
                mapFunds.Remove(oid);
        }
    }
}