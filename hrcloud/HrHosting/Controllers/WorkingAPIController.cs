﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HrHosting.Controllers
{
    [EnableCors(origins: "http://localhost:50940,https://hrcenter.vn", headers: "*", methods: "*")]
    public class WorkingAPIController : BaseAPIController
    {

        [Route("api/main/working/set-working-by-shift")]
        [HttpPost]
        public string SetWorkingByShift(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else {
                Guid id = Guid.NewGuid();
                MyTasks.DBTask.AddTask((a, k) => {
                    var wid = (Guid)a[2];
                    var tm = (TokenModel)a[0];
                    var dyn = (dynamic)a[1];
                    using(var context = new EF.HrCloudEntities())
                    {
                        var sid = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.sid);
                        var shift = context.Shift.Where(s => s.ShiftId == sid).FirstOrDefault();

                        var time = Helper.ParseTimeFromDash((string)dyn.day);
                        var timeVal = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(time);
                        
                        var aid = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.aid);
                        var username = (string)dyn.username;
                        var findWorkings = context.Working.Where(w => w.DateVal == timeVal && w.ShiftRef == sid && w.Username == username).ToList();
                        bool willCreate = true;
                        if(findWorkings!=null && findWorkings.Count > 0)
                        {
                            var count = findWorkings.Where(w => w.Status == 0).Count();
                            willCreate = count == 0;
                        }
                        if (willCreate)
                        {
                            EF.Working working = new EF.Working();
                            working.WorkingId = wid;
                            working.AreaId = aid;
                            working.CreateBy = (string)dyn.admin;
                            working.CreateDate = DateTime.Now;
                            working.DateVal = timeVal;
                            working.Day = (byte)time.Day;
                            working.Month = (byte)time.Month;
                            working.Year = time.Year;
                            working.Domain = (string)dyn.domain;
                            working.IsAccept = false;
                            working.Notes = (string)dyn.note;
                            working.ShiftRef = sid;
                            working.Username = username;
                            working.WorkOrOff = false;
                            working.WBegin = timeVal + (shift.XBegin * 60);
                            working.WEnd = timeVal + (shift.XEnd * 60);
                            //var countWorkingInMonth = context.Working.Where(w=>w.Username == username && w.Month == working.Month && w.ye)
                            context.Working.Add(working);
                            var user = context.Employee.Where(u => u.Username == working.Username).FirstOrDefault();
                            user.WHM += shift.Duration * 60;
                            context.SaveChanges();
                            CacheCenter.ResetWorking(working.AreaId.ToString(), working.DateVal.ToString());
                            CacheCenter.ResetEmployee(tokenModel.Domain);
                            updateChanges("USERS", tokenModel.Domain, "");
                        }
                    }
                }, "", new object[] { tokenModel, data, id });

            return id.ToString();
            }
        }

        [Route("api/main/working/update-working-by-shift")]
        [HttpPost]
        public string UpdateWorkingByShift(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                Guid id = PNUtility.Helpers.UtilityHelper.ParseGuid((string)data.id);

                MyTasks.DBTask.AddTask((a, k) => {
                    var wid = (Guid)a[2];
                    var tm = (TokenModel)a[0];
                    var dyn = (dynamic)a[1];
                    using (var context = new EF.HrCloudEntities())
                    {
                        EF.Working working = context.Working.Where(w => w.WorkingId == wid && w.Domain == tm.Domain).FirstOrDefault();
                        working.Notes = (string)dyn.note;
                        context.SaveChanges();
                        CacheCenter.ResetWorking(working.AreaId.ToString(), working.DateVal.ToString());
                    }
                }, "", new object[] { tokenModel, data, id });

                return id.ToString();
            }
        }

        [Route("api/main/working/off-working-by-shift")]
        [HttpPost]
        public string OffWorkingByShift(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                Guid id = Guid.NewGuid();
                MyTasks.DBTask.AddTask((a, k) => {
                    var wid = (Guid)a[2];
                    var tm = (TokenModel)a[0];
                    var dyn = (dynamic)a[1];
                    using (var context = new EF.HrCloudEntities())
                    {
                        var sid = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.sid);
                        var shift = context.Shift.Where(s => s.ShiftId == sid).FirstOrDefault();

                        var time = Helper.ParseTimeFromDash((string)dyn.day);
                        var timeVal = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(time);

                        var aid = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.aid);
                        var username = (string)dyn.username;
                        var accept = ((string)dyn.accept) == "y";
                        var findWorkings = context.Working.Where(w => w.DateVal == timeVal && w.ShiftRef == sid && w.Username == username).ToList();
                        bool willCreate = true;
                        if (findWorkings != null && findWorkings.Count > 0)
                        {
                            var count = findWorkings.Where(w => w.Status == 0).Count();
                            willCreate = count == 0;
                        }
                        if (willCreate)
                        {
                            EF.Working working = new EF.Working();
                            working.WorkingId = wid;
                            working.AreaId = aid;
                            working.CreateBy = (string)dyn.admin;
                            working.CreateDate = DateTime.Now;
                            working.DateVal = timeVal;
                            working.Day = (byte)time.Day;
                            working.Month = (byte)time.Month;
                            working.Year = time.Year;
                            working.Domain = (string)dyn.domain;
                            working.IsAccept = accept;
                            working.Reason = (string)dyn.note;
                            working.ShiftRef = sid;
                            working.Username = username;
                            working.WorkOrOff = true;
                            working.WBegin = timeVal + (shift.XBegin * 60);
                            working.WEnd = timeVal + (shift.XEnd * 60);
                            //var countWorkingInMonth = context.Working.Where(w=>w.Username == username && w.Month == working.Month && w.ye)
                            context.Working.Add(working);
                            var user = context.Employee.Where(u => u.Username == working.Username).FirstOrDefault();
                            user.ShiftOff += 1;
                            context.SaveChanges();
                            CacheCenter.ResetWorking(working.AreaId.ToString(), working.DateVal.ToString());
                            CacheCenter.ResetEmployee(tokenModel.Domain);
                            updateChanges("USERS", tokenModel.Domain, "");
                            TimelineService.AddAbsent(context, tm, working);
                        }
                    }
                }, "", new object[] { tokenModel, data, id });

                return id.ToString();
            }
        }

        [Route("api/main/working/update-off-working-by-shift")]
        [HttpPost]
        public string UpdateOffWorkingByShift(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                Guid id = PNUtility.Helpers.UtilityHelper.ParseGuid((string)data.id);
                MyTasks.DBTask.AddTask((a, k) => {
                    var wid = (Guid)a[2];
                    var tm = (TokenModel)a[0];
                    var dyn = (dynamic)a[1];
                    using (var context = new EF.HrCloudEntities())
                    {
                        var accept = ((string)dyn.accept) == "y";
                        EF.Working working = context.Working.Where(w => w.WorkingId == wid && w.Domain == tm.Domain).FirstOrDefault();
                        working.IsAccept = accept;
                        working.Reason = (string)dyn.note;
                        context.SaveChanges();
                        CacheCenter.ResetWorking(working.AreaId.ToString(), working.DateVal.ToString());                        
                    }
                }, "", new object[] { tokenModel, data, id });

                return id.ToString();
            }
        }

        [Route("api/main/working/set-working-by-time")]
        [HttpPost]
        public string SetWorkingByTime(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                Guid id = Guid.NewGuid();
                MyTasks.DBTask.AddTask((a, k) => {
                    var wid = (Guid)a[2];
                    var tm = (TokenModel)a[0];
                    var dyn = (dynamic)a[1];
                    using (var context = new EF.HrCloudEntities())
                    {
                        var beginTime = PNUtility.Helpers.UtilityHelper.ParseLong((string)dyn.begin);
                        var endTime = PNUtility.Helpers.UtilityHelper.ParseLong((string)dyn.end);

                        var aid = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.aid);
                        var username = (string)dyn.username;

                        var time = Helper.ParseTimeFromDash((string)dyn.day);
                        var timeVal = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(time);

                        EF.Working working = new EF.Working();
                        working.WorkingId = wid;
                        working.AreaId = aid;
                        working.CreateBy = (string)dyn.admin;
                        working.CreateDate = DateTime.Now;
                        working.DateVal = timeVal;
                        working.Day = (byte)time.Day;
                        working.Month = (byte)time.Month;
                        working.Year = time.Year;
                        working.Domain = (string)dyn.domain;
                        working.IsAccept = false;
                        working.Notes = (string)dyn.note;
                        working.Username = username;
                        working.WorkOrOff = false;
                        working.WBegin = beginTime;
                        working.WEnd = endTime;
                        working.Rate = PNUtility.Helpers.UtilityHelper.ParseInt((string)dyn.rate);
                        //var countWorkingInMonth = context.Working.Where(w=>w.Username == username && w.Month == working.Month && w.ye)
                        context.Working.Add(working);
                        var user = context.Employee.Where(u => u.Username == working.Username).FirstOrDefault();
                        user.WHM += (endTime - beginTime) / 60;
                        context.SaveChanges();
                        CacheCenter.ResetWorking(working.AreaId.ToString(), working.DateVal.ToString());
                        CacheCenter.ResetEmployee(tokenModel.Domain);
                        updateChanges("USERS", tokenModel.Domain, "");
                    }
                }, "", new object[] { tokenModel, data, id });

                return id.ToString();
            }
        }

        [Route("api/main/working/update-working-by-time")]
        [HttpPost]
        public string UpdateWorkingByTime(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                Guid id = PNUtility.Helpers.UtilityHelper.ParseGuid((string)data.id);
                MyTasks.DBTask.AddTask((a, k) => {
                    var wid = (Guid)a[2];
                    var tm = (TokenModel)a[0];
                    var dyn = (dynamic)a[1];
                    using (var context = new EF.HrCloudEntities())
                    {
                        var beginTime = PNUtility.Helpers.UtilityHelper.ParseLong((string)dyn.begin);
                        var endTime = PNUtility.Helpers.UtilityHelper.ParseLong((string)dyn.end);

                        var aid = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.aid);
                        var username = (string)dyn.username;

                        EF.Working working = context.Working.Where(w => w.WorkingId == wid && w.Domain == tm.Domain).FirstOrDefault();
                        var dval = (working.WEnd - working.WBegin) / 60;

                        working.IsAccept = false;
                        working.Notes = (string)dyn.note;

                        working.WorkOrOff = false;
                        working.WBegin = beginTime;
                        working.WEnd = endTime;
                        working.Rate = PNUtility.Helpers.UtilityHelper.ParseInt((string)dyn.rate);

                        var user = context.Employee.Where(u => u.Username == working.Username).FirstOrDefault();
                        user.WHM -= dval;
                        user.WHM += (endTime - beginTime) / 60;
                        context.SaveChanges();
                        CacheCenter.ResetWorking(working.AreaId.ToString(), working.DateVal.ToString());
                        CacheCenter.ResetEmployee(tokenModel.Domain);
                        updateChanges("USERS", tokenModel.Domain, "");
                    }
                }, "", new object[] { tokenModel, data, id });

                return id.ToString();
            }
        }

        [Route("api/main/working/get-working-by-day/{area}/{day}")]
        [HttpGet]
        public HttpResponseMessage GetWorkingByDay(string area, string day)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var date = Helper.ParseTimeFromDash(day);
            var timestamp = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(date);
            var aid = PNUtility.Helpers.UtilityHelper.ParseGuid(area);
            var key = area + "*" + timestamp.ToString();
            var list = CacheCenter.GetWorking(key);
            response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list));
            return response;
        }

        [Route("api/main/working/get-working-by-day/{begin}/{end}/{username}")]
        [HttpGet]
        public HttpResponseMessage GetWorkingByUsername(string begin, string end, string username)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                HttpResponseMessage response = new HttpResponseMessage();
                response.Content = new StringContent("");
                return response;
            }
            else
            {
                using(var context = new EF.HrCloudEntities())
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    var dayBegin = Helper.ParseTimeFromDash(begin);
                    var dayEnd = Helper.ParseTimeFromDash(end);
                    var dayValBegin = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(dayBegin);
                    var dayValEnd = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(dayEnd);
                    var list = context.Working.Where(a => a.DateVal >= dayValBegin && a.DateVal <= dayValEnd && a.Username == username && a.Status == 0).Select(w => new WorkingModel()
                    {
                        Id = w.WorkingId,
                        Accept = w.IsAccept,
                        Begin = w.WBegin,
                        End = w.WEnd,
                        IsOff = w.WorkOrOff,
                        Note = w.Notes,
                        Reason = w.Reason,
                        ShiftId = w.ShiftRef.HasValue ? w.ShiftRef.Value.ToString() : "",
                        Status = w.Status,
                        Username = w.Username,
                        AreaId = w.AreaId,
                        Rate = w.Rate.HasValue ? w.Rate.Value : 0,
                        DateVal = w.DateVal
                    }).ToList();
                    response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list));
                    return response;
                }                
            }
           
        }

        [Route("api/main/working/make-working-billing")]
        [HttpPost]
        public HttpResponseMessage Report(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                HttpResponseMessage response = new HttpResponseMessage();
                response.Content = new StringContent("");
                return response;
            }
            else
            {
                //HttpResponseMessage response = new HttpResponseMessage();
                //MyTasks.DBTask.AddTask((a, k) => {
                //    var tm = (TokenModel)a[0];
                //    var dyn = (dynamic)a[1];
                //    using (var context = new EF.HrCloudEntities())
                //    {
                        
                //    }
                //}, "", new object[] { tokenModel, data });
                //response.Content = new StringContent("ok");
                //return response;

                HttpResponseMessage response = new HttpResponseMessage();
                Guid id = Guid.NewGuid();
                MyTasks.DBTask.AddTask((a, k) => {
                    var tm = (TokenModel)a[0];
                    var dyn = (dynamic)a[1];
                    var pid = (Guid)a[2];
                    using (var context = new EF.HrCloudEntities())
                    {
                        EF.Payment pm = new EF.Payment();
                        pm.PaymentId = pid;
                        pm.OwnerId = tm.OwnerId;
                        pm.Domain = tm.Domain;
                        pm.Username = (string)dyn.username;
                        pm.FromDay = PNUtility.Helpers.UtilityHelper.ParseInt((string)dyn.fromday);
                        pm.ToDay = PNUtility.Helpers.UtilityHelper.ParseInt((string)dyn.today);
                        pm.PrePay = PNUtility.Helpers.UtilityHelper.ParseLong((string)dyn.prepay);
                        pm.Bonus = PNUtility.Helpers.UtilityHelper.ParseLong((string)dyn.bonus);
                        pm.SubPay = PNUtility.Helpers.UtilityHelper.ParseLong((string)dyn.sub);
                        pm.Reason = (string)dyn.reason;
                        pm.Note = (string)dyn.note;
                        pm.CreateDate = DateTime.Now;                        
                        EF.PaymentJson pj = new EF.PaymentJson();
                        pj.PayJsonId = pm.PaymentId;
                        pj.Json = (string)dyn.workings;
                        
                        pm.TotalMoney = PNUtility.Helpers.UtilityHelper.ParseLong((string)dyn.total);
                        context.Payment.Add(pm);
                        context.PaymentJson.Add(pj);
                        context.SaveChanges();
                    }
                }, "", new object[] { tokenModel, data, id });
                response.Content = new StringContent(id.ToString());
                return response;
            }

        }

        [Route("api/main/working/payment-detail/{id}")]
        [HttpGet]
        public HttpResponseMessage GetPayemtDetail(string id)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                HttpResponseMessage response = new HttpResponseMessage();
                response.Content = new StringContent("");
                return response;
            }
            else
            {
                using (var context = new EF.HrCloudEntities())
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    var pid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    var payment = context.Payment.Where(a => a.PaymentId == pid && a.OwnerId == tokenModel.OwnerId).FirstOrDefault();
                    
                    if(payment != null)
                    {
                        var detail = context.PaymentJson.Where(a=>a.PayJsonId == pid).FirstOrDefault();
                        if (detail != null)
                        {
                            var result = new { payment = payment, detail = detail.Json };
                            response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(result));
                            return response;
                        }
                    }
                    response.Content = new StringContent("");
                    return response;
                }
            }

        }

        [Route("api/main/working/get-payment-by-user/{username}/{page}")]
        [HttpGet]
        public HttpResponseMessage GetPayemtByUser(string username, string page)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                HttpResponseMessage response = new HttpResponseMessage();
                response.Content = new StringContent("");
                return response;
            }
            else
            {
                using (var context = new EF.HrCloudEntities())
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    var list = context.Payment.Where(a => a.Username == username && a.OwnerId == tokenModel.OwnerId).OrderByDescending(a=>a.CreateDate).ToList();
                    response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list));
                    return response;
                }
            }

        }

        [Route("api/main/working/cancel-billing")]
        [HttpPost]
        public string CancelBiling(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                MyTasks.DBTask.AddTask((args, key) =>
                {
                    var tm = (TokenModel)args[0];
                    var dyn = (dynamic)args[1];
                    var id = (string)dyn.id;
                    var wid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    using (var context = new EF.HrCloudEntities())
                    {
                        var working = context.Payment.Where(a => a.OwnerId == tm.OwnerId && a.PaymentId == wid).FirstOrDefault();
                        if (working != null && working.Status == 0)
                        {
                            working.Status = 4;
                            context.SaveChanges();
                        }
                    }
                }, "", new object[] { tokenModel, data });
                return "ok";
            }
        }

        [Route("api/main/working/complete-billing")]
        [HttpPost]
        public string CompleteBiling(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                MyTasks.DBTask.AddTask((args, key) =>
                {
                    var tm = (TokenModel)args[0];
                    var dyn = (dynamic)args[1];
                    var id = (string)dyn.id;
                    var check = (string)dyn.fund;
                    var wid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    using (var context = new EF.HrCloudEntities())
                    {
                        var working = context.Payment.Where(a => a.OwnerId == tm.OwnerId && a.PaymentId == wid && a.Status == 0).FirstOrDefault();
                        if (working != null)
                        {
                            working.Status = 1;                            
                        }
                        if(working != null && check == "1")
                        {
                            working.HasFundReport = true;
                            EF.Transaction tran = new EF.Transaction();
                            tran.CreateDate = DateTime.Now;
                            tran.DoBy = tm.Username;
                            tran.DoDate = tran.CreateDate;
                            tran.Explain = "[PHÁT LƯƠNG] Cho " + (string)dyn.name + " - (AUTO)";
                            tran.ExplainU = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(tran.Explain);
                            tran.IsOut = true;
                            tran.IsSel = false;
                            tran.Photo = "";
                            tran.Value = working.TotalMoney;
                            tran.TransactionId = Guid.NewGuid();
                            var fundDBO = context.Fund.Where(a => a.OwnerId == tm.OwnerId).FirstOrDefault();
                            if (fundDBO == null)
                            {
                                fundDBO = new EF.Fund();
                                fundDBO.FundID = Guid.NewGuid();
                                fundDBO.OwnerId = tm.OwnerId;
                                fundDBO.InTotal = 0;
                                fundDBO.OutTotal = 0;
                                fundDBO.Account = 0;
                                context.Fund.Add(fundDBO);
                            }
                            tran.FuncId = fundDBO.FundID;                            
                            fundDBO.Account -= tran.Value;
                            fundDBO.OutTotal += tran.Value;
                            tran.Remain = fundDBO.Account;
                            context.Transaction.Add(tran);
                            context.SaveChanges();
                        }
                        context.SaveChanges();
                    }
                }, "", new object[] { tokenModel, data });
                return "ok";
            }
        }

        [Route("api/main/working/delete-working")]
        [HttpPost]
        public string DeleteWorking(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                MyTasks.DBTask.AddTask((args, key) =>
                {
                    var tm = (TokenModel)args[0];
                    var dyn = (dynamic)args[1];
                    var id = (string)dyn.id;
                    var wid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    using(var context = new EF.HrCloudEntities())
                    {
                        var working = context.Working.Where(a => a.WorkingId == wid && a.Domain == tm.Domain).FirstOrDefault();
                        if(working != null)
                        {
                            working.Status = 2;
                            var user = context.Employee.Where(u => u.Username == working.Username).FirstOrDefault();
                            user.WHM -= (working.WEnd - working.WBegin) / 60;
                            CacheCenter.ResetWorking(working.AreaId.ToString(), working.DateVal.ToString());
                            CacheCenter.ResetEmployee(tokenModel.Domain);
                            updateChanges("USERS", tokenModel.Domain, "");
                            context.SaveChanges();
                        }
                    }
                }, "", new object[] { tokenModel, data });
                return "ok";
            }                
        }

        [Route("api/main/working/delete-working-by-shift")]
        [HttpPost]
        public string DeleteWorkingByShift(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                MyTasks.DBTask.AddTask((args, key) =>
                {
                    var tm = (TokenModel)args[0];
                    var dyn = (dynamic)args[1];
                    var id = (string)dyn.id;
                    var wid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    using (var context = new EF.HrCloudEntities())
                    {
                        var working = context.Working.Where(a => a.WorkingId == wid && a.Domain == tm.Domain).FirstOrDefault();
                        if (working != null)
                        {
                            working.Status = 2;
                            var user = context.Employee.Where(u => u.Username == working.Username).FirstOrDefault();
                            if (working.WorkOrOff)
                            {
                                if (user.ShiftOff > 0)
                                    user.ShiftOff--;
                            }
                            else
                            {
                                user.WHM -= (working.WEnd - working.WBegin) / 60;
                            }
                            CacheCenter.ResetWorking(working.AreaId.ToString(), working.DateVal.ToString());
                            CacheCenter.ResetEmployee(tokenModel.Domain);
                            updateChanges("USERS", tokenModel.Domain, "");
                            context.SaveChanges();
                        }
                    }
                }, "", new object[] { tokenModel, data });
                return "ok";
            }
        }

        [Route("api/main/working/get-working-by-user/{year}/{month}")]
        [HttpGet]
        public HttpResponseMessage GetWorkingByUserInMonth(string year, string month)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }

            var vmonth = PNUtility.Helpers.UtilityHelper.ParseInt(month);
            var vyear = PNUtility.Helpers.UtilityHelper.ParseInt(year);
            using (var context = new EF.HrCloudEntities())
            {
                var list = context.Working.Where(a => a.Username == tokenModel.Username && a.Month == vmonth && a.Year == vyear).Select(w => new WorkingModel()
                {
                    Id = w.WorkingId,
                    Accept = w.IsAccept,
                    Begin = w.WBegin,
                    End = w.WEnd,
                    IsOff = w.WorkOrOff,
                    Note = w.Notes,
                    Reason = w.Reason,
                    ShiftId = w.ShiftRef.HasValue ? w.ShiftRef.Value.ToString() : "",
                    Status = w.Status,
                    Username = w.Username,
                    AreaId = w.AreaId,
                    Rate = w.Rate.HasValue ? w.Rate.Value : 0
                }).ToList();
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list));
                return response;
            }
        }


    }
}
