﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HrHosting.Controllers
{
    [EnableCors(origins: "http://localhost:50940,https://hrcenter.vn", headers: "*", methods: "*")]
    public class AccountingControllerController : ApiController
    {
        private const string skey = "Hoasen@123";
        private const string UPDATE_CHANGES_URL = AppConst.UPDATE_CHANGES_URL_CENTER;

        [Route("api/main/accounting/new-fund-export")]
        public string NewFundExporting(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "400";
            }
            else
            {
                var oid = tokenModel.OwnerId;                
                MyTasks.DBTask.AddTask((args, key) => {
                    using(var context = new EF.HrCloudEntities())
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        var tm = (TokenModel)args[1];
                        var now = DateTime.Now;
                        var dyn = (dynamic)args[0];
                        var date = (string)dyn.date;
                        var text = date.Replace('-', '/');
                        var time = DateTime.ParseExact(text, "dd/MM/yyyy", provider);
                        var money = (string)dyn.money;
                        money = money.Replace(".", "");
                        var amount = PNUtility.Helpers.UtilityHelper.ParseLong(money);
                        var photo = (string)dyn.photo;
                        var newPhotoPath = "";
                        var ownerID = (Guid)args[2];
                        if (!string.IsNullOrEmpty(photo))
                        {
                            var fileName = Helper.GetFileNameFromTemphotoFolder(photo);
                            var newPath = "/Media/Owners/" + tm.Domain + "/Accounting/" + fileName;
                            newPhotoPath = newPath;
                            Helper.MoveFile(photo, newPath);
                        }
                        EF.Transaction tran = new EF.Transaction();
                        tran.CreateDate = now;
                        tran.DoBy = tm.Username;
                        var isSel = !(now.Day == time.Day && now.Month == time.Month && now.Year == time.Year);
                        tran.DoDate = isSel ? time : now;
                        tran.Explain = (string)dyn.content;
                        tran.ExplainU = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(tran.Explain);
                        tran.IsOut = true;
                        tran.IsSel = isSel;
                        tran.Photo = newPhotoPath;
                        tran.Value = amount;
                        tran.RefUser = (string)dyn.user;
                        tran.TransactionId = Guid.NewGuid();
                        var fundDBO = context.Fund.Where(a => a.OwnerId == ownerID).FirstOrDefault();
                        if(fundDBO == null)
                        {
                            fundDBO = new EF.Fund();
                            fundDBO.FundID = Guid.NewGuid();
                            fundDBO.OwnerId = ownerID;
                            fundDBO.InTotal = 0;
                            fundDBO.OutTotal = 0;
                            fundDBO.Account = 0;
                            context.Fund.Add(fundDBO);
                        }
                        tran.FuncId = fundDBO.FundID;
                        context.Transaction.Add(tran);
                        fundDBO.Account -= tran.Value;
                        fundDBO.OutTotal -= tran.Value;
                        tran.Remain = fundDBO.Account;
                        context.SaveChanges();
                    }
                }, tokenModel.Username, new object[] { data, tokenModel, oid });
                return "ok";
            }
        }

        [Route("api/main/accounting/new-fund-import")]
        public string NewFundImporting(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "400";
            }
            else
            {
                var oid = tokenModel.OwnerId;
                MyTasks.DBTask.AddTask((args, key) => {
                    using (var context = new EF.HrCloudEntities())
                    {
                        try
                        {
                            CultureInfo provider = CultureInfo.InvariantCulture;
                            var tm = (TokenModel)args[1];
                            var now = DateTime.Now;
                            var dyn = (dynamic)args[0];
                            var time = DateTime.Now;
                            var money = (string)dyn.money;
                            money = money.Replace(".", "");
                            var amount = PNUtility.Helpers.UtilityHelper.ParseLong(money);
                            var photo = (string)dyn.photo;
                            var newPhotoPath = "";
                            if (!string.IsNullOrEmpty(photo))
                            {
                                var fileName = Helper.GetFileNameFromTemphotoFolder(photo);
                                var newPath = "/Media/Owners/" + tm.Domain + "/Accounting/" + fileName;
                                newPhotoPath = newPath;
                                Helper.MoveFile(photo, newPath);
                            }
                            var ownerID = (Guid)args[2];
                            EF.Transaction tran = new EF.Transaction();
                            tran.CreateDate = now;
                            tran.DoBy = tm.Username;
                            tran.DoDate = now;
                            tran.Explain = (string)dyn.content;
                            tran.ExplainU = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(tran.Explain);
                            tran.IsOut = false;
                            tran.IsSel = false;
                            tran.Photo = newPhotoPath;
                            tran.Value = amount;
                            tran.TransactionId = Guid.NewGuid();
                            var fundDBO = context.Fund.Where(a => a.OwnerId == ownerID).FirstOrDefault();
                            if (fundDBO == null)
                            {
                                fundDBO = new EF.Fund();
                                fundDBO.FundID = Guid.NewGuid();
                                fundDBO.OwnerId = ownerID;
                                fundDBO.InTotal = 0;
                                fundDBO.OutTotal = 0;
                                fundDBO.Account = 0;
                                context.Fund.Add(fundDBO);
                            }
                            tran.FuncId = fundDBO.FundID;
                            context.Transaction.Add(tran);
                            fundDBO.Account += tran.Value;
                            fundDBO.InTotal += tran.Value;
                            tran.Remain = fundDBO.Account;
                            context.SaveChanges();
                        }catch(Exception exxx)
                        {
                            System.IO.File.WriteAllText(Helper.ConvertToServerPath("/App_Data/fundError.txt"), exxx.Message);
                        }
                    }
                }, tokenModel.Username, new object[] { data, tokenModel, oid });
                return "ok";
            }
        }

        [Route("api/main/accounting/read-fund-transactions")]
        public HttpResponseMessage ReadTransactions(dynamic data)
        {
            int step = 0;
            try
            {
                var fromDate = (string)data.fromDate;
                var toDate = (string)data.toDate;
                var min = (string)data.min;
                var max = (string)data.max;
                var type = (string)data.type;
                var query = (string)data.query;
                var uref = (string)data.uref;
                CultureInfo provider = CultureInfo.InvariantCulture;
                step ++;

                using (var context = new EF.HrCloudEntities())
                {
                    var tokenModel = GetValidToken();
                    var fundDBO = context.Fund.Where(a => a.OwnerId == tokenModel.OwnerId).FirstOrDefault();
                    step++;
                    var text = fromDate.Replace('-', '/');
                    var vFromDate = DateTime.ParseExact(text, "dd/MM/yyyy", provider);
                    step++;
                    text = toDate.Replace('-', '/');
                    var vToDate = DateTime.ParseExact(text, "dd/MM/yyyy", provider).AddHours(24);
                    step++;
                    var vMin = PNUtility.Helpers.UtilityHelper.ParseLong(min);
                    var vMax = PNUtility.Helpers.UtilityHelper.ParseLong(max);
                    step++;
                    query = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(query);
                    var isOut = type == "0";
                    step++;
                    var dataSource = context.Transaction.Where(a => a.DoDate >= vFromDate && a.DoDate <= vToDate && a.IsOut == isOut && a.FuncId == fundDBO.FundID);
                    if (vMin != 0)
                        dataSource = dataSource.Where(a => a.Value >= vMin);
                    step++;
                    if (vMax != 0)
                        dataSource = dataSource.Where(a => a.Value <= vMax);
                    step++;
                    if (!string.IsNullOrEmpty(query))
                        dataSource = dataSource.Where(a => a.ExplainU.Contains(query));
                    step++;
                    if (!string.IsNullOrEmpty(uref))
                        dataSource = dataSource.Where(a => a.RefUser == uref);
                    step++;
                    var results = dataSource.OrderByDescending(a => a.DoDate).ToList();
                    var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(results);
                    step++;
                    HttpResponseMessage response = new HttpResponseMessage();
                    response.Content = new StringContent(jdata);
                    return response;
                }
            }
            catch(Exception exx)
            {
                HttpResponseMessage response = new HttpResponseMessage();
                response.Content = new StringContent(exx.Message + ": " + step);
                return response;
            }
        }

        [Route("api/main/accounting/get-current-fund-value")]
        [HttpGet]
        public string GetCurrentFundValue()
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "xxx";
            }
            else
            {
                using(var context = new EF.HrCloudEntities())
                {
                    var fund = context.Fund.Where(a => a.OwnerId == tokenModel.OwnerId).FirstOrDefault();
                    if (fund != null)
                        return PNUtility.Helpers.UtilityHelper.MoneyFormat(fund.Account);
                    else
                        return "zzz";
                }
            }
        }

        [Route("api/main/accounting/quick-report/get-report-by-day")]
        public HttpResponseMessage ReadReportByDay(dynamic data)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            var oid = (string)data.oid;
            var day = (string)data.day;
            using (var context = new EF.HrCloudEntities())
            {
                var text = day.Replace('-', '/');
                var vdate = DateTime.ParseExact(text, "dd/MM/yyyy", provider);
                var endDate = vdate.AddHours(24);
                var ownerId = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                var fund = CacheCenter.GetFundIdByOwner(ownerId);
                var dataSource = context.Transaction.Where(a => a.FuncId == fund.FundID && a.CreateDate >= vdate && a.CreateDate < endDate).OrderByDescending(a=>a.CreateDate).ToList();
                
                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(dataSource);

                HttpResponseMessage response = new HttpResponseMessage();
                response.Content = new StringContent(jdata);
                return response;
            }
        }

        [Route("api/main/accounting/quick-report/get-report-by-last-day")]
        public HttpResponseMessage ReadReportByLastDay(dynamic data)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            var oid = (string)data.oid;
            var day = (string)data.day;
            using (var context = new EF.HrCloudEntities())
            {
                var text = day.Replace('-', '/');
                var vdate = DateTime.ParseExact(text, "dd/MM/yyyy", provider);
                var endDate = vdate.AddHours(24);
                var ownerId = PNUtility.Helpers.UtilityHelper.ParseGuid(oid);
                var fund = CacheCenter.GetFundIdByOwner(ownerId);
                var dataSource = context.Transaction.Where(a => a.FuncId == fund.FundID && a.CreateDate < vdate).OrderByDescending(a => a.CreateDate).FirstOrDefault();
                var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(dataSource);

                HttpResponseMessage response = new HttpResponseMessage();
                response.Content = new StringContent(jdata);
                return response;
            }
        }
        [Route("api/main/accounting/edit-transaction")]
        public string EditTransaction(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "400";
            }
            else
            {
                var oid = tokenModel.OwnerId;
                MyTasks.DBTask.AddTask((args, key) => {
                    using (var context = new EF.HrCloudEntities())
                    {
                        var dyn = (dynamic)args[0];
                        var id = (string)dyn.id;
                        var tid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                        var name = (string)dyn.name;
                        var tranDB = context.Transaction.Where(a => a.TransactionId == tid).FirstOrDefault();
                        tranDB.Explain = name;
                        tranDB.ExplainU = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(tranDB.Explain);
                        context.SaveChanges();
                    }
                }, tokenModel.Username, new object[] { data, tokenModel, oid });
                return "ok";
            }
        }


        private TokenModel GetValidToken()
        {
            var hd = Request.Headers.GetValues("access_token");
            if (hd != null && hd.Count() > 0)
            {
                var at = hd.ElementAt(0);
                return CacheCenter.GetTokenModel(at);
            }
            else
                return null;
        }


    }
}
