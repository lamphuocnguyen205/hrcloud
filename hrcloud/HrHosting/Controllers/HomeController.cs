﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace HrHosting.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            //using(var context = new EF.HrCloudEntities())
            //{
            //    var user = context.Employee.Where(a => a.Username == "sn_longnguyen").FirstOrDefault();
            //    if (user != null)
            //    {
            //        var rate = new EF.Rate();
            //        rate.Name = "Tăng ca thường x 1.6";
            //        rate.RateVal = 1.6;
            //        rate.OwnerId = user.OwnerId;
            //        context.Rate.Add(rate);

            //        var rate2 = new EF.Rate();
            //        rate2.Name = "Tăng ca thường x 2.0";
            //        rate2.RateVal = 2.0;
            //        rate2.OwnerId = user.OwnerId;
            //        context.Rate.Add(rate2);

            //        context.SaveChanges();
            //    }
            //}
            //var time = PNUtility.Helpers.UtilityHelper.JavaTimeStampToDateTime(1605345000);
            //var timestapm = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
            using(var context = new EF.HrCloudEntities())
            {
                var count = context.Employee.ToList().Count;
                ViewBag.Cout = count;
                return View();
            }
            
        }
    }
}
