﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HrHosting.Controllers
{
    [EnableCors(origins: "http://localhost:50940,https://hrcenter.vn", headers: "*", methods: "*")]
    public class AdvanceAPIController : BaseAPIController
    {
        [Route("api/main/advance/get-plan-by-month/{month}")]
        [HttpGet]
        public HttpResponseMessage GetPlanByMonth(string month)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var list = CacheCenter.GetPlanByMonth(tokenModel.Domain, month);
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list));
                return response;
            }
        }

        [Route("api/main/advance/get-advance-by-month/{month}")]
        [HttpGet]
        public HttpResponseMessage GetAdvanceByMonth(string month)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var list = CacheCenter.GetAdvanceByMonth(tokenModel.Domain, month);
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list));
                return response;
            }
        }

        [Route("api/main/advance/create-plan")]
        [HttpPost]
        public string CreateNewPlan(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                Guid Id = Guid.NewGuid();
                MyTasks.DBTask.AddTask((args, key) => {
                    var pid = (Guid)args[2];
                    var tm = (TokenModel)args[0];
                    var dyn = (dynamic)args[1];
                    using(var context = new EF.HrCloudEntities())
                    {
                        var now = DateTime.Now;
                        var month = now.Month;
                        var text = now.Year.ToString() + (month < 9 ? "0" + month.ToString() : month.ToString());
                        var monVal = PNUtility.Helpers.UtilityHelper.ParseInt(text);
                        EF.Plan pl = new EF.Plan();
                        pl.PlanId = pid;
                        pl.CreateBy = tm.Username;
                        pl.CreateDate = now;
                        pl.Domain = tm.Domain;
                        pl.MonVal = monVal;
                        pl.Name = (string)dyn.name;
                        pl.OwnerId = tm.OwnerId;
                        context.Plan.Add(pl);
                        context.SaveChanges();
                        CacheCenter.ResetPlans(tm.Domain, pl.MonVal.ToString());
                    }

                }, "", new object[] { tokenModel, data, Id });
                return Id.ToString();
            }
        }

        [Route("api/main/advance/post-new-advance")]
        [HttpPost]
        public string PostNewAdvance(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                Guid Id = Guid.NewGuid();
                MyTasks.DBTask.AddTask((args, key) => {
                    var pid = (Guid)args[2];
                    var tm = (TokenModel)args[0];
                    var dyn = (dynamic)args[1];
                    using (var context = new EF.HrCloudEntities())
                    {
                        var now = DateTime.Now;
                        var month = now.Month;
                        var text = now.Year.ToString() + (month < 9 ? "0" + month.ToString() : month.ToString());
                        var monVal = PNUtility.Helpers.UtilityHelper.ParseInt(text);
                        EF.Advance adv = new EF.Advance();
                        adv.AdvanceId = pid;
                        adv.CreateBy = tm.Username;
                        adv.CreateDate = now;
                        adv.DateVal = 0;
                        adv.Day = (byte)now.Day;
                        adv.Domain = tm.Domain;
                        adv.IsActive = true;
                        adv.Money = PNUtility.Helpers.UtilityHelper.ParseLong((string)dyn.money);
                        adv.Month = (byte)now.Month;
                        adv.Note = (string)dyn.note;
                        adv.MonthVal = monVal;
                        bool changePlan = false;
                        if((string)dyn.plan != "-1")
                        {
                            var planID = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.plan);
                            var planModel = context.Plan.Where(a => a.PlanId == planID).FirstOrDefault();
                            if(planID != null)
                            {
                                adv.PlanId = planID;
                                planModel.Total += adv.Money;
                                changePlan = true;
                            }
                        }
                        adv.Username = (string)dyn.username;
                        var user = context.Employee.Where(a => a.Username == adv.Username).FirstOrDefault();
                        if(user != null)
                        {
                            user.Advance += adv.Money;
                        }
                        adv.Year = now.Year;
                        context.Advance.Add(adv);                        
                        context.SaveChanges();
                        if (changePlan)
                            CacheCenter.ResetPlans(tm.Domain, monVal.ToString());

                        CacheCenter.ResetAdvances(tm.Domain, monVal.ToString());
                        CacheCenter.ResetEmployee(tm.Domain);
                        updateChanges("USERS", tm.Domain, "");
                    }

                }, "", new object[] { tokenModel, data, Id });
                return Id.ToString() + "*" + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            }
        }

        [Route("api/main/advance/post-new-advance-rev")]
        [HttpPost]
        public string PostNewAdvanceRev(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                Guid Id = Guid.NewGuid();
                MyTasks.DBTask.AddTask((args, key) => {
                    var pid = (Guid)args[2];
                    var tm = (TokenModel)args[0];
                    var dyn = (dynamic)args[1];
                    using (var context = new EF.HrCloudEntities())
                    {
                        var now = DateTime.Now;
                        var month = now.Month;
                        var text = now.Year.ToString() + (month < 9 ? "0" + month.ToString() : month.ToString());
                        var monVal = PNUtility.Helpers.UtilityHelper.ParseInt(text);
                        EF.Advance adv = new EF.Advance();
                        adv.AdvanceId = pid;
                        adv.CreateBy = tm.Username;
                        adv.CreateDate = now;
                        adv.DateVal = 0;
                        adv.Day = (byte)now.Day;
                        adv.Domain = tm.Domain;
                        adv.IsActive = false;
                        adv.Money = PNUtility.Helpers.UtilityHelper.ParseLong((string)dyn.money);
                        adv.Month = (byte)now.Month;
                        adv.Note = (string)dyn.note;
                        adv.MonthVal = monVal;
                        bool changePlan = false;
                        if ((string)dyn.plan != "-1")
                        {
                            var planID = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dyn.plan);
                            var planModel = context.Plan.Where(a => a.PlanId == planID).FirstOrDefault();
                            if (planID != null)
                            {
                                adv.PlanId = planID;
                                planModel.Total -= adv.Money;
                                changePlan = true;
                            }
                        }
                        adv.Username = (string)dyn.username;
                        var user = context.Employee.Where(a => a.Username == adv.Username).FirstOrDefault();
                        if (user != null)
                        {
                            user.Advance -= adv.Money;
                        }
                        adv.Year = now.Year;
                        context.Advance.Add(adv);
                        context.SaveChanges();
                        if (changePlan)
                            CacheCenter.ResetPlans(tm.Domain, monVal.ToString());

                        CacheCenter.ResetAdvances(tm.Domain, monVal.ToString());
                        CacheCenter.ResetEmployee(tm.Domain);
                        updateChanges("USERS", tm.Domain, "");
                    }

                }, "", new object[] { tokenModel, data, Id });
                return Id.ToString() + "*" + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            }
        }
    }
}
