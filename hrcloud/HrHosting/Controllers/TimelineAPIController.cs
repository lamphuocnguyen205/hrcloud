﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HrHosting.Controllers
{
    [EnableCors(origins: "http://localhost:50940,https://hrcenter.vn", headers: "*", methods: "*")]
    public class TimelineAPIController : BaseAPIController
    {
        [Route("api/main/feed/get-feed/{page}")]
        [HttpGet]
        public HttpResponseMessage GetFeedByPage(string page)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                using(var context = new EF.HrCloudEntities())
                {
                    var vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
                    vpage = vpage == 0 ? 1 : vpage;
                    int skip = (vpage - 1) * 10;
                    var list = context.Post.Where(a => a.Domain == tokenModel.Domain && (a.TargetUser == tokenModel.Username || a.IsPublic)).OrderByDescending(a => a.CreateDate).Skip(skip).Take(10).ToList();
                    List<PostModel> feeds = new List<PostModel>();
                    foreach(var item in list)
                    {
                        PostModel pm = new PostModel();
                        pm.Id = item.PostId;
                        pm.JData = item.JData;
                        pm.RefId = item.RefId;
                        pm.Time = item.CreateTime;
                        pm.Date = item.CreateDate;
                        pm.Type = item.Type;
                        feeds.Add(pm);
                    }
                    var data = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(feeds);
                    response.Content = new StringContent(data);
                    return response;
                }
            }
        }
    }
}
