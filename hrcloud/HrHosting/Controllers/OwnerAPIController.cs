﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HrHosting.Controllers
{
    [EnableCors(origins: "http://localhost:50940,https://hrcenter.vn", headers: "*", methods: "*")]
    public class OwnerAPIController : ApiController
    {
        private const string UPDATE_CHANGES_URL = AppConst.UPDATE_CHANGES_URL_CENTER;

        private TokenModel GetValidToken()
        {
            var hd = Request.Headers.GetValues("access_token");
            if (hd != null && hd.Count() > 0)
            {
                var at = hd.ElementAt(0);
                return CacheCenter.GetTokenModel(at);
            }
            else
                return null;
        }

        private void updateChanges(string type, string domain, string id)
        {
            AChanges ac = new AChanges();
            ac.Type = type;
            ac.Time = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
            ac.Domain = domain;
            ac.Id = id;
            var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(ac);
            NetworkService.callPostAPI(UPDATE_CHANGES_URL, jdata, (str) => { }, () => { });
        }

        [Route("api/owner/get-owner-info")]
        public HttpResponseMessage GetOwnerInfo(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var info = CacheCenter.GetOwnerInfo(tokenModel.Domain);
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(info));
                return response;
            }
        }
    }
}
