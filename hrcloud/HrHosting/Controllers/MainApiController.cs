﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Data.Entity;
using System.Globalization;

namespace HrHosting.Controllers
{
    [EnableCors(origins: "http://localhost:50940,https://hrcenter.vn", headers: "*", methods: "*")]
    public class MainApiController : ApiController
    {
        private const string skey = "Hoasen@123";
        private const string UPDATE_CHANGES_URL = AppConst.UPDATE_CHANGES_URL_CENTER;

        [Route("api/main/create-user")]
        [HttpPost]
        public string CreateUser(dynamic data)
        {
            if (!isCenterCalled())
                return "-101";
            var jdata = (string)data.jdata;
            var user = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<AMUser>(jdata);
            EF.Employee em = new EF.Employee();
            em.CreateDate = DateTime.Now;
            em.Domain = user.Domain;
            em.FirstName = user.FirstName;
            em.Lastname = user.LastName;
            em.UName = PNUtility.Helpers.UtilityHelper.ConvertToUnSign3(user.FirstName + " " + user.LastName);
            em.JoinDate = DateTime.Now;
            em.OwnerId = user.OwnerId;
            em.Password = "";
            em.Role = user.Role;
            em.Username = user.Username;
            MyTasks.DBTask.AddTask((args, key) => {
                var employee = (EF.Employee)args[0];
                using (var context = new EF.HrCloudEntities())
                {
                    context.Employee.Add(employee);
                    context.SaveChanges();
                }
            }, "", new object[] { em });

            if (em.Role == "C_ADM")
            {
                var rootFolder = HostingEnvironment.MapPath("/Media/Owners/" + em.Domain);
                if (!System.IO.Directory.Exists(rootFolder))
                {
                    System.IO.Directory.CreateDirectory(rootFolder);
                    System.IO.Directory.CreateDirectory(rootFolder + "/Common");
                    System.IO.Directory.CreateDirectory(rootFolder + "/User");
                    System.IO.Directory.CreateDirectory(rootFolder + "/Company");
                    System.IO.Directory.CreateDirectory(rootFolder + "/Accounting");
                }
            }            
            updateChanges("USERS", user.Domain, "");
            CacheCenter.ResetEmployee(user.Domain);
            return "1";
        }

        [Route("api/main/employee-list")]
        [HttpGet]
        public HttpResponseMessage EmployeeList(string access_token, string page, string query)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            int vpage = PNUtility.Helpers.UtilityHelper.ParseInt(page);
            if (vpage == 0)
                vpage = 1;
            int skip = (vpage - 1) * 10;
            var token = CacheCenter.GetTokenModel(access_token);
            if (token != null)
            {
                var dataSource = CacheCenter.GetFullEmployeeList(token.Domain);
                var result = new List<EF.Employee>();
                if (string.IsNullOrEmpty(query))
                    result = dataSource.OrderByDescending(a => a.Lastname).Skip(skip).Take(10).ToList();
                else
                    result = dataSource.Where(a => a.UName.Contains(query)).OrderByDescending(a => a.Lastname).Skip(skip).Take(10).ToList();

                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(result.Select(a => { return new UserInfo() {
                    Username = a.Username,
                    LastName = a.Lastname,
                    FirstName = a.FirstName,
                    Role = a.Role,
                    Avatar = a.Photo,
                    Domain = a.Domain,
                    JoinDate = a.CreateDate.ToString("dd/MM/yyyy"),
                    Position = a.Position.HasValue ? a.Position.Value : 0,
                    IsAccounting = a.IsAccounting
                }; })));

                return response;
            }
            else
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                return response;
            }
        }

        [Route("api/main/employee-full-list")]
        [HttpGet]
        public HttpResponseMessage EmployeeFullList()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var dataSource = CacheCenter.GetFullEmployeeList(tokenModel.Domain);
                var result = new List<EF.Employee>();
                result = dataSource.OrderByDescending(a => a.Lastname).ToList();
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(result.Select(a => {
                    return new UserInfo()
                    {
                        Username = a.Username,
                        LastName = a.Lastname,
                        FirstName = a.FirstName,
                        Role = a.Role,
                        Avatar = a.Photo,
                        Domain = a.Domain,
                        JoinDate = a.JoinDate.ToString("dd/MM/yyyy"),
                        Position = a.Position.HasValue ? a.Position.Value : 0,
                        Sex = a.Sex,
                        Birthday = a.EmployeeDetail == null ? "01/01/1970" : a.EmployeeDetail.Birthday.HasValue ? a.EmployeeDetail.Birthday.Value.ToString("dd/MM/yyyy") : "01/01/1970",
                        CMND = a.EmployeeDetail == null ? "" : a.EmployeeDetail.CMND,
                        Addr = a.EmployeeDetail == null ? "" : a.EmployeeDetail.Addr,
                        Labels = a.EmployeeDetail == null ? "" : a.EmployeeDetail.Labels,
                        Pictures = a.EmployeeDetail == null ? "[]" : a.EmployeeDetail.PhotoCompany,
                        AreaId = a.AreaId.HasValue ? a.AreaId.Value.ToString() : "",
                        AreaM = a.AreaManagemments,
                        HW = (float)a.WHM,
                        SO = a.ShiftOff,
                        IaC = a.IsAccounting,
                        UName = a.UName,
                        Advance = a.Advance,
                        HValue = a.HValue,
                        Phone = a.Phone
                    };
                }).OrderBy(e=>e.LastName)));

                return response;
            }
            
        }

        [Route("api/main/update-access-token")]
        [HttpPost]
        public string UpdateAccessToken(dynamic data)
        {
            if (!isCenterCalled())
                return "-101";

            var jdata = (string)data.jdata;
            var tokenModel = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<TokenModel>(jdata);
            CacheCenter.Tokens.Add(tokenModel.Token, tokenModel);
            MyTasks.DBTask.AddTask((args, key) => {
                var tm = (TokenModel)args[0];
                using (var context = new EF.HrCloudEntities())
                {
                    EF.Token tok = new EF.Token();
                    tok.TokenId = tm.Token;
                    tok.CreateDate = tm.Create;
                    tok.Domain = tm.Domain;
                    tok.ExpiredDate = tok.CreateDate + 3600;
                    tok.OwnerId = tm.OwnerId;
                    tok.Username = tm.Username;
                    tok.RequetTime = 1;
                    context.Token.Add(tok);
                    context.SaveChanges();
                }
            }, "", new object[] { tokenModel });

            return "1";
        }

        [Route("api/main/create-new-area")]
        [HttpPost]
        public string CreateNewArea(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                Guid id = Guid.NewGuid();
                string tempPhoto = (string)data.photo;
                var newPhotoPath = "/Media/Owners/" + tokenModel.Domain + "/Common/" + tempPhoto.Substring(11, tempPhoto.Length - 11);
                MyTasks.DBTask.AddTask((a, k) => {

                    var newid = (Guid)a[0];
                    var tm = (TokenModel)a[1];
                    var obj = (dynamic)a[2];
                    var name = (string)obj.name;
                    var photo = (string)obj.photo;
                    EF.Area area = new EF.Area();
                    area.AreaId = newid;
                    area.Domain = tm.Domain;
                    area.Hide = false;
                    area.Name = name;
                    area.OwnerId = tm.OwnerId;
                    area.Photo = (string)a[3];
                    using (var context = new EF.HrCloudEntities())
                    {
                        context.Area.Add(area);
                        context.SaveChanges();
                        CacheCenter.AddNewArea(area);
                    }
                    MyTasks.FileTask.AddTask((a1, k1) => {
                        var serverTarget = HostingEnvironment.MapPath((string)a1[0]);
                        var serverSource = HostingEnvironment.MapPath(k1);
                        System.IO.File.Move(serverSource, serverTarget);
                    }, photo, new object[] { area.Photo });
                }, "", new object[] { id, tokenModel, data, newPhotoPath });
                updateChanges("AREA", tokenModel.Domain, "");
                return id.ToString() + "*" + newPhotoPath;
            }
        }

        [Route("api/main/update-area")]
        [HttpPost]
        public string UpdateArea(dynamic data)
        {
            var tokenModel = GetValidToken();
            string newPhotoPath = "";
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                var cphoto = (string)data.cphoto;
                var nphoto = (string)data.nphoto;
                string result = "";
                if (nphoto != cphoto)
                {
                    Guid id = Guid.NewGuid();
                    newPhotoPath = "/Media/Owners/" + tokenModel.Domain + "/Common/" + nphoto.Substring(11, nphoto.Length - 11);
                    result = newPhotoPath;
                }
                else
                {
                    result = cphoto;
                }


                MyTasks.DBTask.AddTask((a, k) => {
                    var tm = (TokenModel)a[0];
                    var obj = (dynamic)a[1];
                    var name = (string)obj.name;
                    var vcphoto = (string)obj.cphoto;
                    var vnphoto = (string)obj.nphoto;
                    var id = PNUtility.Helpers.UtilityHelper.ParseGuid(k);

                    using (var context = new EF.HrCloudEntities())
                    {
                        var area = context.Area.Where(x => x.AreaId == id).FirstOrDefault();
                        if (area != null)
                        {
                            area.Name = name;
                            if (vcphoto != vnphoto && !string.IsNullOrEmpty(vnphoto))
                            {
                                area.Photo = (string)a[2];
                                MyTasks.FileTask.AddTask((a1, k1) => {
                                    var serverTarget = HostingEnvironment.MapPath((string)a1[0]);
                                    var serverSource = HostingEnvironment.MapPath(k1);
                                    System.IO.File.Move(serverSource, serverTarget);
                                    var deleteFile = HostingEnvironment.MapPath((string)a1[1]);
                                    System.IO.File.Delete(deleteFile);
                                }, vnphoto, new object[] { area.Photo, vcphoto });
                            }

                            context.SaveChanges();
                            CacheCenter.ResetArea(tm.Domain);
                        }
                    }
                }, (string)data.id, new object[] { tokenModel, data, newPhotoPath });
                updateChanges("AREA", tokenModel.Domain, "");
                return result;
            }
        }

        [Route("api/main/hide-area")]
        [HttpPost]
        public string HideArea(dynamic data)
        {
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "";
            }
            else
            {
                var id = (string)data.id;

                MyTasks.DBTask.AddTask((a, k) => {
                    using (var context = new EF.HrCloudEntities())
                    {
                        var aid = PNUtility.Helpers.UtilityHelper.ParseGuid(k);
                        var area = context.Area.Where(x => x.AreaId == aid).FirstOrDefault();
                        if (area != null)
                        {
                            area.Hide = true;
                            context.SaveChanges();
                            CacheCenter.ResetArea((string)a[0]);
                        }
                    }
                }, id, new object[] { tokenModel.Domain });
                updateChanges("AREA", tokenModel.Domain, "");
                return id;
            }
        }

        [Route("api/main/area-list")]
        [HttpGet]
        public HttpResponseMessage AreaList()
        {
            HttpResponseMessage response = new HttpResponseMessage();

            var token = GetValidToken();
            if (token != null)
            {
                var list = CacheCenter.GetAllActiveArea(token.Domain).Select(a => new { Id = a.AreaId.ToString(), Name = a.Name, Photo = a.Photo }).ToList();
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list));
                return response;
            }
            else
            {
                response.Content = new StringContent("400");
                return response;
            }
        }

        [Route("api/main/create-shift")]
        [HttpPost]
        public HttpResponseMessage CreateShift(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                Guid id = Guid.NewGuid();

                MyTasks.DBTask.AddTask((a, k) => {
                    var shiftID = (Guid)a[2];
                    var dobj = (dynamic)a[1];
                    var tm = (TokenModel)a[0];
                    using (var context = new EF.HrCloudEntities())
                    {
                        var areaIdText = (string)dobj.areaId;
                        var areaId = PNUtility.Helpers.UtilityHelper.ParseGuid(areaIdText);
                        EF.Shift shift = new EF.Shift();
                        shift.AreaId = areaId;
                        shift.Domain = tm.Domain;
                        shift.Name = (string)dobj.name;
                        shift.ShiftId = shiftID;
                        shift.XBegin = PNUtility.Helpers.UtilityHelper.ParseInt((string)dobj.begin);
                        var duration = Helper.ParseFloat((string)dobj.duration);
                        shift.XEnd = shift.XBegin + (int)(duration * 60);
                        shift.Duration = duration;
                        shift.RateId = PNUtility.Helpers.UtilityHelper.ParseInt((string)dobj.rate);
                        context.Shift.Add(shift);
                        context.SaveChanges();
                        CacheCenter.ResetArea(tm.Domain);
                    }

                }, "", new object[] { tokenModel, data, id });

                updateChanges("SHIFT", tokenModel.Domain, "");

                response.Content = new StringContent(id.ToString());
                return response;
            }
        }

        [Route("api/main/update-shift")]
        [HttpPost]
        public HttpResponseMessage UpdateShift(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var begin = PNUtility.Helpers.UtilityHelper.ParseInt((string)data.begin);
                var duration = Helper.ParseFloat((string)data.duration);
                var text = Helper.GetShiftText(begin, duration);

                MyTasks.DBTask.AddTask((a, k) => {
                    var tm = (TokenModel)a[0];
                    var dobj = (dynamic)a[1];
                    var shiftID = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dobj.sid);

                    using (var context = new EF.HrCloudEntities())
                    {
                        var shift = context.Shift.Where(aa => aa.ShiftId == shiftID).FirstOrDefault();
                        if (shift != null)
                        {
                            shift.Name = (string)dobj.name;
                            shift.XBegin = PNUtility.Helpers.UtilityHelper.ParseInt((string)dobj.begin);
                            var duration1 = Helper.ParseFloat((string)dobj.duration);
                            shift.XEnd = shift.XBegin + (int)(duration1 * 60);
                            shift.Duration = duration1;
                            shift.RateId = PNUtility.Helpers.UtilityHelper.ParseInt((string)dobj.rate);
                            context.SaveChanges();
                            CacheCenter.ResetArea(tm.Domain);
                        }
                    }

                }, "", new object[] { tokenModel, data });
                updateChanges("SHIFT", tokenModel.Domain, "");
                response.Content = new StringContent(text);
                return response;
            }
        }

        [Route("api/main/delete-shift")]
        [HttpPost]
        public string DeleteShift(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "400";
            }
            else
            {
                var aid = (string)data.aid;
                var sid = (string)data.sid;
                var areas = CacheCenter.GetAllActiveArea(tokenModel.Domain);
                if (areas != null)
                {
                    var g_aid = PNUtility.Helpers.UtilityHelper.ParseGuid(aid);
                    var area = areas.Where(a => a.AreaId == g_aid).FirstOrDefault();
                    if (area != null)
                    {
                        var g_sid = PNUtility.Helpers.UtilityHelper.ParseGuid(sid);
                        var shift = area.Shift.Where(a => a.ShiftId == g_sid).FirstOrDefault();
                        if (shift != null)
                        {
                            MyTasks.DBTask.AddTask((a, k) => {
                                var shiftId = (Guid)a[1];
                                using (var context = new EF.HrCloudEntities())
                                {
                                    var shiftModel = context.Shift.Where(aa => aa.ShiftId == shiftId).FirstOrDefault();
                                    if (shiftModel != null)
                                    {
                                        context.Shift.Remove(shiftModel);
                                        context.SaveChanges();
                                        CacheCenter.ResetArea(shift.Domain);
                                    }
                                }
                            }, "", new object[] { tokenModel, g_sid });
                            updateChanges("SHIFT", tokenModel.Domain, "");
                            return sid;
                        }
                    }
                }
                return "";
            }
        }

        [Route("api/main/get-all-shifts")]
        [HttpGet]
        public HttpResponseMessage GetAllShifts()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var list = CacheCenter.GetAllActiveArea(tokenModel.Domain);
                List<AreaModel> areas = new List<AreaModel>();

                foreach (var item in list)
                {
                    AreaModel a = new AreaModel();
                    a.Id = item.AreaId.ToString();
                    a.Name = item.Name;
                    a.Photo = item.Photo;
                    a.Shifts = new List<ShiftModel>();
                    foreach (var s in item.Shift)
                    {
                        ShiftModel sm = new ShiftModel();
                        sm.AreaId = a.Id;
                        sm.Duration = s.Duration;
                        sm.Id = s.ShiftId.ToString();
                        sm.Name = s.Name;
                        sm.Begin = s.XBegin;
                        sm.DText = Helper.GetShiftText(s.XBegin, s.Duration);
                        sm.Rate = s.RateId;
                        a.Shifts.Add(sm);
                    }
                    a.Shifts = a.Shifts.OrderBy(c => c.Begin).ToList();
                    areas.Add(a);
                }
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(areas));
                return response;
            }
        }

        [Route("api/main/get-shift-by-id")]
        [HttpGet]
        public HttpResponseMessage GetShiftById(string area, string id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var list = CacheCenter.GetAllActiveArea(tokenModel.Domain);
                var aid = PNUtility.Helpers.UtilityHelper.ParseGuid(area);
                var dbo = list.Where(a => a.AreaId == aid).FirstOrDefault();
                if (dbo != null)
                {
                    var sid = PNUtility.Helpers.UtilityHelper.ParseGuid(id);
                    var s = dbo.Shift.Where(a => a.ShiftId == sid).FirstOrDefault();
                    ShiftModel sm = new ShiftModel();
                    if (s != null)
                    {
                        sm.AreaId = s.AreaId.ToString();
                        sm.Duration = s.Duration;
                        sm.Id = s.ShiftId.ToString();
                        sm.Name = s.Name;
                        sm.Begin = s.XBegin;
                        sm.DText = Helper.GetShiftText(s.XBegin, s.Duration);
                    }
                    response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(sm));
                    return response;
                }
                else
                {
                    response.Content = new StringContent("401");
                    return response;
                }
            }
        }

        [Route("api/main/get-all-labels")]
        [HttpGet]
        public HttpResponseMessage GetAllLabels()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var list = CacheCenter.GetAllLabels(tokenModel.Domain).Select(a => new { Id = a.LabelId, Name = a.Name, SKey = a.SKey, Color = a.Color }).ToList();
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list));
                return response;
            }
        }

        [Route("api/main/create-label")]
        [HttpPost]
        public HttpResponseMessage CreateLabel(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                Guid id = Guid.NewGuid();

                MyTasks.DBTask.AddTask((a, k) => {
                    var labelID = (Guid)a[2];
                    var dobj = (dynamic)a[1];
                    var tm = (TokenModel)a[0];
                    using (var context = new EF.HrCloudEntities())
                    {
                        EF.Label label = new EF.Label();
                        label.Domain = tm.Domain;
                        label.Name = (string)dobj.name;
                        label.OwnerId = tm.OwnerId;
                        label.SKey = labelID;
                        label.Color = (string)dobj.color;
                        context.Label.Add(label);
                        context.SaveChanges();
                        CacheCenter.ResetLabel(tm.Domain);
                    }

                }, "", new object[] { tokenModel, data, id });
                updateChanges("LABEL", tokenModel.Domain, "");
                response.Content = new StringContent(id.ToString());
                return response;
            }
        }

        [Route("api/main/update-label")]
        [HttpPost]
        public HttpResponseMessage UpdateLabel(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var skey = (string)data.skey;
                Guid id = PNUtility.Helpers.UtilityHelper.ParseGuid(skey);
                MyTasks.DBTask.AddTask((a, k) => {
                    var labelID = (Guid)a[2];
                    var dobj = (dynamic)a[1];
                    var tm = (TokenModel)a[0];
                    using (var context = new EF.HrCloudEntities())
                    {
                        var label = context.Label.Where(aa => aa.SKey == labelID).FirstOrDefault();
                        label.Domain = tm.Domain;
                        label.Name = (string)dobj.name;
                        label.OwnerId = tm.OwnerId;
                        label.SKey = labelID;
                        label.Color = (string)dobj.color;
                        context.SaveChanges();
                        CacheCenter.ResetLabel(tm.Domain);
                    }

                }, "", new object[] { tokenModel, data, id });
                updateChanges("LABEL", tokenModel.Domain, "");
                response.Content = new StringContent(id.ToString());
                return response;
            }
        }

        [Route("api/main/delete-label")]
        [HttpPost]
        public string DeleteLabel(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "400";
            }
            else
            {
                var sid = (string)data.skey;
                var labels = CacheCenter.GetAllLabels(tokenModel.Domain);
                if (labels != null)
                {
                    var g_sid = PNUtility.Helpers.UtilityHelper.ParseGuid(sid);
                    var label = labels.Where(a => a.SKey == g_sid).FirstOrDefault();
                    if (label != null)
                    {
                        MyTasks.DBTask.AddTask((a, k) => {
                            var labelSkey = (Guid)a[1];
                            using (var context = new EF.HrCloudEntities())
                            {
                                var labelModel = context.Label.Where(aa => aa.SKey == labelSkey).FirstOrDefault();
                                if (labelModel != null)
                                {
                                    context.Label.Remove(labelModel);
                                    context.SaveChanges();
                                    CacheCenter.ResetLabel(labelModel.Domain);
                                }
                            }
                        }, "", new object[] { tokenModel, g_sid });
                        updateChanges("LABEL", tokenModel.Domain, "");
                        return sid;
                    }
                }
                return "";
            }
        }

        [Route("api/main/create-position")]
        [HttpPost]
        public HttpResponseMessage CreatePosition(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                Guid id = Guid.NewGuid();

                MyTasks.DBTask.AddTask((a, k) => {
                    var labelID = (Guid)a[2];
                    var dobj = (dynamic)a[1];
                    var tm = (TokenModel)a[0];
                    using (var context = new EF.HrCloudEntities())
                    {
                        EF.Postion position = new EF.Postion();
                        position.Domain = tm.Domain;
                        position.Name = (string)dobj.name;
                        position.OwnerId = tm.OwnerId;
                        position.SKey = labelID;
                        position.Color = (string)dobj.color;
                        context.Postion.Add(position);
                        context.SaveChanges();
                        CacheCenter.ResetPosition(tm.Domain);
                    }

                }, "", new object[] { tokenModel, data, id });
                updateChanges("POSITION", tokenModel.Domain, "");
                response.Content = new StringContent(id.ToString());
                return response;
            }
        }

        [Route("api/main/get-all-positions")]
        [HttpGet]
        public HttpResponseMessage GetAllPositions()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var list = CacheCenter.GetAllPositons(tokenModel.Domain).Select(a => new { Id = a.PositionId, Name = a.Name, SKey = a.SKey, Color = a.Color }).ToList();
                response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(list));
                return response;
            }
        }

        [Route("api/main/update-position")]
        [HttpPost]
        public HttpResponseMessage UpdatePosition(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var skey = (string)data.skey;
                Guid id = PNUtility.Helpers.UtilityHelper.ParseGuid(skey);
                MyTasks.DBTask.AddTask((a, k) => {
                    var labelID = (Guid)a[2];
                    var dobj = (dynamic)a[1];
                    var tm = (TokenModel)a[0];
                    using (var context = new EF.HrCloudEntities())
                    {
                        var position = context.Postion.Where(aa => aa.SKey == labelID).FirstOrDefault();
                        position.Domain = tm.Domain;
                        position.Name = (string)dobj.name;
                        position.OwnerId = tm.OwnerId;
                        position.SKey = labelID;
                        position.Color = (string)dobj.color;
                        context.SaveChanges();
                        CacheCenter.ResetPosition(tm.Domain);
                    }

                }, "", new object[] { tokenModel, data, id });
                updateChanges("POSITION", tokenModel.Domain, "");
                response.Content = new StringContent(id.ToString());
                return response;
            }
        }

        [Route("api/main/delete-position")]
        [HttpPost]
        public string DeletePosition(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                return "400";
            }
            else
            {
                var sid = (string)data.skey;
                var positions = CacheCenter.GetAllPositons(tokenModel.Domain);
                if (positions != null)
                {
                    var g_sid = PNUtility.Helpers.UtilityHelper.ParseGuid(sid);
                    var position = positions.Where(a => a.SKey == g_sid).FirstOrDefault();
                    if (position != null)
                    {
                        MyTasks.DBTask.AddTask((a, k) => {
                            var labelSkey = (Guid)a[1];
                            using (var context = new EF.HrCloudEntities())
                            {
                                var labelModel = context.Postion.Where(aa => aa.SKey == labelSkey).FirstOrDefault();
                                if (labelModel != null)
                                {
                                    context.Postion.Remove(labelModel);
                                    context.SaveChanges();
                                    CacheCenter.ResetPosition(labelModel.Domain);
                                }
                            }
                        }, "", new object[] { tokenModel, g_sid });
                        updateChanges("POSITION", tokenModel.Domain, "");
                        return sid;
                    }
                }
                return "";
            }
        }

        [Route("api/main/get-full-user-info")]
        [HttpGet]
        public HttpResponseMessage GetFullUserInfo(string username)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var user = CacheCenter.GetFullEmployeeList(tokenModel.Domain).Where(a => a.Username == username).FirstOrDefault();
                if (user != null)
                {
                    UserInfo ui = new UserInfo();
                    ui.FirstName = user.FirstName;
                    ui.LastName = user.Lastname;
                    ui.Domain = user.Domain;
                    ui.Avatar = user.Photo;
                    ui.Sex = user.Sex;
                    response.Content = new StringContent(PNUtility.Helpers.JSONHelper.Jsoner.Serialize(ui));
                    return response;
                }
                else
                {
                    response.Content = new StringContent("404");
                    return response;
                }
            }
        }

        [Route("api/main/hide-user")]
        [HttpPost]
        public HttpResponseMessage MakeHideUser(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var username = (string)data.username;
                var user = CacheCenter.GetFullEmployeeList(tokenModel.Domain).Where(a => a.Username == username).FirstOrDefault();
                if (user != null)
                {
                    MyTasks.DBTask.AddTask((a, k) => {
                        var dobj = (dynamic)a[0];
                        var duser = (EF.Employee)a[1];

                        using (var context = new EF.HrCloudEntities())
                        {
                            var dbo = context.Employee.Include(em => em.EmployeeDetail).Where(x => x.Username == k).FirstOrDefault();
                            if (dbo != null)
                            {
                                dbo.Status = 6;
                                context.SaveChanges();
                                CacheCenter.ResetEmployee(dbo.Domain);
                                updateChanges("USERS", tokenModel.Domain, "");
                            }
                        }
                    }, username, new object[] { data, user });
                    response.Content = new StringContent("ok");
                    return response;
                }
                else
                {
                    response.Content = new StringContent("404");
                    return response;
                }
            }
        }

        [Route("api/main/update-user-info")]
        [HttpPost]
        public HttpResponseMessage UpdateUserInfo(dynamic data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var tokenModel = GetValidToken();
            if (tokenModel == null)
            {
                response.Content = new StringContent("400");
                return response;
            }
            else
            {
                var username = (string)data.username;
                var user = CacheCenter.GetFullEmployeeList(tokenModel.Domain).Where(a => a.Username == username).FirstOrDefault();
                if (user != null)
                {
                    MyTasks.DBTask.AddTask((a, k) => {
                        var dobj = (dynamic)a[0];
                        var duser = (EF.Employee)a[1];
                        
                        using(var context = new EF.HrCloudEntities())
                        {
                            var dbo = context.Employee.Include(em=>em.EmployeeDetail).Where(x => x.Username == k).FirstOrDefault();
                            if(dbo != null)
                            {
                                var position = PNUtility.Helpers.UtilityHelper.ParseInt((string)dobj.position);
                                dbo.Position = position;
                                var areaId = PNUtility.Helpers.UtilityHelper.ParseGuid((string)dobj.area);
                                dbo.AreaId = areaId;
                                dbo.FirstName = (string)dobj.firstName;
                                dbo.Lastname = (string)dobj.lastName;
                                dbo.Sex = (string)dobj.sex == "1";
                                dbo.Phone = (string)dobj.phone;
                                dbo.IsAccounting = (string)dobj.func == "1";
                                EF.EmployeeDetail emdetail = dbo.EmployeeDetail == null ? new EF.EmployeeDetail() : dbo.EmployeeDetail;
                                

                                CultureInfo provider = CultureInfo.InvariantCulture;
                                var birthday = DateTime.ParseExact((string)dobj.birthday, "dd/MM/yyyy", provider);
                                emdetail.Birthday = birthday;
                                emdetail.CMND = (string)dobj.cmnd;
                                emdetail.Addr = (string)dobj.addr;
                                var joindate = DateTime.ParseExact((string)dobj.joindate, "dd/MM/yyyy", provider);
                                dbo.JoinDate = joindate;
                                emdetail.Labels = (string)dobj.labels;
                                if (dbo.EmployeeDetail == null)
                                {
                                    emdetail.Username = dbo.Username;
                                    context.EmployeeDetail.Add(emdetail);
                                }
                                var photo = (string)dobj.avatar;
                                if (!string.IsNullOrEmpty(photo))
                                {
                                    if (!string.IsNullOrEmpty(dbo.Photo))
                                        Helper.DeleteFile(dbo.Photo);

                                    var fileName = Helper.GetFileNameFromTemphotoFolder(photo);
                                    var newPath = "/Media/Owners/" + dbo.Domain + "/" + fileName;
                                    dbo.Photo = newPath;
                                    Helper.MoveFile(photo, newPath);
                                }

                                var isChanged = (bool)dobj.isChanged;
                                if (isChanged)
                                {
                                    var nstr = (string)dobj.pictures;
                                    UpdateMultiPhotoModel updateModel = new UpdateMultiPhotoModel(dbo.Domain, emdetail.PhotoCompany, nstr);
                                    emdetail.PhotoCompany = updateModel.GetResultText();
                                    updateModel.ProcessImages();
                                }
                                var mtext = (string)dobj.hcost;
                                var mo = PNUtility.Helpers.UtilityHelper.ParseLong(mtext);
                                dbo.HValue = mo;

                                context.SaveChanges();
                                CacheCenter.ResetEmployee(dbo.Domain);
                                updateChanges("USERS", tokenModel.Domain, "");
                            }
                        }
                    }, username, new object[] { data, user });
                    response.Content = new StringContent("ok");
                    return response;
                }
                else
                {
                    response.Content = new StringContent("404");
                    return response;
                }
            }
        }

       

        private TokenModel GetValidToken()
        {
            var hd = Request.Headers.GetValues("access_token");
            if (hd != null && hd.Count() > 0)
            {
                var at = hd.ElementAt(0);
                return CacheCenter.GetTokenModel(at);
            }
            else
                return null;
        }

        private bool isCenterCalled()
        {
            var hd = Request.Headers.GetValues("API-Token");
            return hd.Count() > 0 && hd.ElementAt(0) == skey;
        }

        private void updateChanges(string type, string domain, string id)
        {
            AChanges ac = new AChanges();
            ac.Type = type;
            ac.Time = PNUtility.Helpers.UtilityHelper.ConvertToTimestamp(DateTime.Now);
            ac.Domain = domain;
            ac.Id = id;
            var jdata = PNUtility.Helpers.JSONHelper.Jsoner.Serialize(ac);
            NetworkService.callPostAPI(UPDATE_CHANGES_URL, jdata, (str) => { }, () => { });
        }
    }
}
