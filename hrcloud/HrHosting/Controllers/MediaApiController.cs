﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HrHosting.Controllers
{
    [EnableCors(origins: "http://localhost:50940,https://hrcenter.vn", headers: "*", methods: "*")]
    public class MediaApiController : ApiController
    {
        [Route("api/main/post-multi-photo")]
        [HttpPost]
        public string UploadMultiImages()
        {
            string res = "";
            for (int i = 0; i < 2; i++)
            {
                var text = HttpContext.Current.Request.Form["photo" + i.ToString()];
                if (!string.IsNullOrEmpty(text))
                {
                    Guid fid = Guid.NewGuid();
                    var newfile = Convert.FromBase64String(HttpContext.Current.Request.Form["photo" + i.ToString()]);
                    string fileName = "/TempPhoto/" + fid.ToString() + ".png";
                    System.IO.File.WriteAllBytes(HostingEnvironment.MapPath(fileName), newfile);
                    res = res + "/TempPhoto/" + fid.ToString() + ".png" + "*";
                }
            }
            return res;
        }

        [Route("api/main/post-photo")]
        [HttpPost]
        public string UploadImages()
        {
            try
            {
                if (HttpContext.Current.Request.Form["photo"] != null)
                {
                    Guid fid = Guid.NewGuid();
                    var newfile = Convert.FromBase64String(HttpContext.Current.Request.Form["photo"]);
                    string fileName = "/TempPhoto/" + fid.ToString() + ".png";
                    System.IO.File.WriteAllBytes(HostingEnvironment.MapPath(fileName), newfile);
                    return "/TempPhoto/" + fid.ToString() + ".png";
                }
                return "";
            }
            catch(Exception ex)
            {
                return ex.Message + "-x";
            }
            
        }
    }
}
