﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class ChangeDataModel
    {
        public long AreaChanged { get; set; }
        public long ShiftChanged { get; set; }
        public long LabelChanged { get; set; }
        public long PositonChanged { get; set; }
        public long UserListChanged { get; set; }
        public Dictionary<string, long> UserChanges { get; set; }
    }
}