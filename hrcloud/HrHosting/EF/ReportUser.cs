//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HrHosting.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReportUser
    {
        public System.Guid ReportId { get; set; }
        public string Domain { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int Month { get; set; }
        public string CreateBy { get; set; }
        public string Username { get; set; }
        public decimal Advance { get; set; }
        public bool HasAdvance { get; set; }
        public byte OffCount { get; set; }
        public int Minutes { get; set; }
        public decimal WMoney { get; set; }
        public decimal InMoney { get; set; }
        public string InJData { get; set; }
        public decimal OutMoney { get; set; }
        public string OutJData { get; set; }
        public decimal Salary { get; set; }
        public string Workings { get; set; }
    }
}
