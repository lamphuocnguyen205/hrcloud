//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HrHosting.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Shift
    {
        public System.Guid ShiftId { get; set; }
        public System.Guid AreaId { get; set; }
        public int XBegin { get; set; }
        public int XEnd { get; set; }
        public string Name { get; set; }
        public string Domain { get; set; }
        public int XOrder { get; set; }
        public double Duration { get; set; }
        public int RateId { get; set; }
    
        public virtual Area Area { get; set; }
    }
}
