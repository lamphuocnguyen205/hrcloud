﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class MyTasks
    {
        public static PNUtility.Tasks.TaskHandler FileTask = new PNUtility.Tasks.TaskHandler();
        public static PNUtility.Tasks.TaskHandler DBTask = new PNUtility.Tasks.TaskHandler();
        public static PNUtility.Tasks.TaskHandler GeneralTask = new PNUtility.Tasks.TaskHandler();
        public static PNUtility.Tasks.TaskHandler NetworkTask = new PNUtility.Tasks.TaskHandler();
    }
}