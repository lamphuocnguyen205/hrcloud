﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting.Models
{
    public class PhotoItem
    {
        public string URL { get; set; }
        public string Desc { get; set; }
        public bool IsTempPath()
        {
            if (URL.Substring(0, 2) == "/T")
            {
                return true;
            }
            else
                return false;
        }

        public string GetFileName()
        {
            if (URL.Length > 40)
                return URL.Substring(URL.Length - 40, 36);
            else
                return "";
        }

        public void MoveToDataFolder(string domain)
        {
            var fileName = Helper.GetFileNameFromTemphotoFolder(URL);
            var newPath = "/Media/Owners/" + domain + "/Company/" + fileName;
            Helper.MoveFile(URL, newPath);
        }

        public void Remove()
        {
            Helper.DeleteFile(URL);
        }
    }
}