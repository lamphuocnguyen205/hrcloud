﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class AreaModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public List<ShiftModel> Shifts { get; set; }
    }

    public class ShiftModel
    {
        public string Id { get; set; }
        public string AreaId { get; set; }
        public string Name { get; set; }
        public string DText { get; set; }
        public int Begin { get; set; }
        public double Duration { get; set; }
        public int Rate { get; set; }
    }
}