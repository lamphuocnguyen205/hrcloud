﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class WorkingModel
    {
        public Guid Id { get; set; }
        public Guid AreaId { get; set; }
        public string Username { get; set; }
        public string ShiftId { get; set; }
        public long Begin { get; set; }
        public long End { get; set; }
        public int Status { get; set; }
        public bool IsOff { get; set; }
        public string Note { get; set; }
        public bool Accept { get; set; }
        public string Reason { get; set; }
        public int Rate { get; set; }

        public long DateVal { get; set; }
    }
}