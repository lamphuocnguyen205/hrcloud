﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class AdvanceModel
    {
        public Guid Id { get; set; }
        public string CreateDate { get; set; }
        public string Note { get; set; }
        public string PlanId { get; set; }
        public string PlanName { get; set; }
        public bool IsActive { get; set; }
        public decimal Money { get; set; }
        public string Username { get; set; }
    }
}