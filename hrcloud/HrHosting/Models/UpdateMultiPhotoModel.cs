﻿using HrHosting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class UpdateMultiPhotoModel
    {
        public List<PhotoItem> OldList { get; set; }
        public List<PhotoItem> NewList { get; set; }
        private string originText;
        private string newText;
        private string domain;
        private List<string> newImages;
        private List<string> removeImages;

        public UpdateMultiPhotoModel(string domain, string oStr, string nStr)
        {
            this.domain = domain;
            this.newText = nStr;
            if (!string.IsNullOrEmpty(oStr))
            {
                OldList = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<PhotoItem>>(oStr);
            }
            else
                OldList = new List<PhotoItem>();

            if (!string.IsNullOrEmpty(nStr))
            {
                NewList = PNUtility.Helpers.JSONHelper.Jsoner.Deserialize<List<PhotoItem>>(nStr);
            }
            else
                NewList = new List<PhotoItem>();
        }

        public void ProcessImages()
        {
            MyTasks.FileTask.AddTask((a, k) => {
                foreach (var item in OldList)
                {
                    var find = NewList.Where(i => i.URL == item.URL).FirstOrDefault();
                    if (find == null)
                    {
                        var filename = item.GetFileName();
                        if (!newText.Contains(filename))
                            item.Remove();
                    }
                }
                foreach (var item in NewList)
                {
                    if (item.IsTempPath())
                    {
                        item.MoveToDataFolder(k);
                    }
                }

            }, this.domain, new object[] { });
            
        }

        public string GetResultText()
        {
            return newText.Replace("TempPhoto", "Media/Owners/" + domain + "/Company");
        }
    }
}