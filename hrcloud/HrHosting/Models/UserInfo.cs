﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class UserInfo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string UName { get; set; }
        public string Role { get; set; }
        public string Avatar { get; set; }
        public string Avatar100 { get; set; }
        public string Domain { get; set; }
        public string JoinDate { get; set; }
        public string Labels { get; set; }
        public int Position { get; set; }
        public bool Sex { get; set; }
        public float HW { get; set; }
        public int SW { get; set; }
        public int SO { get; set; }
        public string Birthday { get; set; }
        public string CMND { get; set; }
        public string Addr { get; set; }
        public string Pictures { get; set; }
        public string AreaId { get; set; }
        public string AreaM { get; set; }
        public bool IaC { get; set; }
        public decimal Advance { get; set; }
        public decimal HValue { get; set; }

        public string Phone { get; set; }

        public bool IsAccounting { get; set; }

    }
}