﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class PostModel
    {
        public Guid Id { get; set; }
        public long Date { get; set; }
        public long Time { get; set; }
        public int Type { get; set; }
        public string JData { get; set; }
        public string RefId { get; set; }

    }
}