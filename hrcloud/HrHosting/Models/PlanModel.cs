﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace HrHosting
{
    public class PlanModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string CreateDate { get; set; }
        public decimal Total { get; set; }
        public string FeedId { get; set; }
    }
}