﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class OwnerInfo
    {
        public string Name { get; set; }
        public Guid OwnerId { get; set; }
        public List<WRate> Rates { get; set; }
         
    }

    public class WRate
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public double RateVal { get; set; }
    }
}