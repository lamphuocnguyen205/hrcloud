﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class FeedAbsentModel
    {
        public List<AbsentItem> AbsentList { get; set; }
    }

    public class AbsentItem
    {
        public string Username { get; set; }
        public string ShiftId { get; set; }
    }
}