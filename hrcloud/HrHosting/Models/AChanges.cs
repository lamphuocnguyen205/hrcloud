﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrHosting
{
    public class AChanges
    {
        public string Domain { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public long Time { get; set; }
        public List<AChanges> SubChanges { get; set; }
    }
}