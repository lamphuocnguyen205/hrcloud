﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace HrHosting
{
    public class Helper
    {
        public static string GetShiftText(int begin, double duration)
        {
            var hour = begin / 60;
            var minutes = begin % 60;
            string res = "";
            res = GetTimeText(hour) + "h" + GetTimeText(minutes);
            var end = (int)(begin + (duration * 60));
            hour = end / 60;
            minutes = end % 60;
            if (end < 1440)
            {
                res = res + " - " + GetTimeText(hour) + "h" + GetTimeText(minutes);
            }
            else {
                res = res + " - " + GetTimeText(hour % 24) + "h" + GetTimeText(minutes);
                res = res + " (hôm sau)";
            }
            return res;
        }

        public static string GetTimeText(int val)
        {
            return val < 10 ? "0" + val.ToString() : val.ToString();
        }

        public static string ConvertToServerPath(string path)
        {
            return HostingEnvironment.MapPath(path);
        }

        public static void DeleteFile(string filePath)
        {
            MyTasks.FileTask.AddTask((a, k) => {
                try
                {
                    var spath = ConvertToServerPath(k);
                    if (System.IO.File.Exists(spath))
                    {                       
                        System.IO.File.Delete(spath);
                    }
                }
                catch (Exception eeee) { }
            }, filePath, new object[] { });            
        }

        public static string GetFileNameFromTemphotoFolder(string path)
        {
            try
            {
                return path.Substring(11, path.Length - 11);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static void MoveFile(string temp, string taget)
        {
            try
            {
                System.IO.File.Move(ConvertToServerPath(temp), ConvertToServerPath(taget));
            }
            catch (Exception) { }
        }

        public static void CopyFile(string temp, string taget)
        {
            try
            {
                System.IO.File.Copy(ConvertToServerPath(temp), ConvertToServerPath(taget));
            }
            catch (Exception) { }
        }

        public static DateTime ParseTimeFromDash(string dashText)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            return DateTime.ParseExact(dashText, "dd-MM-yyyy", provider);
        }

        public static float ParseFloat(string input)
        {
            var text = input.Replace(',', '.');
            return float.Parse(input);
        }
    }
}